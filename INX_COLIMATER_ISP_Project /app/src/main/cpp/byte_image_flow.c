#include "byte_image_flow.h"
#include <math.h>

int ExtractLocalMin(int** local_min_x, int** local_min_y, BYTE* dns, int image_h, int image_w, int radius, int half, int stride, int border, int nms, BYTE* mask, int patch_size) {
    int point_cnt;
    int local_min_cnt;
    int max_reserve_size = 10000;
    int *point_j = (int *)G3Alloc(max_reserve_size * sizeof(int));
    int *point_i = (int *)G3Alloc(max_reserve_size * sizeof(int));
    FindKeypointsNumber(point_i, point_j, &point_cnt, &local_min_cnt, dns, image_h, image_w, half, stride, border, nms, mask, patch_size);

    (*local_min_x) = (int*)G3ReAlloc(*local_min_x, local_min_cnt * sizeof(int));
    (*local_min_y) = (int*)G3ReAlloc(*local_min_y, local_min_cnt * sizeof(int));

    FindLocalMin(*local_min_x, *local_min_y, dns, image_h, image_w, nms, point_cnt, point_i, point_j);
    G3Free(point_i);
    G3Free(point_j);

    return local_min_cnt;
}

int ExtractLocalMin_V2(int *local_min_x, int *local_min_y, BYTE* dns, int image_h, int image_w, int radius, int half, int stride, int border, int nms, BYTE* mask, int patch_size) {
    int point_cnt;
    int local_min_cnt;
    int max_reserve_size = 10000;
    int *point_j = (int *)G3Alloc(max_reserve_size * sizeof(int));
    int *point_i = (int *)G3Alloc(max_reserve_size * sizeof(int));
    FindKeypointsNumber(point_i, point_j, &point_cnt, &local_min_cnt, dns, image_h, image_w, half, stride, border, nms, mask, patch_size);
    FindLocalMin(local_min_x, local_min_y, dns, image_h, image_w, nms, point_cnt, point_i, point_j);
    G3Free(point_i);
    G3Free(point_j);

    return local_min_cnt;
}

void ExtractAngle(float* local_min_ort,BYTE* ipp_image, int image_h, int image_w, int local_min_cnt, int radius, int angle_switch, int* local_min_x, int* local_min_y, BYTE* mask) {
    float* pixel_ort = (float*)G3CAlloc(image_h * image_w, sizeof(float));
    //double* pixel_ort_r = (double*)G3CAlloc(image_h * image_w, sizeof(double));
    Detect_V3(pixel_ort, radius, ipp_image, mask, image_h, image_w, angle_switch);
    OrientOfPoints_V2(local_min_ort, image_h, image_w, local_min_cnt, local_min_x, local_min_y, pixel_ort);
    for (int i = 0; i < local_min_cnt; i++) {
        local_min_ort[i] = floor(local_min_ort[i]*180.0/3.14159265358979323846+0.5)/180.0*3.14159265358979323846;
    }
    G3Free(pixel_ort);
    //G3Free(pixel_ort_r);
}

void ExtractAngle_small(float* local_min_ort,BYTE* ipp_image, int image_h, int image_w, int local_min_cnt, int radius, int angle_switch, int* local_min_x, int* local_min_y, BYTE* mask) {
    float* pixel_ort = (float*)G3CAlloc(image_h * image_w, sizeof(float));
    //double* pixel_ort_r = (double*)G3CAlloc(image_h * image_w, sizeof(double));
    Detect_V3(pixel_ort, radius, ipp_image, mask, image_h, image_w, angle_switch);
    OrientOfPoints_V2_small(local_min_ort, image_h, image_w, local_min_cnt, local_min_x, local_min_y, pixel_ort);
    for (int i = 0; i < local_min_cnt; i++) {
        local_min_ort[i] = floor(local_min_ort[i]*180.0/3.14159265358979323846+0.5)/180.0*3.14159265358979323846;
    }
    G3Free(pixel_ort);
    //G3Free(pixel_ort_r);
}

void DestroyKptsInfo(int* local_min_x, int* local_min_y, float* local_min_ort) {
    G3Free(local_min_x);
    G3Free(local_min_y);
    G3Free(local_min_ort);
   // G3Free(local_min_r);
}

void ExtractAngleReliImg(float* ort_img, double* reli_img, BYTE* ipp_image, int image_h, int image_w, int local_min_cnt, int radius, int angle_switch, int* local_min_x, int* local_min_y, BYTE* mask){
    //float* pixel_ort = (float*)G3CAlloc(image_h * image_w, sizeof(float));
    //double* pixel_ort_reli = (double*)G3CAlloc(image_h * image_w, sizeof(double));
    //double* pixel_ort_r = (double*)G3CAlloc(image_h * image_w, sizeof(double));
    Detect_V3_reli(ort_img, reli_img, radius, ipp_image, mask, image_h, image_w, angle_switch);
    //OrientOfPoints_V2(local_min_ort, image_h, image_w, local_min_cnt, local_min_x, local_min_y, pixel_ort);
    for (int i = 0; i < image_h * image_w; i++) {
        ort_img[i] = floor(ort_img[i]*180.0/3.14159265358979323846+0.5)/180.0*3.14159265358979323846; //local_min_ort[i]*180.0/3.14159265358979323846+0.5;
    }
    //G3Free(pixel_ort);
}
