#ifndef GOMATCHER_BYTE_IMAGE_H
#define GOMATCHER_BYTE_IMAGE_H

#include "macros_allocate.h"

#ifdef __cplusplus
extern "C" {
#endif
  
// Find local min --------------------------------------------------

EGIS BYTE *CreateMaskByte(int image_h, int image_w, int center_trig, int radius_trig, float center_x, float center_y, float radius);

EGIS void SetMaskByte(BYTE *image, int image_h, int image_w, int center_trig, int radius_trig, float center_x, float center_y, float radius);

EGIS void FindKeypointsNumber(int *point_i, int *point_j, int *point_cnt, int *local_min_cnt, const BYTE *image, size_t image_h, size_t image_w, int half, int stride, int ex_border, int nms, const BYTE *mask, int patch_size);

EGIS void FindLocalMin(int *local_min_x, int *local_min_y, const BYTE *image, size_t image_h, size_t image_w, int nms, int point_cnt, int *point_i, int *point_j);

EGIS void TBinary8Bits(const BYTE* img, BYTE* out, int h, int w, int radius, const BYTE* mask);

EGIS void RidgeOrient_V3(BYTE* image, int image_h, int image_w, int gradientsigma, int blocksigma, int orientsmoothsigma, int fft_flag, unsigned int resize, unsigned int fix_point,unsigned int turbo, float* pixel_ort);

EGIS void RidgeOrient_V3_reli(BYTE* image, int image_h, int image_w, int gradientsigma, int blocksigma, int orientsmoothsigma, int fft_flag, unsigned int resize, unsigned int fix_point,unsigned int turbo, float* pixel_ort, double* pixel_ort_r);

EGIS void Detect_V3(float* pixel_ort, int radius, BYTE* img_uint8, BYTE* mask, int image_h, int image_w, int switch_angle_calculation);

EGIS void OrientOfPoints_V2(float* local_min_ort, int image_h, int image_w, int local_min_cnt, int* local_min_x, int* local_min_y, float* pixel_ort);

EGIS void OrientOfPoints_V2_small(float* local_min_ort, int image_h, int image_w, int local_min_cnt, int* local_min_x, int* local_min_y, float* pixel_ort);

EGIS void Detect_V3_reli(float* pixel_ort, double* pixel_ort_reli,int radius, BYTE* img_uint8, BYTE* mask, int image_h, int image_w, int switch_angle_calculation);

EGIS int extract_patches_v3(unsigned char *patches, 
                               unsigned char *image, 
                               float *pts_x, 
                               float *pts_y, 
                               double *pts_angle_radians, 
                               BYTE *valid_index, 
                               unsigned long pts_count, 
                               int image_h, 
                               int image_w,
                               int patch_height,
                               int resize_patch_height,
                               bool with_flip,
                               int fp_flag,
                               float patch_th,
                               BYTE* mask);

// Calculate Shannon entropy of an image, value of data must be within 0~255
float entropy(int *data, size_t size);


#ifdef __cplusplus
}
#endif

#endif
