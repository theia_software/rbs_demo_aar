//
//  quality_level.h
//  ISP
//
//  Created by Luan Liu on 2019/10/30.
//  Copyright © 2019 Theia. All rights reserved.
//

#ifndef quality_level_h
#define quality_level_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "byte_image.h"
#include "orientation.h"

float CalQualityLevel(int *image, int *bkg, unsigned char *ref_img, int width, int height);
float calSignalPower(float* signal_map, float* offset_map, int height, int width);
float calNoisePower(BYTE* img, float* signal_map, int height, int width);
float CreateIdealSignalMap(BYTE* ori_img, BYTE* ref_img, float* signal_map, int width, int height);


#endif /* quality_level_h */
