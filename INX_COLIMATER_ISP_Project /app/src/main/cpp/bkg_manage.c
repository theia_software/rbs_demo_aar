
#include "bkg_manage.h"
#include "ISP_filter.h"
#include <stdlib.h>
#include <string.h>

const int MAX_TIME_STAMP = 10000;
const int KERNEL_SIZE = 3;
//const int BKG_THRESHOLD = 200;
const int BKG_THRESHOLD = 99999999;
const int STABLE_THRESHOLD = 8;
const int ENROLL_THRESHOLD = 12;
const float LEARN_RATE1 = 1.0f/16.0f;// for updating an stable bkg.
const float LEARN_RATE2 = 1.0f/10.0f;// for updating an unstable bkg.
const float LEARN_RATE3 = 1.0f/5.0f; // for constructing a new bkg.





BKG_DATA *init_bkg_data(int *init_bkg, int width, int height, int max_bkg_num)
{
    BKG_DATA *bkg_data;
    
    bkg_data = (BKG_DATA *)malloc(sizeof(BKG_DATA));
    bkg_data->bkg_set = (int *)malloc(width * height * max_bkg_num * sizeof(int));
    bkg_data->frequency = (int *)calloc(max_bkg_num, sizeof(int));
    bkg_data->use_time_stamp = (int *)calloc(max_bkg_num, sizeof(int));
    bkg_data->exist_time_stamp = (int *)calloc(max_bkg_num, sizeof(int));
    bkg_data->not_empty = (int *)calloc(max_bkg_num, sizeof(int));
    bkg_data->width = width;
    bkg_data->height = height;
    bkg_data->max_bkg_num = max_bkg_num;
    memcpy(bkg_data->bkg_set, init_bkg, sizeof(int) * width * height);
    bkg_data->bkg_num = 1;
    bkg_data->use_time_stamp[0] = MAX_TIME_STAMP;
    bkg_data->exist_time_stamp[0] = 1;
    bkg_data->frequency[0] = 0;
    bkg_data->not_empty[0] = 1;
    return bkg_data;
}

void free_bkg_data(BKG_DATA *bkg_data)
{
    free(bkg_data->bkg_set);
    free(bkg_data->frequency);
    free(bkg_data->use_time_stamp);
    free(bkg_data->exist_time_stamp);
    free(bkg_data->not_empty);
    free(bkg_data);
}

unsigned char *encode_bkg_data_stream(BKG_DATA *bkg_data, int *data_size)
{
    unsigned char *data_stream;
    int bkg_size = bkg_data->width * bkg_data->height;
    int i = 0, size;
    
    *data_size  = sizeof(int) * bkg_size * bkg_data->max_bkg_num;
    *data_size += sizeof(int) * bkg_data->max_bkg_num * 4;
    *data_size += sizeof(int) * 4;
    
    data_stream = (unsigned char *)malloc(sizeof(unsigned char) * (*data_size));
    
    size = sizeof(int);
    memcpy(data_stream + i, &bkg_data->width, size);
    i += size;
    
    memcpy(data_stream + i, &bkg_data->height, size);
    i += size;
    

    memcpy(data_stream + i, &bkg_data->bkg_num, size);
    i += size;

    memcpy(data_stream + i, &bkg_data->max_bkg_num, size);
    i += size;
    
    size = sizeof(int) * bkg_size * bkg_data->max_bkg_num;
    memcpy(data_stream + i, bkg_data->bkg_set, size);
    i += size;
    
    size = sizeof(int) * bkg_data->max_bkg_num;
    memcpy(data_stream + i, bkg_data->frequency, size);
    i += size;
    
    memcpy(data_stream + i, bkg_data->use_time_stamp, size);
    i += size;

    memcpy(data_stream + i, bkg_data->exist_time_stamp, size);
    i += size;
    
    memcpy(data_stream + i, bkg_data->not_empty, size);
    
    return data_stream;
}


BKG_DATA *decode_bkg_data_stream(unsigned char * data_stream)
{
    
    BKG_DATA *bkg_data;
    int bkg_size;
    int i = 0, size;
    
    bkg_data = (BKG_DATA *)malloc(sizeof(BKG_DATA));
    
    
    size = sizeof(int);
    memcpy(&bkg_data->width, data_stream + i, size);
    i += size;
    memcpy(&bkg_data->height, data_stream + i, size);
    i += size;

    memcpy(&bkg_data->bkg_num, data_stream + i, size);
    i += size;

    memcpy(&bkg_data->max_bkg_num, data_stream + i, size);
    i += size;

    bkg_size = bkg_data->width * bkg_data->height;
    
    bkg_data->bkg_set = (int *)malloc(bkg_size * bkg_data->max_bkg_num * sizeof(int));
    bkg_data->frequency = (int *)malloc(bkg_data->max_bkg_num * sizeof(int));
    bkg_data->use_time_stamp = (int *)malloc(bkg_data->max_bkg_num * sizeof(int));
    bkg_data->exist_time_stamp = (int *)malloc(bkg_data->max_bkg_num * sizeof(int));
    bkg_data->not_empty = (int *)malloc(bkg_data->max_bkg_num * sizeof(int));

    size = sizeof(int) * bkg_size * bkg_data->max_bkg_num;
    memcpy(bkg_data->bkg_set, data_stream + i, size);
    i += size;
    
    size = sizeof(int) * bkg_data->max_bkg_num;
    memcpy(bkg_data->frequency, data_stream + i, size);
    i += size;
    
    memcpy(bkg_data->use_time_stamp, data_stream + i, size);
    i += size;

    memcpy(bkg_data->exist_time_stamp, data_stream + i, size);
    i += size;
    
    memcpy(bkg_data->not_empty, data_stream + i, size);

    
    return bkg_data;
}


void blending_img(int *img1, int *img2, int size, float learn_rate)
{
    int i;
    
    for (i = 0; i < size; i++) {
        img1[i] = img1[i] * (1.0f - learn_rate) + img2[i] * learn_rate;
    }
}


static void update_bkg(BKG_DATA *bkg_data, int *img, int bkg_id)
{
    int size = bkg_data->width * bkg_data->height;
    int offset = bkg_id * size;
    
    if (bkg_data->frequency[bkg_id] > STABLE_THRESHOLD) //update a stable bkg
        blending_img(bkg_data->bkg_set + offset, img, size, LEARN_RATE1);
    else //update an unstable bkg
        blending_img(bkg_data->bkg_set + offset, img, size, LEARN_RATE2);

}


void update_bkg_data(BKG_DATA *bkg_data, int *new_img, int bkg_id, int learn_bkg)
{
    int i;
    
    
    for (i = 0; i < bkg_data->max_bkg_num; i++) {
        if (bkg_data->use_time_stamp[i] <MAX_TIME_STAMP){
            bkg_data->use_time_stamp[i]++;
        }
        if (bkg_data->exist_time_stamp[i] > 0 && bkg_data->exist_time_stamp[i] <MAX_TIME_STAMP){
            bkg_data->exist_time_stamp[i]++;
        }
    }
    
    if (learn_bkg){
        update_bkg(bkg_data, new_img, bkg_id);
    }
    
    bkg_data->frequency[bkg_id]++;
    bkg_data->use_time_stamp[bkg_id] = 1;
}




void remove_bkg(BKG_DATA *bkg_data)
{
    int i;
    int max_use_time_stamp = 0;
    int max_exist_time_stamp = 0;
    int remove_bkg_id = -1;

    for (i = 0; i < bkg_data->max_bkg_num; i++) {
        if (bkg_data->use_time_stamp[i] > max_use_time_stamp) {
            max_use_time_stamp = bkg_data->use_time_stamp[i];
            remove_bkg_id = i;
        }
    }


    if (remove_bkg_id == -1) {
        for (i = 0; i < bkg_data->max_bkg_num; i++) {
            if (bkg_data->exist_time_stamp[i] > max_exist_time_stamp) {
                max_exist_time_stamp = bkg_data->exist_time_stamp[i];
                remove_bkg_id = i;
            }
        }
    }

    bkg_data->frequency[remove_bkg_id] = 0;
    bkg_data->use_time_stamp[remove_bkg_id] = MAX_TIME_STAMP;
    bkg_data->exist_time_stamp[remove_bkg_id] = 0;
    bkg_data->not_empty[remove_bkg_id] = 0;
    bkg_data->bkg_num--;
}



int add_new_bkg(BKG_DATA *bkg_data, int *new_bkg)
{
    int i, offset, bkg_id = -1;
    int size = bkg_data->width * bkg_data->height;
    if (bkg_data->bkg_num == bkg_data->max_bkg_num) {
        remove_bkg(bkg_data);
    }

    for (i = 0; i < bkg_data->max_bkg_num; i++) {
        if (!bkg_data->not_empty[i]) {
            offset = i * size;
            memcpy(bkg_data->bkg_set + offset, new_bkg, sizeof(int) *size);
            bkg_data->exist_time_stamp[i] = 1;
            bkg_data->use_time_stamp[i] = MAX_TIME_STAMP;;
            bkg_data->frequency[i] = 0;
            bkg_data->not_empty[i] = 1;
            bkg_id = i;
            break;
        }
    }

    bkg_data->bkg_num++;
    return bkg_id;
}

int judge_add_new_bkg(BKG_DATA *bkg_data, int bkg_id, float energy)
{
    int exist_time_stamp = bkg_data->exist_time_stamp[bkg_id];
    int frequency = bkg_data->frequency[bkg_id];
    
    //when the number of bkg is 1.
    //may be occurred in the first enroll process.
    if (bkg_data->bkg_num == 1) {
        if (exist_time_stamp < ENROLL_THRESHOLD) return 0;
        if (energy > BKG_THRESHOLD) return 1;
        else return 0;
    }
    
    
    if (frequency > STABLE_THRESHOLD) { //when a stable bkg has been used.
        if (energy > BKG_THRESHOLD) return 1;
        else return 0;
    }else{ //when an unstable bkg has been used.
        if (energy > BKG_THRESHOLD * 1.5f) return 1;
        else return 0;
    }
}



int subtract_bkg(int *img, BKG_DATA *bkg_data, int *best_bkg)
{

    int width = bkg_data->width;
    int height = bkg_data->height;
    int bkg_num = bkg_data->bkg_num;
    int size = width * height;
    int i, j, offset;
    int bkg_id = -1, new_bkg_id;
    int *bkg;
    int *img_sb = (int*)malloc(sizeof(int) * size);
    float *temp1 = (float*)malloc(sizeof(float) * size);
    float *temp2 = (float*)malloc(sizeof(float) * size);
    int bad_bkg = 0;
    
    
    
    float energy, min_energy = -1.0f;
    
    for (i = 0; i < bkg_num; i++) {
        
        offset = i * size;
        
        bkg = bkg_data->bkg_set + offset;
        for (j = 0; j < size; j++) {
            temp1[j] = (float)(img[j] - bkg[j]);
        }
        
        memcpy(temp2, temp1, sizeof(float) * size);
        mean_filter(temp2, width, height, KERNEL_SIZE);
        energy = 0.0f;
        for (j = 0; j < size; j++) {
            temp1[j] -= temp2[j];
            energy += temp1[j] > 0 ? temp1[j] : -temp1[j];
        }
        
        energy /= size;
        
        //printf("%4d (%3d, %5d, %3d)", (int)round(energy), bkg_data->frequency[i], bkg_data->use_time_stamp[i], bkg_data->exist_time_stamp[i]);
        
        if (min_energy < 0 || energy < min_energy) {
            min_energy = energy;
            bkg_id = i;
        }
    }
    
    
    
    offset = bkg_id * size;
    bkg = bkg_data->bkg_set + offset;
    memcpy(best_bkg, bkg, sizeof(int) * width * height);
    for (i = 0; i < size; i++) {
        img_sb[i] = img[i] - bkg[i];
    }
    
    
    
    if (judge_add_new_bkg(bkg_data, bkg_id, min_energy)) {
        int *bkg_fusion  = (int*)malloc(sizeof(int) * size);
        memcpy(bkg_fusion, bkg_data->bkg_set + offset, sizeof(int) * width * height);
        blending_img(bkg_fusion, img, size, LEARN_RATE3);
        update_bkg_data(bkg_data, img, bkg_id, 0);
        new_bkg_id = add_new_bkg(bkg_data, bkg_fusion);
        bad_bkg = 1;
        //printf(" Add new bkg !!");
    }else{
        update_bkg_data(bkg_data, img, bkg_id, 1);
    }
    
    //printf("\n");
    
    memcpy(img, img_sb, sizeof(int) * width * height);
    
    
    free(temp1);
    free(temp2);
    free(img_sb);
    
    return bad_bkg;
}





