#include <stdlib.h>
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include "fft.h"

//function
void EE(complex* a, complex* b,complex* c)
{
    c->Re = a->Re * b->Re - a->Im * b->Im;
    c->Im = a->Re * b->Im + a->Im * b->Re;
}

void EE_fp(fp_complex* a, fp_complex* b,fp_complex* c)
{
    c->fp_Re = FP_MULTI(a->fp_Re,b->fp_Re) - FP_MULTI(a->fp_Im,b->fp_Im);
    c->fp_Im = FP_MULTI(a->fp_Re,b->fp_Im) + FP_MULTI(a->fp_Im,b->fp_Re);
}

void FFT(complex *vect, int num)
{
    int f , m, nv2, nm1, i, k, l, j = 0;
    complex u, w, t;
    int le , lei, ip;

    nv2 = num / 2;
    nm1 = num - 1;
    for(i = 0; i < nm1; i++)
    {
        if(i < j)
        {
            t = vect[j];
            vect[j] = vect[i];
            vect[i] = t;
        }
        k = nv2;
        while( k <= j)
        {
            j = j - k;
            k = k / 2;
        }
        j = j + k;
    }

    f = num;

    for(l = 1; (f=f/2)!=1; l++);

    for( m = 1; m <= l; m++)
    {
        le = 2 << (m - 1);
        lei = le / 2;
        u.Re = 1.0;
        u.Im = 0.0;
        w.Re = cos(M_PI / lei);    //  here can look up table to speed up
        w.Im = -sin(M_PI / lei);
        for(j = 0; j < lei; j++)
        {
            for(i = j; i < num; i = i + le)
            {
                ip = i + lei;
                EE(&(vect[ip]), &u,&t);

                vect[ip].Re = vect[i].Re - t.Re;
                vect[ip].Im = vect[i].Im - t.Im;
                vect[i].Re = vect[i].Re + t.Re;
                vect[i].Im = vect[i].Im + t.Im;
            }
            EE(&u, &w,&u);
        }
    }
}

void FFT_fp(fp_complex *vect, int num)
{
    int f , m, nv2, nm1, i, k, l, j = 0;
    fp_complex u, w, t;
    int le , lei, ip;

    nv2 = num / 2;
    nm1 = num - 1;
    for(i = 0; i < nm1; i++)
    {
        if(i < j)
        {
            t = vect[j];
            vect[j] = vect[i];
            vect[i] = t;
        }
        k = nv2;
        while( k <= j)
        {
            j = j - k;
            k = k / 2;
        }
        j = j + k;
    }

    f = num;

    for(l = 1; (f=f/2)!=1; l++);

    for( m = 1; m <= l; m++)
    {
        le = 2 << (m - 1);
        lei = le / 2;
        u.fp_Re = FP_VALUE(1);
        u.fp_Im = 0;
        w.fp_Re = FP_VALUE(cos(M_PI / lei));    //  here can look up table to speed up
        w.fp_Im = FP_VALUE(-sin(M_PI / lei));
        for(j = 0; j < lei; j++)
        {
            for(i = j; i < num; i = i + le)
            {
                ip = i + lei;
                EE_fp(&(vect[ip]), &u,&t);

                vect[ip].fp_Re = vect[i].fp_Re - t.fp_Re;
                vect[ip].fp_Im = vect[i].fp_Im - t.fp_Im;
                vect[i].fp_Re = vect[i].fp_Re + t.fp_Re;
                vect[i].fp_Im = vect[i].fp_Im + t.fp_Im;
            }
            EE_fp(&u, &w,&u);
        }
    }
}


void iFFT(complex *vect, int num)
{
    int f , m, nv2, nm1, i, k, l, j = 0;
    complex u, w, t;
    int le , lei, ip;

    nv2 = num / 2;
    nm1 = num - 1;
    for(i = 0; i < nm1; i++)
    {
        if(i < j)
        {
            t = vect[j];
            vect[j] = vect[i];
            vect[i] = t;
        }
        k = nv2;
        while( k <= j)
        {
            j = j - k;
            k = k / 2;
        }
        j = j + k;
    }

    f = num;
    for (l = 1; (f = f / 2) != 1; l++);

    for( m = 1; m <= l; m++)
    {
        le = 2 << (m - 1);
        lei = le / 2;
        u.Re = 1.0;
        u.Im = 0.0;
        w.Re = cos(M_PI / lei);          //  here can look up table to speed up
        w.Im = sin(M_PI / lei);
        for(j = 0; j <lei; j++)
        {
            for(i = j; i <num; i = i + le)
            {
                ip = i + lei;
                EE(&(vect[ip]), &u,&t);
                vect[ip].Re = vect[i].Re - t.Re;
                vect[ip].Im = vect[i].Im - t.Im;
                vect[i].Re = vect[i].Re + t.Re;
                vect[i].Im = vect[i].Im + t.Im;
            }
            EE(&u, &w,&u);
        }
    }
}

void iFFT_fp(fp_complex *vect, int num)
{
    int f , m, nv2, nm1, i, k, l, j = 0;
    fp_complex u, w, t;
    int le , lei, ip;

    nv2 = num / 2;
    nm1 = num - 1;
    for(i = 0; i < nm1; i++)
    {
        if(i < j)
        {
            t = vect[j];
            vect[j] = vect[i];
            vect[i] = t;
        }
        k = nv2;
        while( k <= j)
        {
            j = j - k;
            k = k / 2;
        }
        j = j + k;
    }

    f = num;
    for (l = 1; (f = f / 2) != 1; l++);

    for( m = 1; m <= l; m++)
    {
        le = 2 << (m - 1);
        lei = le / 2;
        u.fp_Re = FP_VALUE(1);
        u.fp_Im = 0;
        w.fp_Re = FP_VALUE(cos(M_PI / lei));          //  here can look up table to speed up
        w.fp_Im = FP_VALUE(sin(M_PI / lei));
        for(j = 0; j <lei; j++)
        {
            for(i = j; i <num; i = i + le)
            {
                ip = i + lei;
                EE_fp(&(vect[ip]), &u,&t);
                vect[ip].fp_Re = vect[i].fp_Re - t.fp_Re;
                vect[ip].fp_Im = vect[i].fp_Im - t.fp_Im;
                vect[i].fp_Re = vect[i].fp_Re + t.fp_Re;
                vect[i].fp_Im = vect[i].fp_Im + t.fp_Im;
            }
            EE_fp(&u, &w,&u);
        }
    }
}


void FFT2D(complex* Input, complex* output,int num)
{
    unsigned int i, j;

    //for each row 1d fft
    for(i = 0; i < num; i++)
    {
        FFT(Input+(i*num), num);
        for(j=0;j<num;j++)
        {
            output[j*num+i] = Input[i*num+j];
        }
    }

    //for each col 1d fft
    for(i = 0; i < num; i++)
    {
        FFT(output+(i*num), num);
    }
}

void FFT2D_fp(fp_complex* Input, fp_complex* output,int num)
{
    unsigned int i, j;

    //for each row 1d fft
    for(i = 0; i < num; i++)
    {
        FFT_fp(Input+(i*num), num);
        for(j=0;j<num;j++)
        {
            output[j*num+i] = Input[i*num+j];
        }
    }

    //for each col 1d fft
    for(i = 0; i < num; i++)
    {
        FFT_fp(output+(i*num), num);
    }
}

void iFFT2D_transpose(complex* Input, complex* output,unsigned int num)
{
    unsigned int i, j;
    unsigned int div = num*num;

    //for each row 1d fft
    for(i = 0; i < num; i++)
    {
        iFFT(Input+(i*num), num);
        for(j=0;j<num;j++)
        {
            output[j*num+i] = Input[i*num+j];
        }
    }

    //for each col 1d fft
    for(i = 0; i < num; i++)
    {
        iFFT(output+(i*num), num);
    }

    for(i = 0; i < num; i++)
    {
        for(j = 0; j < num; j++)
        {
            output[i*num+j].Re /= div;
        }
    }
}


void iFFT2D_transpose_fp(fp_complex* Input, fp_complex* output,unsigned int num)
{
    unsigned int i, j;
    unsigned int div = num*num;

    //for each row 1d fft
    for(i = 0; i < num; i++)
    {
        iFFT_fp(Input+(i*num), num);
        for(j=0;j<num;j++)
        {
            output[j*num+i] = Input[i*num+j];
        }
    }

    //for each col 1d fft
    for(i = 0; i < num; i++)
    {
        iFFT_fp(output+(i*num), num);
    }

    for(i = 0; i < num; i++)
    {
        for(j = 0; j < num; j++)
        {
            output[i*num+j].fp_Re /= div;
        }
    }
}


float bilinear_function_4(float a0,float a1,float a2,float a3,float d)
{
    float x1,x2,x;

    x1 = a0+d*(a1-a0);
    x2 = a2+d*(a3-a2);
    x = x1+d*(x2-x1);

    return x;
}

int bilinear_function_4_fp(unsigned int a0,unsigned int a1,unsigned int a2,unsigned int a3,unsigned int fp_d)
{
    int fp_x1,fp_x2,fp_x;

    fp_x1 = FP_VALUE(a0)+fp_d*(a1-a0);
    fp_x2 = FP_VALUE(a2)+fp_d*(a3-a2);
    fp_x = fp_x1+FP_MULTI(fp_d,(fp_x2-fp_x1));

    return fp_x;
}


float bilinear_function_2(float a0,float a1,float d)
{
    float x = a0+d*(a1-a0);

    return x;
}

void bilinear_image_fp(unsigned short* image,int* fp_out,unsigned int* out_tmp,unsigned int ori_size,unsigned int resize)
{
    unsigned int i,j;
    unsigned int tmp = resize+1;
    unsigned int tmp1 = resize-1;

    if(resize == 100)
    {
        for(i=0;i<resize;i++)
        {
            for(j=0;j<resize;j++)
            {
                fp_out[i*resize +j] = bilinear_function_4_fp((image[(i*2)*ori_size+j*2]),
                                                             (image[(i*2)*ori_size+j*2+1]),
                                                             (image[(i*2+1)*ori_size+j*2]),
                                                             (image[(i*2+1)*ori_size+j*2+1]),
                                                             0x8000);//0x8000 is 0.5's fix pointed value
            }
        }
    }
#if 0
    else //resize == 200
    {
        for(i=0;i<ori_size;i++)
        {
            for(j=0;j<ori_size;j++)
            {
                out_tmp[(i*2+1)*tmp+j*2+1] = (float)(image[i*ori_size+j]);
            }
        }

        for(i=1;i<resize;i+=2)
        {
            for(j=2;j<tmp1;j+=2)
            {
                out_tmp[i*tmp+j] = bilinear_function_2(out_tmp[i*tmp+j-1],out_tmp[i*tmp+j+1],1);
            }
        }

        for(j=1;j<resize;j+=2)
        {
            for(i=2;i<tmp1;i+=2)
            {
                out_tmp[i*tmp+j] = bilinear_function_2(out_tmp[(i-1)*tmp+j],out_tmp[(i+1)*tmp+j],1);
            }
        }

        for(i=2;i<tmp1;i+=2)
        {
            for(j=2;j<tmp1;j+=2)
            {
                out_tmp[i*tmp+j] = bilinear_function_2(out_tmp[i*tmp+j-1],out_tmp[i*tmp+j+1],1);
            }
        }

        for(i=0;i<resize;i++)
        {
            for(j=0;j<resize;j++)
            {
                out[i*resize+j] = bilinear_function_4(out_tmp[i*tmp+j],out_tmp[i*tmp+j+1],out_tmp[(i+1)*tmp+j],out_tmp[(i+1)*tmp+j+1],0.5);
            }
        }
    }
#endif
}

void bilinear_image(unsigned short* image,float* orien,float* out,float* out_tmp,unsigned int ori_size,unsigned int resize)
{
    unsigned int i,j;
    unsigned int tmp = resize+1;
    unsigned int tmp1 = resize-1;

    if(resize == 100)
    {
        if(image != NULL)
        {
            for(i=0;i<resize;i++)
            {
                for(j=0;j<resize;j++)
                {
                    out[i*resize +j] = bilinear_function_4((float)(image[(i*2)*ori_size+j*2]),(float)(image[(i*2)*ori_size+j*2+1]),(float)(image[(i*2+1)*ori_size+j*2]),(float)(image[(i*2+1)*ori_size+j*2+1]),0.5);
                }
            }
        }
        else
        {
            for(i=0;i<resize;i++)
            {
                for(j=0;j<resize;j++)
                {
                    out[i*resize +j] = bilinear_function_4(orien[(i*2)*ori_size+j*2],orien[(i*2)*ori_size+j*2+1],orien[(i*2+1)*ori_size+j*2],orien[(i*2+1)*ori_size+j*2+1],0.5);
                }
            }
        }
    }
    else //resize == 200
    {
        if(image != NULL)
        {
            for(i=0;i<ori_size;i++)
            {
                for(j=0;j<ori_size;j++)
                {
                    out_tmp[(i*2+1)*tmp+j*2+1] = (float)(image[i*ori_size+j]);
                }
            }
        }
        else
        {
            for(i=0;i<ori_size;i++)
            {
                for(j=0;j<ori_size;j++)
                {
                    out_tmp[(i*2+1)*tmp+j*2+1] = orien[i*ori_size+j];
                }
            }
        }

        for(i=1;i<resize;i+=2)
        {
            for(j=2;j<tmp1;j+=2)
            {
                out_tmp[i*tmp+j] = bilinear_function_2(out_tmp[i*tmp+j-1],out_tmp[i*tmp+j+1],1);
            }
        }

        for(j=1;j<resize;j+=2)
        {
            for(i=2;i<tmp1;i+=2)
            {
                out_tmp[i*tmp+j] = bilinear_function_2(out_tmp[(i-1)*tmp+j],out_tmp[(i+1)*tmp+j],1);
            }
        }

        for(i=2;i<tmp1;i+=2)
        {
            for(j=2;j<tmp1;j+=2)
            {
                out_tmp[i*tmp+j] = bilinear_function_2(out_tmp[i*tmp+j-1],out_tmp[i*tmp+j+1],1);
            }
        }

        for(i=0;i<resize;i++)
        {
            for(j=0;j<resize;j++)
            {
                out[i*resize+j] = bilinear_function_4(out_tmp[i*tmp+j],out_tmp[i*tmp+j+1],out_tmp[(i+1)*tmp+j],out_tmp[(i+1)*tmp+j+1],0.5);
            }
        }
    }
}

