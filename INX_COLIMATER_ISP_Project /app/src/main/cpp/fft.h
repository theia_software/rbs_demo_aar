#ifndef FFT_H
#define FFT_H

#define SHIFT_AMOUNT     16
#define SHIFT_MASK       ((1 << SHIFT_AMOUNT) - 1)
#define FP_VALUE(A)      ((int)((A)*(1<<SHIFT_AMOUNT)))
#define FP_INT_VALUE(A)      ((int)((A)<<SHIFT_AMOUNT))
#define FP_ADD(A,B)      ((A)+(B))
#define FP_SUBTRACT(A,B) ((A)-(B))
#define FP_MULTI(A,B)    ((int)((((int long long)(A))*((int long long)(B)))>>SHIFT_AMOUNT))
#define FP_MULTI_NO_SHIFT(A,B)    ((int long long)((((int long long)(A))*((int long long)(B)))))
#define FP_SHIFT(A)       ((A)>>SHIFT_AMOUNT)
#define FP_DIV(A,B)      ((A)/(B))
#define FP_INT(A)        ((float)((A)>>SHIFT_AMOUNT))
#define FP_FRAC(A)       ((float)(((A) & SHIFT_MASK) / (1 << SHIFT_AMOUNT)))
#define FP_TO_NONFP(A)   (FP_INT((A))+FP_FRAC((A)))

//struct
typedef struct _my_complex
{
    double Re;
    double Im;
} complex;


typedef struct _fp_complex
{
    int fp_Re;
    int fp_Im;
} fp_complex;


typedef struct
{
    unsigned int mirror_extra_half_size;
    unsigned int fft_size;
    complex* kernel_in;
    complex* kernel_out;
    fp_complex* fp_kernel_in;
    fp_complex* fp_kernel_out;
    float* kernel_reference;
    int* fp_kernel_reference;
    unsigned int kernel_reference_size;
    complex* raw1_in;
    complex* raw1_out;
    fp_complex* fp_raw1_in;
    fp_complex* fp_raw1_out;
    float* raw1_reference;
    int* fp_raw1_reference;
    unsigned int raw1_reference_size;
    complex* raw2_in;
    complex* raw2_out;
    fp_complex* fp_raw2_in;
    fp_complex* fp_raw2_out;
    float* raw2_reference;
    int* fp_raw2_reference;
    unsigned int raw2_reference_size;
    complex* raw3_in;
    complex* raw3_out;
    fp_complex* fp_raw3_in;
    fp_complex* fp_raw3_out;
    float* raw3_reference;
    int* fp_raw3_reference;
    unsigned int raw3_reference_size;
    float* out1;
    float* out2;
    float* out3;
    int* fp_out1;
    int* fp_out2;
    int* fp_out3;
} fft_data;

//function
void EE(complex* a, complex* b,complex* c);
void EE_fp(fp_complex* a, fp_complex* b,fp_complex* c);
void FFT2D(complex* Input, complex* output,int num);
void FFT2D_fp(fp_complex* Input, fp_complex* output,int num);
void iFFT2D_transpose(complex* Input, complex* output,unsigned int num);
void iFFT2D_transpose_fp(fp_complex* Input, fp_complex* output,unsigned int num);
float bilinear_function_4(float a0,float a1,float a2,float a3,float d);
int bilinear_function_4_fp(unsigned int a0,unsigned int a1,unsigned int a2,unsigned int a3,unsigned int fp_d);
float bilinear_function_2(float a0,float a1,float d);
void bilinear_image_fp(unsigned short* image,int* fp_out,unsigned int* out_tmp,unsigned int ori_size,unsigned int resize);
void bilinear_image(unsigned short* image,float* orien,float* out,float* out_tmp,unsigned int ori_size,unsigned int resize);

#endif

