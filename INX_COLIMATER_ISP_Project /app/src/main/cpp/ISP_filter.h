//
//  ISP_filter.h
//  ET713_Image_Preprocessing
//
//  Created by 黃詠淮 on 2019/1/28.
//  Copyright © 2019 黃詠淮. All rights reserved.
//

#ifndef ISP_filter_h
#define ISP_filter_h


float *_pad_array(float *img, int height, int width, int p_y_size, int p_x_size);
void mean_filter(float *img, int width, int height, int k_size);
void _sym_separable_filter(float *img, int width, int height, float *half_filter, int f_radius);
void _band_pass_filter(float *img, int width, int height, int f_radius1, int f_radius2);
void remove_impulse_noise(float *img, int width, int height, float threshold);
void remove_impulse_noise_1(float *img, int width, int height);
void wiener_filter(float *img, int width, int height, int k_size, float noise);


#endif /* ISP_filter_h */
