#include "statistics.h"
#include <stdlib.h>
#include <string.h>
#include "ET760_Collimator_ISP.h"
#include "kiss_fft.h"
#include "kiss_fftr.h"
#include "kiss_fftndr.h"
#include "kiss_fftnd.h"
#include "ISP_filter.h"
#include "ISP_normalize.h"
#include "clahe.h"
#include "quality_level.h"
#define LIB_VERSION "IPP: 1.2 (Temp Fix Compile Error), BKG: None, SNR: None"

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

char* check_version(){
    return LIB_VERSION;
}



void remove_noise_frequency_domain(kiss_fft_cpx *fimg, int width, int height)
{
    int i, j, k, l, size = width * height;
    float *filter = (float*)malloc(sizeof(float) * size);
    float *temp1 = (float*)malloc(sizeof(float) * size);
    float *temp2 = (float*)malloc(sizeof(float) * size);
    float kernel[] = {0.0545, 0.2442, 0.4026};
    float threshold = 3.5f;
    float real, imag, f;
    
    
    for (i = 0; i < size; i++) filter[i] = 1.0f;
    
    
    for (i = 0; i < 2; i++) {
        for (j = 5; j < width - 6; j++) {
            //filter[i * width + j] = 0.5f;
            //filter[(height - 1 - i) * width + j] = 0.5f;
            filter[i * width + j] = 0.0f;//0.2f;
            filter[(height - 1 - i) * width + j] = 0.0f;//0.2f;

        }
    }
    
    for (i = 5; i < height - 6; i++) {
        for (j = 0; j < 2; j++) {
            //filter[i * width + j] = 0.2f;
            //filter[i * width + (width - 1 - j)] = 0.2f;
            filter[i * width + j] = 0.0f;//0.2f;
            filter[i * width + (width - 1 - j)] = 0.0f;//0.2f;
        }
    }
    
    
    f = 1.0f / (width * height);
    for (i = 0; i < size; i++){
        real = fimg[i].r * f;
        imag = fimg[i].i * f;
        temp1[i] = temp2[i] = sqrt(real * real + imag * imag);
    }
    
    mean_filter(temp2, width, height, 5);
    
    _sym_separable_filter(filter, width, height, kernel, 2);
    for (i = 0; i < size; i++){
        fimg[i].r *= filter[i];
        fimg[i].i *= filter[i];
    }
    
    
    for (i = 0; i < size; i++){
        if (temp2[i] < 0.000001f) {
            temp1[i] = 0;
        }else{
            temp1[i] = temp1[i] / ((temp2[i] * 25 - temp1[i]) / 24);
        }
    }
    
    for (i = 0; i < size; i++) filter[i] = 1.0f;


    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (temp1[i*width+j] > threshold) {
                for (k = i - 1; k <= i + 1; k++) {
                    for (l = j - 1; l <= j + 1; l++) {
                        if (k >= 0 && k <= height - 1 && l >= 0 && l <= width - 1) {
                            filter[k * width + l] = 0.0f;
                        }
                    }
                }
            }
        }
    }


    _sym_separable_filter(filter, width, height, kernel, 2);

    for (i = 0; i < size; i++){
        fimg[i].r *= filter[i];
        fimg[i].i *= filter[i];
    }
    
    free(filter);
    free(temp1);
    free(temp2);
}


int num_power_of_2(int num)
{
    int i;
    for (i = 1; i <= num; i = i * 2);
    
    return i;
}



void sort(float *data, int len)
{
    int i,j;
    float key;
    for (i=1;i<len;i++){
        key = data[i];
        j = i - 1;
        while((j>=0) && (data[j]>key)) {
            data[j+1] = data[j];
            j--;
        }
        data[j+1] = key;
    }
}



void col_mean_alignment(float *img, int width, int height)
{
    
    int i, j;
    float *col = (float*)malloc(sizeof(float) * height);
    float mean;
    
    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
            col[j] = img[j * width + i];
        }
        mean = _cal_mean(col, height);
        
        for (j = 0; j < height; j++) {
            img[j * width + i] -= mean;
        }
    }
    
    free(col);
}

void row_mean_alignment(float *img, int width, int height)
{
    
    int i, j;
    float *row = (float*)malloc(sizeof(float) * width);
    float mean;
    
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            row[j] = img[i * width + j];
        }
        mean = _cal_mean(row, width);
        
        for (j = 0; j < width; j++) {
            img[i * width + j] -= mean;
        }
    }
    
    free(row);
}



void ET760_Collimator_ISP(int *input_img, int *bkg, unsigned char *result_img, int width, int height)
{
    
    int i, j;
    int size = width * height;
    float *img = (float*)malloc(sizeof(float) * size);
    float *img1 = (float*)malloc(sizeof(float) * size);
    float mean, std;
    unsigned char *map = (unsigned char*)malloc(sizeof(unsigned char) * (width > height ? width : height));
    //unsigned char *map1 = (unsigned char*)malloc(sizeof(unsigned char) * size);
    
    int width_fft;
    int height_fft;
    int fft_size;
    int fft_dim[2];
    
    kiss_fft_cpx *freqdata;
    kiss_fft_cpx *timedata;
    
    kiss_fftnd_cfg config_f;
    kiss_fftnd_cfg config_i;
    
    
    
    //Subtract background
    if (bkg) {
        for (i = 0; i < size; i++) {
            img[i] = (float)(input_img[i] - bkg[i]);
        }
    }
    else
    {
        for (i = 0; i < size; i++) {
            img[i] = (float)input_img[i];
        }
    }
    

    col_mean_alignment(img, width, height);
    row_mean_alignment(img, width, height);


    linear_normalize(img, size, 1.0f, 0.0f);
    wiener_filter(img, width, height, 7, 0.025);
    
    //Frequency domain filtering for removing various types of noise from image
    int pad_x = 64, pad_y = 32;
    int p_height = height + pad_y * 2;
    int p_width = width + pad_x * 2;

    width_fft =  kiss_fft_next_fast_size(p_width);
    height_fft = kiss_fft_next_fast_size(p_height);

    //width_fft = num_power_of_2(width);
    //height_fft = num_power_of_2(height);


    fft_size = width_fft * height_fft;
    fft_dim[0] = height_fft;
    fft_dim[1] = width_fft;

    float *img_pad = _pad_array(img, height, width, pad_y, pad_x);

    freqdata =  (kiss_fft_cpx *)calloc(height_fft * (width_fft), sizeof(kiss_fft_cpx) ) ;
    timedata =  (kiss_fft_cpx *)calloc(fft_size, sizeof(kiss_fft_cpx));


    for (i = 0; i < p_height; i++) {
        for (j = 0; j < p_width; j++) {
            timedata[i * width_fft + j].r = img_pad[i * p_width + j] ;
            timedata[i * width_fft + j].i = 0;
        }
    }

    config_f = kiss_fftnd_alloc(fft_dim, 2, 0, 0, 0);
    kiss_fftnd(config_f, timedata,freqdata);



    remove_noise_frequency_domain(freqdata, width_fft, height_fft);

    config_i = kiss_fftnd_alloc(fft_dim, 2, 1, 0, 0);

    kiss_fftnd(config_i, freqdata, timedata);


    for (i = pad_y; i < height + pad_y; i++) {
        for (j = pad_x; j < width + pad_x; j++) {
            img[(i-pad_y) * width + (j-pad_x)] = timedata[i * width_fft + j].r;
        }
    }

    free(img_pad);
    kiss_fft_free(timedata);
    kiss_fft_free(freqdata);
    kiss_fft_free(config_f);
    kiss_fft_free(config_i);

    //Successive (4-pass) band-pass/band-enhance process
    //The 1st pass
    _band_pass_filter(img, width, height, 1, 3);
    mean_filter(img, width, height, 3);

    //The 2nd pass
    memcpy(img1, img, sizeof(float) * size);
    _band_pass_filter(img1, width, height, 2, 4);

    for (i = 0; i < size; i++) {
        img[i] += img1[i] * 5.0f;
    }
    //mean_filter(img, width, height, 3);


    //The 3rd pass
    memcpy(img1, img, sizeof(float) * size);
    _band_pass_filter(img1, width, height, 3, 5);

    for (i = 0; i < size; i++) {
        img[i] += img1[i] * 5.0f;
    }
    //mean_filter(img, width, height, 3);

    //The 4rd pass
    memcpy(img1, img, sizeof(float) * size);
    _band_pass_filter(img1, width, height, 4, 6);

    for (i = 0; i < size; i++) {
        img[i] += img1[i] * 5.0f;
    }
    mean_filter(img, width, height, 3);




    //discard the pixel values outside three standard deviations & perform linear normalize
    _cal_mean_std(img, size, &mean, &std);
    _drop_data_out_of_range(img, size, mean + 3 * std, mean - 3 * std);
    linear_normalize(img, size, 255.0f, 0.0f);



    for (i = 0; i < size; i++) {
        result_img[i] = (unsigned char)(img[i] + 0.5f);
    }



    //Perform contrast limited adaptive histogram equalization to enhance the contrast of image
    CLAHE (result_img, width, height, 0, 255, 3, 3, 256, 4);


    for (i = 0; i < size; i++) {
        result_img[i] = (unsigned char)(result_img[i]  * 0.25f + img[i] * 0.75f + 0.5f);
    }

    
    kiss_fft_cleanup();
    free(img);
    free(img1);
    free(map);
 
}




void ET760_Collimator_ISP_binning(int *input_img, int *bkg, unsigned char *result_img, int width, int height)
{
    
    int i, j;
    int size = width * height;
    float *img = (float*)malloc(sizeof(float) * size);
    float *img1 = (float*)malloc(sizeof(float) * size);
    float mean, std;
    unsigned char *map = (unsigned char*)malloc(sizeof(unsigned char) * (width > height ? width : height));
    //unsigned char *map1 = (unsigned char*)malloc(sizeof(unsigned char) * size);
    
    int width_fft;
    int height_fft;
    int fft_size;
    int fft_dim[2];
    
    kiss_fft_cpx *freqdata;
    kiss_fft_cpx *timedata;
    
    kiss_fftnd_cfg config_f;
    kiss_fftnd_cfg config_i;
    
    
    
    //Subtract background
    if (bkg) {
        for (i = 0; i < size; i++) {
            img[i] = (float)(input_img[i] - bkg[i]);
        }
    }
    else
    {
        for (i = 0; i < size; i++) {
            img[i] = (float)input_img[i];
        }
    }
    

    //col_mean_alignment(img, width, height);
    //row_mean_alignment(img, width, height);


    linear_normalize(img, size, 1.0f, 0.0f);
    wiener_filter(img, width, height, 3, 0.025);

    //Frequency domain filtering for removing various types of noise from image
    int pad_x = 64, pad_y = 32;
    int p_height = height + pad_y * 2;
    int p_width = width + pad_x * 2;

    width_fft =  kiss_fft_next_fast_size(p_width);
    height_fft = kiss_fft_next_fast_size(p_height);

    //width_fft = num_power_of_2(width);
    //height_fft = num_power_of_2(height);


    fft_size = width_fft * height_fft;
    fft_dim[0] = height_fft;
    fft_dim[1] = width_fft;

    float *img_pad = _pad_array(img, height, width, pad_y, pad_x);

    freqdata =  (kiss_fft_cpx *)calloc(height_fft * (width_fft), sizeof(kiss_fft_cpx) ) ;
    timedata =  (kiss_fft_cpx *)calloc(fft_size, sizeof(kiss_fft_cpx));


    for (i = 0; i < p_height; i++) {
        for (j = 0; j < p_width; j++) {
            timedata[i * width_fft + j].r = img_pad[i * p_width + j] ;
            timedata[i * width_fft + j].i = 0;
        }
    }

    config_f = kiss_fftnd_alloc(fft_dim, 2, 0, 0, 0);
    kiss_fftnd(config_f, timedata,freqdata);



    remove_noise_frequency_domain(freqdata, width_fft, height_fft);

    config_i = kiss_fftnd_alloc(fft_dim, 2, 1, 0, 0);

    kiss_fftnd(config_i, freqdata, timedata);


    for (i = pad_y; i < height + pad_y; i++) {
        for (j = pad_x; j < width + pad_x; j++) {
            img[(i-pad_y) * width + (j-pad_x)] = timedata[i * width_fft + j].r;
        }
    }

    free(img_pad);
    kiss_fft_free(timedata);
    kiss_fft_free(freqdata);
    kiss_fft_free(config_f);
    kiss_fft_free(config_i);




    memcpy (img1, img, sizeof(float) * size);
    mean_filter(img1, width, height, 3);
    for (i = 0; i < size; i++) {
        //img[i] += (img[i] - img1[i]) * 5.0f;
        img[i] = (img[i] - img1[i]) * 5.0f;
    }
    //mean_filter(img, width, height, 3);

    memcpy(img1, img, sizeof(float) * size);
    _band_pass_filter(img1, width, height, 1, 2);
    for (i = 0; i < size; i++) {
        img[i] += img1[i] * 5.0f;
    }
    //mean_filter(img, width, height, 3);


    memcpy(img1, img, sizeof(float) * size);
    _band_pass_filter(img1, width, height, 2, 3);
    for (i = 0; i < size; i++) {
        img[i] += img1[i] * 5.0f;
    }
    //mean_filter(img, width, height, 3);

    memcpy(img1, img, sizeof(float) * size);
	_band_pass_filter(img1, width, height, 2, 4);
    for (i = 0; i < size; i++) {
        img[i] += img1[i] * 5.0f;
    }
    mean_filter(img, width, height, 3);



    //discard the pixel values outside three standard deviations & perform linear normalize
    _cal_mean_std(img, size, &mean, &std);
    _drop_data_out_of_range(img, size, mean + 3 * std, mean - 3 * std);
    linear_normalize(img, size, 255.0f, 0.0f);



    for (i = 0; i < size; i++) {
        result_img[i] = (unsigned char)(img[i] + 0.5f);
    }



    //Perform contrast limited adaptive histogram equalization to enhance the contrast of image
    CLAHE (result_img, width, height, 0, 255, 3, 3, 256, 4);


    for (i = 0; i < size; i++) {
        result_img[i] = (unsigned char)(result_img[i]  * 0.25f + img[i] * 0.75f + 0.5f);
    }

    
    kiss_fft_cleanup();
    free(img);
    free(img1);
    free(map);
 
}

