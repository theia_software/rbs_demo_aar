
#ifndef bkg_manage_h
#define bkg_manage_h

#include <stdio.h>


typedef struct{
    int *bkg_set;
    int *frequency;
    int *use_time_stamp;
    int *exist_time_stamp;
    int *not_empty;
    int width;
    int height;
    int bkg_num;
    int max_bkg_num;
}BKG_DATA;

BKG_DATA *init_bkg_data(int *init_bkg, int width, int height, int max_bkg_num);
void free_bkg_data(BKG_DATA *bkg_data);
unsigned char *encode_bkg_data_stream(BKG_DATA *bkg_data, int *data_size);
BKG_DATA *decode_bkg_data_stream(unsigned char * data_stream);
int subtract_bkg(int *img, BKG_DATA *bkg_data, int *best_bkg);
#endif /* bkg_manage_h */
