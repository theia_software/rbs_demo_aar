//
//  orientation.c
//  ISP
//
//  Created by Luan Liu on 2019/10/28.
//  Copyright © 2019 Theia. All rights reserved.
//

#include "orientation.h"

void amplifySignal(float* clean_signal, float* raw_signal, float* offset, int length){
    float* zero_mean_clean_signal_abs = (float*)malloc(length * sizeof(float));
    float* zero_mean_raw_signal_abs = (float*)malloc(length * sizeof(float));
    
    for(int i = 0; i < length; i++){
        zero_mean_clean_signal_abs[i] = fabsf(clean_signal[i] - offset[i]);
        zero_mean_raw_signal_abs[i]   = fabsf(raw_signal[i]   - offset[i]);
    }
    
    float *peaks;
    int *locs, peakNum;
    findpeaksf(zero_mean_clean_signal_abs, length, &peaks, &locs, &peakNum, 5);
    float clean_peak_mean = array_meanf(peaks, peakNum);
    free(locs);
    free(peaks);
    
    findpeaksf(zero_mean_raw_signal_abs, length, &peaks, &locs, &peakNum, 5);
    float raw_peak_mean = array_meanf(peaks, peakNum);
    //printf("clean_peak_mean = %f, raw_peak_mean = %f\n", clean_peak_mean, raw_peak_mean);
    
    float scale = raw_peak_mean / clean_peak_mean;
    for(int i = 0; i < length; i++){
        clean_signal[i] = (clean_signal[i] - offset[i]) * scale + offset[i];
    }
}

void MeanFilter(int** signal, int length, int kernel_size){
    int* result = (int*)malloc(length*sizeof(int));
    int kernel_radius = kernel_size / 2;
    float weight = 1.0f / kernel_size;
    
    for(int i = 0; i < length; i++){
        float value = 0.0;
        for(int j = 0; j < kernel_size; j++){
            int img_x = i + (j - kernel_radius);
            if(img_x >= 0 && img_x < length)
                value += (*signal)[img_x] * weight;
            else if(img_x < 0)
                value += (*signal)[-img_x - 1] * weight;
            else
                value += (*signal)[2 * length - img_x - 1] * weight;
        }
        result[i] = (int)roundf(value);
    }
    free(*signal);
    *signal = result;
}

void MeanFilterf(float** signal, int length, int kernel_size){
    float* result = (float*)malloc(length*sizeof(float));
    int kernel_radius = kernel_size / 2;
    float weight = 1.0f / kernel_size;
    
    for(int i = 0; i < length; i++){
        float value = 0.0;
        for(int j = 0; j < kernel_size; j++){
            int img_x = i + (j - kernel_radius);
            if(img_x >= 0 && img_x < length)
                value += (*signal)[img_x] * weight;
            else if(img_x < 0)
                value += (*signal)[-img_x - 1] * weight;
            else
                value += (*signal)[2 * length - img_x - 1] * weight;
        }
        result[i] = value;
    }
    free(*signal);
    *signal = result;
}

float* GaussFilter(float* signal, int length, float std){
    int kernel_size = 2 * ceil(2 * std) + 1;
    int kernel_radius = ceil(2 * std);
    float* weight = (float*)malloc(kernel_size * sizeof(float));
    float total = 0.0f;
    for(int i = 0; i < kernel_size; i++){
        weight[i] = exp(-pow(i - kernel_radius, 2) / (2 * std * std));
        total += weight[i];
    }
    for(int i = 0; i < kernel_size; i++)
        weight[i] /= total;
    
    float* result = (float*)malloc(length * sizeof(float));
    for(int i = 0; i < length; i++){
        float value = 0.0;
        for(int j = 0; j < kernel_size; j++){
            int img_x = i + (j - kernel_radius);
            if(img_x >= 0 && img_x < length)
                value += signal[img_x] * weight[j];
            else if(img_x < 0)
                value += signal[-img_x - 1] * weight[j];
            else
                value += signal[2 * length - img_x - 1] * weight[j];
        }
        result[i] = value;
    }
    return result;
}

float* InvertGaussFilter(float* signal, int length, float std){
    int kernel_size = 2 * ceil(2 * std) + 1;
    int kernel_radius = ceil(2 * std);
    float* weight = (float*)malloc(kernel_size * sizeof(float));
    float total = 0.0f;
    for(int i = 0; i < kernel_size; i++){
        weight[i] = 1 - exp(-pow(i - kernel_radius, 2) / (2 * std * std));
        total += weight[i];
    }
    for(int i = 0; i < kernel_size; i++)
        weight[i] /= total;
    
    float* result = (float*)malloc(length * sizeof(float));
    for(int i = 0; i < length; i++){
        float value = 0.0;
        for(int j = 0; j < kernel_size; j++){
            int img_x = i + (j - kernel_radius);
            if(img_x >= 0 && img_x < length)
                value += signal[img_x] * weight[j];
            else if(img_x < 0)
                value += signal[-img_x - 1] * weight[j];
            else
                value += signal[2 * length - img_x - 1] * weight[j];
        }
        result[i] = value;
    }
    return result;
}

float* FindSignalTrend(float* signal, int length, float period){
    int kernel_size = (int)roundf(period * 3.0 / 2.0);
    int kernel_radius = (int)roundf(period * 3.0 / 4.0);
    float* result = (float*)malloc(length * sizeof(float));
    
    for(int i = 0; i < length; i++){
        int img_x1 = i - kernel_radius;
        if(img_x1 < 0)
            img_x1 = -img_x1 - 1;

        int img_x2 = i - kernel_radius + kernel_size - 1;
        if(img_x2 > length - 1)
            img_x2 = 2 * length - img_x2 - 1;
        
        result[i] = (signal[img_x1] + signal[img_x2]) * 0.5;
    }
    return result;
}

void CalAngleHist(int* hist, int bin_num, float* pixel_ort, int pixel_num){
    memset(hist, 0, bin_num * sizeof(int));
    int value;
    for(int i = 0; i< pixel_num; i++){
        value = (int)roundf(pixel_ort[i] * 180.0 / 3.14159265358979323846);
        hist[value]++;
    }
}

int FindMax(int* array, int length){
    int max = array[0];
    int peak = 0;
    for(int i = 0; i < length; i++){
        if(max < array[i]){
            max = array[i];
            peak = i;
        }
    }
    return peak;
}

float* ones(int height, int width){
    int pixel_num = height * width;
    float *result = (float*)malloc(height * width * sizeof(float));
    
    for(int i = 0; i < pixel_num; i++)
        result[i] = 1.0f;
    
    return result;
}

BYTE* imrotate_full(BYTE* image, int height, int width, float angle_degree, int *out_height, int *out_width){
    float diagonal = sqrt(height * height + width * width);
    float theta0 = atan((float)height / (float)width);
    float theta1 = angle_degree * PI / 180.0;
    if(angle_degree >= 0.0f && angle_degree <= 90.0f){
        *out_height = (int)ceilf(diagonal * sin(theta0 + theta1));
        *out_width = (int)ceilf(diagonal * sin(theta0 - theta1 + PI / 2));
    }
    else if(angle_degree >= -90.0f && angle_degree <= 0.0f){
        *out_height = (int)ceilf(diagonal * sin(theta0 - theta1));
        *out_width = (int)ceilf(diagonal * sin(theta0 + theta1 + PI / 2));
    }
    else{
        printf("angle beyond -90 ~ +90 degrees is not supported");
        *out_height = 0;
        *out_width = 0;
        return NULL;
    }
    
    int full_height = *out_height;
    int full_width = *out_width;
    BYTE* out = (BYTE*)calloc(full_width * full_height, sizeof(BYTE));
    
    float center_x = (width - 1) / 2.0;
    float center_y = (height - 1) / 2.0;
    float center_x_out = (*out_width - 1) / 2.0;
    float center_y_out = (*out_height - 1) / 2.0;
    
    float x, y; // coordinates before rotation
    float alpha, beta;
    int x0, x1, y0, y1;
    BYTE Q11, Q12, Q21, Q22;
    
    float cosine = cos(theta1);
    float sine = sin(theta1);
    
    for(int i = 0; i < full_height; i++){
        for(int j = 0; j < full_width; j++){
            x = center_x + cosine * (j - center_x_out) - sine  * (i - center_y_out);
            y = center_y + sine  * (j - center_x_out) + cosine * (i - center_y_out);
            
            if(x <= -1.0 || y <= -1.0 || x >= (float)width || y >= (float)height){
                out[i * full_width + j] = 0;
                continue;
            }
            
            alpha = x - floorf(x);
            beta  = y - floorf(y);
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            y0 = (int)floorf(y);
            y1 = (int)ceilf(y);
            
            if(x0 == -1 || y0 == -1){
                Q11 = 0;
            }else{
                Q11 = image[y0 * width + x0];
            }
            
            if(x1 == width || y0 == -1){
                Q12 = 0;
            }else{
                Q12 = image[y0 * width + x1];
            }
            
            if(x0 == -1 || y1 == height){
                Q21 = 0;
            }else{
                Q21 = image[y1 * width + x0];
            }
            
            if(x1 == width || y1 == height){
                Q22 = 0;
            }else{
                Q22 = image[y1 * width + x1];
            }
            
            out[i * full_width + j] = (BYTE)roundf((1-alpha) * (1-beta) * Q11 \
            + alpha * (1-beta) * Q12 + (1-alpha) * beta * Q21 + alpha * beta * Q22);
        }
    }
    return out;
}

float* imrotatef_full(float* image, int height, int width, float angle_degree, int *out_height, int *out_width){
    float diagonal = sqrt(height * height + width * width);
    float theta0 = atan((float)height / (float)width);
    float theta1 = angle_degree * PI / 180.0;
    if(angle_degree >= 0.0f && angle_degree <= 90.0f){
        *out_height = (int)ceilf(diagonal * sin(theta0 + theta1));
        *out_width = (int)ceilf(diagonal * sin(theta0 - theta1 + PI / 2));
    }
    else if(angle_degree >= -90.0f && angle_degree <= 0.0f){
        *out_height = (int)ceilf(diagonal * sin(theta0 - theta1));
        *out_width = (int)ceilf(diagonal * sin(theta0 + theta1 + PI / 2));
    }
    else{
        printf("angle beyond -90 ~ +90 degrees is not supported");
        *out_height = 0;
        *out_width = 0;
        return NULL;
    }
    
    int full_height = *out_height;
    int full_width = *out_width;
    float* out = (float*)calloc(full_width * full_height, sizeof(float));
    
    float center_x = (width - 1) / 2.0;
    float center_y = (height - 1) / 2.0;
    float center_x_out = (*out_width - 1) / 2.0;
    float center_y_out = (*out_height - 1) / 2.0;
    
    float x, y; // coordinates before rotation
    float alpha, beta;
    int x0, x1, y0, y1;
    float Q11, Q12, Q21, Q22;
    
    float cosine = cos(theta1);
    float sine = sin(theta1);
    
    for(int i = 0; i < *out_height; i++){
        for(int j = 0; j < *out_width; j++){
            x = center_x + cosine * (j - center_x_out) - sine  * (i - center_y_out);
            y = center_y + sine  * (j - center_x_out) + cosine * (i - center_y_out);
            
            if(x <= -1.0 || y <= -1.0 || x >= (float)width || y >= (float)height){
                out[i * full_width + j] = 0;
                continue;
            }
            
            alpha = x - floorf(x);
            beta  = y - floorf(y);
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            y0 = (int)floorf(y);
            y1 = (int)ceilf(y);
            
            if(x0 == -1 || y0 == -1){
                Q11 = 0.0f;
            }else{
                Q11 = image[y0 * width + x0];
            }
            
            if(x1 == width || y0 == -1){
                Q12 = 0.0f;
            }else{
                Q12 = image[y0 * width + x1];
            }
            
            if(x0 == -1 || y1 == height){
                Q21 = 0.0f;
            }else{
                Q21 = image[y1 * width + x0];
            }
            
            if(x1 == width || y1 == height){
                Q22 = 0.0f;
            }else{
                Q22 = image[y1 * width + x1];
            }
            
            out[i * full_width + j] = (1-alpha) * (1-beta) * Q11 + alpha * (1-beta) * Q12 \
            + (1-alpha) * beta * Q21 + alpha * beta * Q22;
        }
    }
    return out;
}

void inverse_imrotatef_full(float* src, float* target, int height, int width, float angle_degree){
    float diagonal = sqrt(height * height + width * width);
    float theta0 = atan((float)height / (float)width);
    float theta1 = angle_degree * PI / 180.0;
    int src_height, src_width;
    if(angle_degree >= 0.0f && angle_degree <= 90.0f){
        src_height = (int)ceilf(diagonal * sin(theta0 + theta1));
        src_width = (int)ceilf(diagonal * sin(theta0 - theta1 + PI / 2));
    }
    else if(angle_degree >= -90.0f && angle_degree <= 0.0f){
        src_height = (int)ceilf(diagonal * sin(theta0 - theta1));
        src_width = (int)ceilf(diagonal * sin(theta0 + theta1 + PI / 2));
    }
    else{
        printf("angle beyond -90 ~ +90 degrees is not supported");
        return;
    }
    
    float center_x = (width - 1) / 2.0;
    float center_y = (height - 1) / 2.0;
    float center_x_src = (src_width - 1) / 2.0;
    float center_y_src = (src_height - 1) / 2.0;
    
    float x, y; // src image coordinates
    float alpha, beta;
    int x0, x1, y0, y1;
    float Q11, Q12, Q21, Q22;
    
    float cosine = cos(-theta1); // inverse rotate
    float sine = sin(-theta1);
    
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            x = center_x_src + cosine * (j - center_x) - sine  * (i - center_y);
            y = center_y_src + sine  * (j - center_x) + cosine * (i - center_y);
            
            if(x <= -1.0 || y <= -1.0 || x >= (float)src_width || y >= (float)src_height){
                printf("Someting wrong in inverse imrotate\n");
                continue;
            }
            
            alpha = x - floorf(x);
            beta  = y - floorf(y);
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            y0 = (int)floorf(y);
            y1 = (int)ceilf(y);
            
            if(x0 < 0 || y0 < 0 || x1 > src_width - 1 || y1 > src_height - 1){
                printf("Someting wrong in inverse imrotate\n");
                continue;
            }
            
            Q11 = src[y0 * src_width + x0];
            Q12 = src[y0 * src_width + x1];
            Q21 = src[y1 * src_width + x0];
            Q22 = src[y1 * src_width + x1];
            
            target[i * width + j] = (1-alpha) * (1-beta) * Q11 + alpha * (1-beta) * Q12 \
            + (1-alpha) * beta * Q21 + alpha * beta * Q22;
        }
    }
}

void inverse_imrotate_full(BYTE* src, BYTE* target, int height, int width, float angle_degree){
    float diagonal = sqrt(height * height + width * width);
    float theta0 = atan((float)height / (float)width);
    float theta1 = angle_degree * PI / 180.0;
    int src_height, src_width;
    if(angle_degree >= 0.0f && angle_degree <= 90.0f){
        src_height = (int)ceilf(diagonal * sin(theta0 + theta1));
        src_width = (int)ceilf(diagonal * sin(theta0 - theta1 + PI / 2));
    }
    else if(angle_degree >= -90.0f && angle_degree <= 0.0f){
        src_height = (int)ceilf(diagonal * sin(theta0 - theta1));
        src_width = (int)ceilf(diagonal * sin(theta0 + theta1 + PI / 2));
    }
    else{
        printf("angle beyond -90 ~ +90 degrees is not supported");
        return;
    }
    
    float center_x = (width - 1) / 2.0;
    float center_y = (height - 1) / 2.0;
    float center_x_src = (src_width - 1) / 2.0;
    float center_y_src = (src_height - 1) / 2.0;
    
    float x, y; // src image coordinates
    float alpha, beta;
    int x0, x1, y0, y1;
    BYTE Q11, Q12, Q21, Q22;
    
    float cosine = cos(-theta1); // inverse rotate
    float sine = sin(-theta1);
    
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            x = center_x_src + cosine * (j - center_x) - sine  * (i - center_y);
            y = center_y_src + sine  * (j - center_x) + cosine * (i - center_y);
            
            if(x <= -1.0 || y <= -1.0 || x >= (float)src_width || y >= (float)src_height){
                printf("Someting wrong in inverse imrotate\n");
                continue;
            }
            
            alpha = x - floorf(x);
            beta  = y - floorf(y);
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            y0 = (int)floorf(y);
            y1 = (int)ceilf(y);
            
            if(x0 < 0 || y0 < 0 || x1 > src_width - 1 || y1 > src_height - 1){
                printf("Someting wrong in inverse imrotate\n");
                continue;
            }
            
            Q11 = src[y0 * src_width + x0];
            Q12 = src[y0 * src_width + x1];
            Q21 = src[y1 * src_width + x0];
            Q22 = src[y1 * src_width + x1];
            
            target[i * width + j] = (BYTE)roundf((1-alpha) * (1-beta) * Q11 + alpha * (1-beta) * Q12 \
            + (1-alpha) * beta * Q21 + alpha * beta * Q22);
        }
    }
}

int* sum_norm(int* src, float* denom, int length){
    int* out = (int*)malloc(length * sizeof(int));
    for(int i = 0; i < length; i++){
        out[i] = (int)roundf(src[i] / denom[i]);
    }
    return out;
}

float* sum_normf(int* src, float* denom, int length){
    float* out = (float*)malloc(length * sizeof(float));
    for(int i = 0; i < length; i++){
        out[i] = src[i] / denom[i];
    }
    return out;
}

float CalStd(int* peaks, int peak_num){
    float mean = array_mean(peaks, peak_num);
    printf("mean = %f\n", mean);
    float var = 0.0;
    for(int i = 0; i < peak_num; i++)
        var += pow(peaks[i] - mean, 2);
    return sqrt(var / peak_num) / mean;
}

float CalPeakMean(BYTE* img, int height, int width, float angle){
    float mean = 0.0f;
    float* temp = ones(height, width);
    int out_height, out_width;
    BYTE* img_rotated = imrotate_full(img, height, width, angle - 90, &out_height, &out_width);
    float* temp_rotated = imrotatef_full(temp, height, width, angle - 90, &out_height, &out_width);
    
    int* img_row = sum(img_rotated, out_height, out_width, 1);
    float* temp_row = sumf(temp_rotated, out_height, out_width, 1);
    int* profile = sum_norm(img_row, temp_row, out_width);
    MeanFilter(&profile, out_width, 3);
    
    int safe_distance = 10;
    int *peaks, *locs, peak_num;
    findpeaks(profile, out_width, &peaks, &locs, &peak_num, safe_distance);
        
    int deprecated = 2;
    if(peak_num < deprecated * 4){
        printf("Too few peaks\n");
        return -1.0;
    }
    
    int crop_num = peak_num - 2 * deprecated;
    int* peaks_crop = (int*)calloc(crop_num, sizeof(int));
    for(int i = 0; i < crop_num; i++)
        peaks_crop[i] = peaks[i + deprecated];
    
    mean = array_mean(peaks_crop, crop_num);
    
    free(temp);
    free(img_rotated);
    free(temp_rotated);
    free(img_row);
    free(temp_row);
    free(profile);
    free(peaks);
    free(peaks_crop);
    free(locs);
    
    return mean;
}

int Primary_Orientation(unsigned char* img, float* pixel_ort, int height, int width){
    int search_range = 3;
    int bin_num = 181; // 0 ~ 180 degree
    int pixel_num = width * height;
    int* hist = malloc(bin_num * sizeof(int));
    
    CalAngleHist(hist, bin_num, pixel_ort, pixel_num);
    MeanFilter(&hist, 181, 3);
    int peak_angle = FindMax(hist, bin_num);
    
    float max_mean = CalPeakMean(img, height, width, (float)peak_angle);
    int primary_angle = peak_angle;
    for(int i = peak_angle - search_range; i <= peak_angle + search_range; i++){
        float mean = CalPeakMean(img, height, width, (float)i);
        if(max_mean < mean){
            max_mean = mean;
            primary_angle = i;
        }
    }
    free(hist);
    return primary_angle;
}
