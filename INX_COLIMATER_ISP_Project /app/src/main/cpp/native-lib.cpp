#include <jni.h>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <android/log.h>
#include <stdlib.h>
#include <termios.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include "ET760_Collimator_ISP.h"

#define TAG "INX_EP_JNI"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,TAG ,__VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, TAG ,__VA_ARGS__)


/**
 * 注意 這裡的值不是吃DEFINE來的哦 這裡是先寫死
 */


#define MAX_LENGTH 375*240
#define MAX_BACKGROUND_CNT 10

bool isInited = false ;
int c_frame[MAX_LENGTH] ; // 只宣告一次MALLOC用的FRAME
int c_background[MAX_LENGTH] ; //用來擋背的BACKGROUND DEFAULT要全零
int c_bkg_set[MAX_LENGTH*MAX_BACKGROUND_CNT] ; // DEFAULT 共有十張
int c_img_sb[MAX_LENGTH] ;
int c_ref_img[MAX_LENGTH] ;
unsigned char c_ref_img_8bit[MAX_LENGTH] ; // need copy form jint to unsigned char @@
unsigned char resultIMG[MAX_LENGTH] ; //接RESULT用的


//extern "C"
//JNIEXPORT void JNICALL
//Java_fingerlib_igis_com_auo_1isp_AUO_1ISP_AUOISP_1JNI(JNIEnv *env, jclass type, jintArray frame_,
//                                                      jintArray background_, jint w, jint h,
//                                                      jint denoise_hv) {
//
//}



extern "C"
JNIEXPORT jstring JNICALL
Java_fingerlib_igis_com_inx_1ep_1isp_INX_1EP_1ISP_stringFromJNI(JNIEnv *env, jclass clazz) {
    std::string hello = "This is INX_EP_ISP, update date: 10/28 v2";
    return env->NewStringUTF(hello.c_str());
}


void init_something(int w, int h ) {
//    c_frame = (int*) malloc(w * h * sizeof(int)) ;
//    c_background = (int*) malloc(w * h * sizeof(int)) ;
//    resultIMG = (unsigned char * ) malloc(w * h * sizeof(unsigned char)) ;
    isInited = true ;
}




/**
 * 如果大小一直變化，需要重新INIT
 */
extern "C"
JNIEXPORT jstring JNICALL
Java_fingerlib_igis_com_inx_1ep_1isp_INX_1EP_1ISP_init(JNIEnv *env, jclass clazz, jint w, jint h) {

    //init_something(w, h) ;
    std::string hello = "INIT INX_EP_ISP";
    return env->NewStringUTF(hello.c_str());

}



extern "C"
JNIEXPORT void JNICALL
Java_fingerlib_igis_com_inx_1ep_1isp_INX_1EP_1ISP_INX_1EP_1ISP_1JNI(JNIEnv *env, jclass clazz,
                                                                    jintArray frame_,
                                                                    jintArray background_, jint w,jint h) {
    LOGI("@@@ IN THE ET760_Collimator_ISP FUNCTION @@@@") ;

    jint *frame = env->GetIntArrayElements(frame_, NULL);
    jint *background = env->GetIntArrayElements(background_, NULL);



    int len = w * h ;
    // copy to c_frame ;
    env->GetIntArrayRegion(frame_, 0, len, c_frame);
    env->GetIntArrayRegion(background_, 0, len, c_background);

    //LOGI("@@@ BEFORE_CALL_ISP @@@@") ;
    ET760_Collimator_ISP(c_frame, c_background, resultIMG, w, h) ;
    // copy back to j_frame
    for ( int i = 0 ; i < len ; i++ ) {
        c_frame[i] = resultIMG[i];
    }

    env->SetIntArrayRegion(frame_, 0, len, c_frame) ;

    env->ReleaseIntArrayElements(frame_, frame, 0);
    env->ReleaseIntArrayElements(background_, background, 0);

    LOGI("END ET760_Collimator_ISP FUNCTION ") ;
}

extern "C"
JNIEXPORT int JNICALL
Java_fingerlib_igis_com_inx_1ep_1isp_INX_1EP_1ISP_sub_1best_1bkg(JNIEnv *env, jclass thiz,
                                                                 jintArray frame_, jintArray bkg_set_,
                                                                 jintArray current_bkg_num_,
                                                                 jint max_bkg_num,
                                                                 jintArray img_sb, jint w, jint h,
                                                                 jint ksize, jint enable_block,
                                                                 jint black_w, jint black_h,
                                                                 jint black_w_num, jint black_h_num,
                                                                 jint bypass_adjust_bkg, jfloat learning_rate) {

    LOGI("@@@ IN THE sub_best_bkg FUNCTION @@@@") ;
//    int c_current_bkg_num[1] ;
//
//    jint *frame = env->GetIntArrayElements(frame_, NULL);
//    jint *bkg_set = env->GetIntArrayElements(bkg_set_, NULL);
//    jint *current_bkg_num = env->GetIntArrayElements(current_bkg_num_, NULL);
//
//
//    int c_bkg_num = 1 * max_bkg_num ;
//    if ( c_bkg_num > MAX_BACKGROUND_CNT ) {
//        LOGI("@@@ ERROR_BKG>10 ONLY DEFINE 10 in static ram @@@@") ;
//    }
//
//
//    int len = w * h ;
//    env->GetIntArrayRegion(frame_, 0, len, c_frame);
//    env->GetIntArrayRegion(bkg_set_, 0, len * c_bkg_num , c_bkg_set );
//    env->GetIntArrayRegion(current_bkg_num_, 0, 1 , c_current_bkg_num );
//
//
////            sub_best_bkg(img,bkg_set,&current_bkg_num,max_bkg_num,img_sb,width,height,k_size,enable_block,block_w,block_h,block_w_num,block_h_num,bypass_adjust_bkg,learning_rate);
//
//
//
//    LOGI("注意，sub_best_bkg有些INTERFACE 不一定有WORKING 這裡是照CALL") ;
//    int bestIndex = sub_best_bkg(c_frame, c_bkg_set, c_current_bkg_num, max_bkg_num, c_img_sb ,w,h,ksize,
//            enable_block,black_w,black_h,black_w_num, black_h_num,bypass_adjust_bkg, learning_rate) ;
//    LOGI("sub_best_bkg 計算完成") ;
//    // copy back to j_frame
//    for ( int i = 0 ; i < len ; i++ ) {
//        c_frame[i] = c_img_sb[i];
//    }
//
//    env->SetIntArrayRegion(frame_, 0, len, c_frame) ;
//    env->SetIntArrayRegion(bkg_set_, 0, len * c_bkg_num , c_bkg_set );
//
//    env->ReleaseIntArrayElements(frame_, frame, 0);
//    env->ReleaseIntArrayElements(bkg_set_, bkg_set, 0);
//
//    LOGI("END sub_best_bkg FUNCTION ") ;


//    return bestIndex;
    return 0 ;
}

extern "C"
JNIEXPORT jstring JNICALL
Java_fingerlib_igis_com_inx_1ep_1isp_INX_1EP_1ISP_getVersion(JNIEnv *env, jclass clazz) {
    char *ver = check_version() ;
    return env->NewStringUTF(ver);
}

extern "C"
JNIEXPORT jfloat JNICALL
Java_fingerlib_igis_com_inx_1ep_1isp_INX_1EP_1ISP_cal_1snr(JNIEnv *env, jclass clazz,
                                                           jintArray frame_, jintArray background_,
                                                           jintArray ref_img_, jint w,
                                                           jint h) {

//    LOGI("@@@ IN THE CAL_SNR FUNCTION @@@@") ;
//    int len = w * h ;
//
//    jint *frame = env->GetIntArrayElements(frame_, NULL);
//    jint *background = env->GetIntArrayElements(background_, NULL);
//    jint *ref_img = env->GetIntArrayElements(ref_img_, NULL);
//
//    // copy to c_frame ;
//    env->GetIntArrayRegion(frame_, 0, len, c_frame);
//    env->GetIntArrayRegion(background_, 0, len, c_background); // background must be [has value] or [all zero] with subcated frame
//    env->GetIntArrayRegion(ref_img_, 0, len, c_ref_img);
//
//    for ( int i = 0 ; i < len ; i++ ) {
//        c_ref_img_8bit[i] = c_ref_img[i];
//    }
//
//    float val = cal_snr(c_frame, c_background, c_ref_img_8bit, w, h) ;
//
//    env->ReleaseIntArrayElements(frame_, frame, 0);
//    env->ReleaseIntArrayElements(background_, background, 0);
//    env->ReleaseIntArrayElements(ref_img_, ref_img, 0);
//
//    LOGI("END CAL_SNR FUNCTION ") ;
//    return val ;

    return 0 ;
}