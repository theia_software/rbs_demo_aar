//
//  image_processing.c
//  Mapis_stitch
//
//  Created by Luan Liu on 2019/8/14.
//  Copyright © 2019 Theia. All rights reserved.
//

#include <time.h>
#include "image_processing.h"
//extern int * __error(void);
//#define errno (*__error())
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;

unsigned short** load_bin_file_short(char *bin_fn, int width, int height);
unsigned short** G3AllocUnsignedShortImage(int width, int height);
static int fopen_s(FILE **f, const char *name, const char *mode);

unsigned char* imrotate(unsigned char *image, int height, int width, float angle_degree){
    
    float angle_rad = angle_degree * PI / 180.0;
    float cosine = cosf(angle_rad);
    float sine = sinf(angle_rad);
    
    unsigned char *out = (unsigned char*)calloc(height * width, sizeof(unsigned char));
    float center_x = (width  - 1) / 2.0;
    float center_y = (height - 1) / 2.0;
    
    float alpha, beta;
    int x0, x1, y0, y1;
    unsigned char Q11, Q12, Q21, Q22;
    
    float x, y; // coordinates before rotation
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            x = center_x + cosine * (j - center_x) - sine  * (i - center_y);
            y = center_y + sine  * (j - center_x) + cosine * (i - center_y);
            
            alpha = x - floorf(x);
            beta  = y - floorf(y);
            
            if(x <= -1.0 || y <= -1.0 || x >= (float)width || y >= (float)height){
                out[i * width + j] = 0;
                continue;
            }
            
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            y0 = (int)floorf(y);
            y1 = (int)ceilf(y);
            
            if(x0 == -1 || y0 == -1){
                Q11 = 0;
            }else{
                Q11 = image[y0 * width + x0];
            }
            
            if(x1 == width || y0 == -1){
                Q12 = 0;
            }else{
                Q12 = image[y0 * width + x1];
            }
            
            if(x0 == -1 || y1 == height){
                Q21 = 0;
            }else{
                Q21 = image[y1 * width + x0];
            }
            
            if(x1 == width || y1 == height){
                Q22 = 0;
            }else{
                Q22 = image[y1 * width + x1];
            }
            
            out[i * width + j] = (1-alpha) * (1-beta) * Q11 \
            + alpha * (1-beta) * Q12 \
            + (1-alpha) * beta * Q21 \
            + alpha * beta * Q22;
        }
    }
    return out;
}

unsigned short* imrotate_16bit(unsigned short *image, int height, int width, float angle_degree){
    
    float angle_rad = angle_degree * PI / 180.0;
    float cosine = cosf(angle_rad);
    float sine = sinf(angle_rad);
    
    unsigned short *out = (unsigned short*)calloc(height * width, sizeof(unsigned short));
    float center_x = (width  - 1) / 2.0;
    float center_y = (height - 1) / 2.0;
    
    float alpha, beta;
    int x0, x1, y0, y1;
    unsigned short Q11, Q12, Q21, Q22;
    
    float x, y; // coordinates before rotation
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            x = center_x + cosine * (j - center_x) - sine  * (i - center_y);
            y = center_y + sine  * (j - center_x) + cosine * (i - center_y);
            
            alpha = x - floorf(x);
            beta  = y - floorf(y);
            
            if(x <= -1.0 || y <= -1.0 || x >= (float)width || y >= (float)height){
                out[i * width + j] = 0;
                continue;
            }
            
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            y0 = (int)floorf(y);
            y1 = (int)ceilf(y);
            
            if(x0 == -1 || y0 == -1){
                Q11 = 0;
            }else{
                Q11 = image[y0 * width + x0];
            }
            
            if(x1 == width || y0 == -1){
                Q12 = 0;
            }else{
                Q12 = image[y0 * width + x1];
            }
            
            if(x0 == -1 || y1 == height){
                Q21 = 0;
            }else{
                Q21 = image[y1 * width + x0];
            }
            
            if(x1 == width || y1 == height){
                Q22 = 0;
            }else{
                Q22 = image[y1 * width + x1];
            }
            
            out[i * width + j] = (1-alpha) * (1-beta) * Q11 \
            + alpha * (1-beta) * Q12 \
            + (1-alpha) * beta * Q21 \
            + alpha * beta * Q22;
        }
    }
    return out;
}

unsigned short* imrotate_roi_16bit(unsigned short *image, int height, int width, float angle_degree, int* roi){
    
    float angle_rad = angle_degree * PI / 180.0;
    float cosine = cosf(angle_rad);
    float sine = sinf(angle_rad);
    
    unsigned short *out = (unsigned short*)calloc(height * width, sizeof(unsigned short));
    float center_x = (width  - 1) / 2.0;
    float center_y = (height - 1) / 2.0;
    
    float alpha, beta;
    int x0, x1, y0, y1;
    unsigned short Q11, Q12, Q21, Q22;
    
    // int start_j = (int)floorf(center_x + cosine * (roi[0] - center_x) + sine * (roi[1] - center_y));
    // int start_i = (int)floorf(center_y - sine * (roi[0] - center_x) + cosine * (roi[1] - center_y));
    // int end_j = (int)ceilf(center_x + cosine * (roi[0] + roi[2] - center_x) + sine * (roi[1] + roi[3] - center_y));
    // int end_i = (int)ceilf(center_x - sine * (roi[0] + roi[2] - center_x) + cosine * (roi[1] + roi[3] - center_y));
    int start_j = max(roi[0] - 20, 0);
    int start_i = max(roi[1] - 20, 0);
    int end_j = min(roi[0] + roi[2] + 40, width - 1);
    int end_i = min(roi[1] + roi[3] + 40, height - 1);
    
    float x, y; // coordinates before rotation
    for(int i = start_i; i < end_i; i++){
        for(int j = start_j; j < end_j; j++){
            x = center_x + cosine * (j - center_x) - sine * (i - center_y);
            y = center_y + sine * (j - center_x) + cosine * (i - center_y);
            
            alpha = x - floorf(x);
            beta  = y - floorf(y);
            
            if(x <= -1.0 || y <= -1.0 || x >= (float)width || y >= (float)height){
                out[i * width + j] = 0;
                continue;
            }
            
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            y0 = (int)floorf(y);
            y1 = (int)ceilf(y);
            
            if(x0 == -1 || y0 == -1){
                Q11 = 0;
            }else{
                Q11 = image[y0 * width + x0];
            }
            
            if(x1 == width || y0 == -1){
                Q12 = 0;
            }else{
                Q12 = image[y0 * width + x1];
            }
            
            if(x0 == -1 || y1 == height){
                Q21 = 0;
            }else{
                Q21 = image[y1 * width + x0];
            }
            
            if(x1 == width || y1 == height){
                Q22 = 0;
            }else{
                Q22 = image[y1 * width + x1];
            }
            
            out[i * width + j] = (1-alpha) * (1-beta) * Q11 \
            + alpha * (1-beta) * Q12 \
            + (1-alpha) * beta * Q21 \
            + alpha * beta * Q22;
        }
    }
    return out;
}

unsigned short* imresize2_16bit(unsigned short *image, int height, int width, int out_height, int out_width) {

    unsigned short *out = (unsigned short*)calloc(out_height * out_width, sizeof(unsigned short));

    float m = (float)height / (float)out_height;
    float n = (float)width / (float)out_width;
    float a, b, value, xf, yf;
    int x0, y0, x1, y1;


    for (int i = 0; i < out_height; i++) {
        for (int j = 0; j < out_width; j++) {
            yf = -0.5 + m / 2 + (float)i * m;
            xf = -0.5 + n / 2 + (float)j * n;

            if (xf < 0.0) {
                xf = 0.0;
            }
            if (xf > width - 1) {
                xf = width - 1.0;
            }
            if (yf < 0.0) {
                yf = 0.0;
            }
            if (yf > height - 1) {
                yf = height - 1.0;
            }

            y0 = (int)floorf(yf);
            x0 = (int)floorf(xf);
            y1 = (int)ceilf(yf);
            x1 = (int)ceilf(xf);
            a = (yf - (float)y0);
            b = (xf - (float)x0);
            value = (1 - a)*(1 - b)*(image[y0 * width + x0]) + (1 - a)*b*(image[y0 * width + x1]) + a * (1 - b)*(image[y1 * width + x0]) + a * b*(image[y1 * width + x1]);
            out[i * out_width + j] = (unsigned short)value;
        }
    }
    return out;
}

unsigned char* convolution(unsigned char *image, int height, int width, float *kernel, int kernel_size){
    
    unsigned char *out = (unsigned char*)calloc(height * width, sizeof(unsigned char));
    
    int kernel_radius = kernel_size / 2;
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            float value = 0.0;
            for(int m = 0; m < kernel_size; m++){
                for(int n = 0; n < kernel_size; n++){
                    int img_x = j + (n - kernel_radius);
                    int img_y = i + (m - kernel_radius);
                    if (img_x >= 0 && img_y >= 0 && img_x < width && img_y < height){
                        value += image[img_y * width + img_x] * kernel[m * kernel_size + n];
                    }
                }
            }
            out[i * width + j] = (unsigned char)value;
        }
    }
    return out;
}

int* convolution1D(int *signal, int length, float *kernel, int kernel_size){
    int *out = (int*)malloc(length*sizeof(int));
    int kernel_radius = kernel_size / 2;
    for(int i = 0; i < length; i++){
        float value = 0.0;
        for(int j = 0; j < kernel_size; j++){
            int img_x = i + (j - kernel_radius);
            if(img_x >= 0 && img_x < length){
                value += signal[img_x] * kernel[j];
            }
        }
        out[i] = (int)value;
    }
    return out;
}

void removeBackground(unsigned short* fingerImage, unsigned short* backgroundImage, int height, int width){
    int num = height * width;
    for(int i = 0; i < num; i++){
        if(fingerImage[i] < backgroundImage[i])
            fingerImage[i] = 0;
        else
            fingerImage[i] -= backgroundImage[i];
    }
}

void removeBackground_roi(unsigned short* fingerImage, unsigned short* backgroundImage, int height, int width, int* roi){
    int start_j = max(roi[0] - 20, 0);
    int start_i = max(roi[1] - 20, 0);
    int end_j = min(roi[0] + roi[2] + 40, width - 1);
    int end_i = min(roi[1] + roi[3] + 40, height - 1);
    
    for(int i = start_i; i < end_i; i++){
        for(int j = start_j; j < end_j; j++)
            if(fingerImage[i * width + j] < backgroundImage[i * width + j])
            fingerImage[i * width + j] = 0;
        else
            fingerImage[i * width + j] -= backgroundImage[i * width + j];
    }
}

unsigned short* removeAvgBackground(unsigned short* fingerImage, unsigned short* backgroundImage, int height, int width){
    unsigned short* out = (unsigned short*)calloc(height * width, sizeof(unsigned short));
    int* buffer = (int*)calloc(height * width, sizeof(int));
    
    int min = (int)fingerImage[0] - (int)backgroundImage[0];
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            buffer[i * width + j] = (int)fingerImage[i * width + j] - (int)backgroundImage[i * width + j];
            if(min > buffer[i * width + j])
                min = buffer[i * width + j];
        }
    }
    
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            out[i * width + j] = (unsigned short)((buffer[i * width + j] - min));
//            out[i * width + j] = (unsigned short)max(buffer[i * width + j], 0);
        }
    }
    
    free(buffer);
    return out;
}

unsigned char* convertBits(unsigned short* image, int height, int width, int* roi_coordinates){
    int x0 = roi_coordinates[0]; // 250
    int x1 = roi_coordinates[1]; // 500 not included
    int y0 = roi_coordinates[2]; // 245
    int y1 = roi_coordinates[3]; // 495 not included
    
    int delta_x = x1 - x0;
    int delta_y = y1 - y0;
    
    int pixel_num = delta_x * delta_y;
    int cornerPart = pixel_num / 10;
//    clock_t start, end;
//    start = clock();
    unsigned char *out = (unsigned char*)calloc(height*width, sizeof(unsigned char));
//    end = clock();
//    printf("\talloc mem time = %f\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    
//    start = clock();
    int* hist = (int*)calloc(uint16_max, sizeof(int));
    for(int i = y0; i < y1; i++){
        for(int j = x0; j < x1; j++){
            hist[image[i * width + j]] += 1;
        }
    }
//    end = clock();
//    printf("\tcal hist time = %f\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    
//    start = clock();
    int lower_bound = 0;
    int upper_bound = uint16_max;
    int acc_num = 0;
    for(int i = 0; i < uint16_max; i++){
        acc_num += hist[i];
        if(acc_num >= cornerPart){
            lower_bound = i;
            break;
        }
    }
    
    acc_num = 0;
    for(int i = uint16_max - 1; i >= 0; i--){
        acc_num += hist[i];
        if(acc_num >= cornerPart){
            upper_bound = i;
            break;
        }
    }
//    end = clock();
//    printf("\tfind boundary time = %f\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    
    if(pixel_num > uint16_max * 6 / 5){
//        start = clock();
        unsigned char LUT[uint16_max] = {0};
        for(int i = 0; i < uint16_max; i++){
            if(i < lower_bound)
                LUT[i] = (unsigned char)roundf(i * 12.0 / (lower_bound + 0.0001));
            else if(i > upper_bound)
                LUT[i] = (unsigned char)roundf((i - upper_bound) * 12.0 / (uint16_max - upper_bound + 0.0001) + 243);
            else
                LUT[i] = (unsigned char)roundf((i - lower_bound) * 231.0 / (upper_bound - lower_bound + 0.0001) + 12);
        }
        for(int i = y0; i < y1; i++){
            for(int j = x0; j < x1; j++){
                out[i * width + j] = LUT[image[i * width + j]];
            }
        }
//        end = clock();
//        printf("\tmake LUT time = %f\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    }
    else{
//        start = clock();
        for(int i = y0; i < y1; i++){
            for(int j = x0; j < x1; j++){
                if(image[i * width + j] < lower_bound){
                    out[i * width + j] = (unsigned char)roundf(image[i * width + j] * 25.0 / (lower_bound + 0.0001));
                }
                else if(image[i * width + j] > upper_bound){
                    out[i * width + j] = (unsigned char)roundf((image[i * width + j] - upper_bound) * 25.0 / (uint16_max - upper_bound + 0.0001) + 230);
                }
                else{
                    out[i * width + j] = (unsigned char)roundf((image[i * width + j] - lower_bound) * 205.0 / (upper_bound - lower_bound + 0.0001) + 25);
                }
            }
        }
//        end = clock();
//        printf("\tconvert bits time = %f\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    }
    return out;
}

unsigned short **load_uint16_bin(char *fileName, int width, int height) {
    // width = 800, height = 768
    unsigned short **raw = NULL;
    
    if (strstr(fileName, ".bin") != NULL) {
        raw = load_bin_file_short(fileName, width, height);
    }
    //    for (int row = 0; row < h; ++row){
    //        for (int col = 0; col < w; ++col){
    //            printf("%d", raw[row][col]);
    //        }
    //    }
    return raw;
}

unsigned short **load_bin_file_short(char *bin_fn, int width, int height) {
    FILE *f;
    unsigned short **ret = NULL;
    if (fopen_s(&f, bin_fn, "rb") == 0) {
        int i;
        unsigned char c[2];
        ret = G3AllocUnsignedShortImage(width, height);
        for (i = 0; i < width * height; i++) {
            fread(&c, 1, sizeof(c), f);
            *(ret[0] + i) = c[0]<<8 | c[1];
            //            int row = i/200;
            //            int col = i%200;
            //            ret[row][col] = c[0]<<8 | c[1];
            //            printf("%d",*(ret[0] + i));
        }
        fclose(f);
    }
    return ret;
}

unsigned short **G3AllocUnsignedShortImage(int width, int height)
{
    int ptr_len = sizeof(unsigned short*)*height;
    int width_size = sizeof(unsigned short)*width;
//    int height_size = sizeof(unsigned short)*height;
//    printf("width = %d\n", width);
//    printf("height = %d\n", height);
//    printf("width size = %d\n", width_size);
//    printf("height size = %d\n", height_size);
    unsigned short *mem_buf = (unsigned short*)malloc(ptr_len+width_size*height);
    unsigned short **image = (unsigned short**) mem_buf;
    int i;
    if(!image) return NULL;
    mem_buf += ptr_len / sizeof(unsigned short);
    for(i = 0; i < height; i++, mem_buf+=width) {
        image[i] = mem_buf;
    }
    return image;
}

static int fopen_s(FILE **f, const char *name, const char *mode)
{
    int ret = 0;
    //assert(f);
    *f = fopen(name, mode);
    /* Can't be sure about 1-to-1 mapping of errno and MS' errno_t */
#if 0  
    if (!*f)
        ret = errno;
    return ret;
#else
    return 0;
#endif
}

void findpeaks(int* profile, int length, int** pks, int** locs, int* FinalPeaks, int distance) {
    int i;
    int peaksNUM = 0;
    int gradientM, gradientP, gradientPP, checkPeak;
    *pks = (int*)malloc(sizeof(int)*peaksNUM);
    *locs = (int*)malloc(sizeof(int)*peaksNUM);
    
    for (i = 1; i < length - 1; i++) {
        gradientM = profile[i] - profile[i - 1];
        gradientP = profile[i + 1] - profile[i];
        checkPeak = gradientM * gradientP;
        if (checkPeak < 0 && gradientM>0 && ((i - (*locs)[peaksNUM - 1]) > distance || peaksNUM == 0)) {
            peaksNUM++;
            *pks =(int*) realloc(*pks,sizeof(int)*peaksNUM);
            *locs =(int*) realloc(*locs, sizeof(int)*peaksNUM);
            (*pks)[peaksNUM - 1] = profile[i];
            (*locs) [peaksNUM - 1] = i;
            
            //printf("pksNUM=%d,pks=%d, %d, locs=%d, %d\n", peaksNUM, (*pks)[peaksNUM-1], profile [i], (*locs)[peaksNUM-1], i);
        }
        else if (gradientP == 0 && i!=length-2 && ((i - (*locs)[peaksNUM - 1]) > distance || peaksNUM == 0)) {
            gradientPP = profile[i + 2] - profile[i+1];
            if (gradientPP < 0) {
                peaksNUM++;
                *pks = (int*)realloc(*pks, sizeof(int)*peaksNUM);
                *locs = (int*)realloc(*locs, sizeof(int)*peaksNUM);
                (*pks)[peaksNUM - 1] = profile[i];
                (*locs)[peaksNUM - 1] = i;
            }
        }
    }
    
    *FinalPeaks = peaksNUM;
}

void findpeaksf(float* profile, int length, float** pks, int** locs, int* FinalPeaks, int distance){
    int i;
    int peaksNUM = 0;
    float gradientM, gradientP, gradientPP, checkPeak;
    *pks = (float*)malloc(sizeof(float) * peaksNUM);
    *locs = (int*)malloc(sizeof(int) * peaksNUM);
    
    for (i = 1; i < length - 1; i++) {
        gradientM = profile[i] - profile[i - 1];
        gradientP = profile[i + 1] - profile[i];
        checkPeak = gradientM * gradientP;
        if (checkPeak < 0.0f && gradientM > 0.0f && ((i - (*locs)[peaksNUM - 1]) > distance || peaksNUM == 0)) {
            peaksNUM++;
            *pks = (float*)realloc(*pks, sizeof(float) * peaksNUM);
            *locs = (int*)realloc(*locs, sizeof(int) * peaksNUM);
            (*pks)[peaksNUM - 1] = profile[i];
            (*locs)[peaksNUM - 1] = i;
            
            //printf("pksNUM=%d,pks=%d, %d, locs=%d, %d\n", peaksNUM, (*pks)[peaksNUM-1], profile [i], (*locs)[peaksNUM-1], i);
        }
        else if (fabsf(gradientP) < 0.000001 && i != (length - 2) && ((i - (*locs)[peaksNUM - 1]) > distance || peaksNUM == 0)) {
            gradientPP = profile[i + 2] - profile[i + 1];
            if (gradientPP < 0.0f) {
                peaksNUM++;
                *pks = (float*)realloc(*pks, sizeof(float) * peaksNUM);
                *locs = (int*)realloc(*locs, sizeof(int) * peaksNUM);
                (*pks)[peaksNUM - 1] = profile[i];
                (*locs)[peaksNUM - 1] = i;
            }
        }
    }
    
    *FinalPeaks = peaksNUM;
}

int* sum(unsigned char* image,int height, int width, int type) {
    int* vec=0;
    
    switch (type) {
        case 1:
            vec = calloc(width, sizeof(int));
            for (int i = 0; i < width; i++) {
                for (int j = 0; j <  height; j++) {
                    vec[i] += image[i+j*width];
                }
            }
            break;
        case 2:
            vec = calloc(height, sizeof(int));
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    vec[i] += image[i*width + j];
                }
            }
            break;
        default:
            printf("error at type\n");
            vec = calloc(1, sizeof(uint8_t));
    }
    return vec;
}

float* sumf(float* image,int height, int width, int type) {
    float* vec = NULL;
    
    switch (type) {
        case 1:
            vec = (float*)calloc(width, sizeof(int));
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    vec[i] += image[i + j * width];
                }
            }
            break;
        case 2:
            vec = (float*)calloc(height, sizeof(int));
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    vec[i] += image[i * width + j];
                }
            }
            break;
        default:
            printf("error at type\n");
    }
    return vec;
}

int array_max(int* arr, int length){
    int result = arr[0];
    for(int i = 0; i < length; i++){
        if(result < arr[i])
            result = arr[i];
    }
    return result;
}

int array_min(int* arr, int length){
    int result = arr[0];
    for(int i = 0; i < length; i++){
        if(result > arr[i])
            result = arr[i];
    }
    return result;
}

float array_mean(int* arr, int length){
    float result = 0.0;
    for(int i = 0; i < length; i++){
        result += arr[i];
    }
    return result / length;
}

float array_meanf(float* arr, int length){
    float result = 0.0;
    for(int i = 0; i < length; i++){
        result += arr[i];
    }
    return result / length;
}

float *array_norm(int* arr, int length){
    float *out = (float*)malloc(length*sizeof(float));
    
    int max = array_max(arr, length);
    int min = array_min(arr, length);
    
    float denom = (float)(max - min);
    
    for(int i = 0; i < length; i++){
        out[i] = (float)(arr[i] - min) / denom;
    }
    
    return out;
}

float find_precise(float* arr, int length, float target, char* position){
    float out = -1.0;
    
    if(strcmp(position, "first") == 0){
        for(int i = 0; i < length; i++){
            if(arr[i] >= target){
                out = i - (arr[i] - target) / (arr[i] - arr[i - 1]);
                break;
            }
        }
        return out;
    }
    else if(strcmp(position, "last") == 0){
        for(int i = length - 1; i >= 0; i--){
            if(arr[i] >= target){
                out = i + (arr[i] - target) / (arr[i] - arr[i + 1]);
                break;
            }
        }
        return out;
    }
    else{
        return out;
    }
}

float detectCalibAngle(unsigned char* image, int height, int width, int safe_peak_dist, int deprecated_num_one_size){
    float angle_degree = 0.0;
    int *row = sum(image, height, width, 1);
    int *col = sum(image, height, width, 2);
    
    int kernel_size = 5;
    float u[5] = {2/16., 3/16., 6/16., 3/16., 2/16.};
    //float u[5] = {1/5., 1/5., 1/5., 1/5., 1/5.};
    int* row_smooth = convolution1D(row, width, u, kernel_size);
    int* col_smooth = convolution1D(col, height, u, kernel_size);
    
    int *pks_x, *pks_y, *locs_x, *locs_y, hori_pks_num, vert_pks_num;
    findpeaks(row_smooth, width, &pks_x, &locs_x, &hori_pks_num, safe_peak_dist);
    findpeaks(col_smooth, height, &pks_y, &locs_y, &vert_pks_num, safe_peak_dist);
    
    if(hori_pks_num <= 2 * deprecated_num_one_size + 3 || vert_pks_num <= 2 * deprecated_num_one_size + 3){
        printf("Too few centers are found! Assign calib angle = 0.0\n");
        return angle_degree;
    }
    
    int x_start, x_end, y_start, y_end;
    x_start = locs_x[deprecated_num_one_size];
    x_end = locs_x[hori_pks_num - deprecated_num_one_size - 1];
    
    y_start = locs_y[deprecated_num_one_size];
    y_end = locs_y[deprecated_num_one_size + 2];
    int belt_width = x_end - x_start;
    int belt_height = y_end - y_start;
    
    unsigned char* belt = (unsigned char*)malloc(belt_width * belt_height * sizeof(unsigned char));
    for(int i = y_start; i < y_end; i++){
        for(int j = x_start; j < x_end; j++){
            belt[(i - y_start) * belt_width + (j - x_start)] = image[i * width + j];
        }
    }
    int *belt1_row = sum(belt, belt_height, belt_width, 1);
    int *belt1_row_smooth = convolution1D(belt1_row, belt_width, u, kernel_size);
    
    y_start = locs_y[vert_pks_num - deprecated_num_one_size - 3];
    y_end = locs_y[vert_pks_num - deprecated_num_one_size - 1];
    for(int i = y_start; i < y_end; i++){
        for(int j = x_start; j < x_end; j++){
            belt[(i - y_start) * belt_width + (j - x_start)] = image[i * width + j];
        }
    }
    int *belt2_row = sum(belt, belt_height, belt_width, 1);
    int *belt2_row_smooth = convolution1D(belt2_row, belt_width, u, kernel_size);
    
    int *belt1_pks_x, *belt1_locs_x, *belt2_pks_x, *belt2_locs_x, pks_num;
    findpeaks(belt1_row_smooth, belt_width, &belt1_pks_x, &belt1_locs_x, &pks_num, safe_peak_dist);
    float belt1_x = array_mean(belt1_locs_x, pks_num);
    findpeaks(belt2_row_smooth, belt_width, &belt2_pks_x, &belt2_locs_x, &pks_num, safe_peak_dist);
    float belt2_x = array_mean(belt2_locs_x, pks_num);
    
    float delta_x = belt2_x - belt1_x;
    int delta_y = locs_y[vert_pks_num - deprecated_num_one_size - 2] - locs_y[deprecated_num_one_size + 1];
//    printf("delta_x = %f, delta_y = %d\n", delta_x, delta_y);
    angle_degree = -atan(delta_x/delta_y) * 180.0 / PI;
//    printf("angle = %f\n", angle_degree);
    free(row);
    free(col);
    free(row_smooth);
    free(col_smooth);
    free(pks_x);
    free(pks_y);
    free(locs_x);
    free(locs_y);
    free(belt);
    free(belt1_row);
    free(belt1_row_smooth);
    free(belt2_row);
    free(belt2_row_smooth);
    free(belt1_pks_x);
    free(belt1_locs_x);
    free(belt2_pks_x);
    free(belt2_locs_x);
    return angle_degree;
}

void maskOut(unsigned char* img, unsigned char* out, int height, int width, int* roi){
    int x0 = roi[0];
    int y0 = roi[1];
    int x1 = x0 + roi[2];
    int y1 = y0 + roi[3];
    
    for(int i = y0; i < y1; i++){
        for(int j = x0; j < x1; j++){
            out[(i - y0) * roi[2] + (j - x0)] = img[i * width + j];
        }
    }
}

void genWeightMask(float delta_x, float delta_y, float** out1, float** out2, float** out3, float** out4){
    
    int size1_x = (int)floorf(2 * delta_x);
    int size2_x = (int)ceilf(2 * delta_x);
    int size1_y = (int)floorf(2 * delta_y);
    int size2_y = (int)ceilf(2 * delta_y);
    
    float *linear_weight1_x = (float*)malloc(size1_x * sizeof(float));
    float *linear_weight2_x = (float*)malloc(size2_x * sizeof(float));
    float *linear_weight1_y = (float*)malloc(size1_y * sizeof(float));
    float *linear_weight2_y = (float*)malloc(size2_y * sizeof(float));
    
    *out1 = (float*)malloc(size1_y * size1_x * sizeof(float));
    *out2 = (float*)malloc(size1_y * size2_x * sizeof(float));
    *out3 = (float*)malloc(size2_y * size1_x * sizeof(float));
    *out4 = (float*)malloc(size2_y * size2_x * sizeof(float));
    
    float center_weight = 1.0;
    float low_weight = 0.0;
    
    float std = 0.18;
    int odd_flag = size1_x % 2;
    int half_pos = size1_x / 2;
    float step = (center_weight - low_weight) / half_pos;
    for(int i = 0; i < half_pos + odd_flag; i++){
        linear_weight1_x[i] = exp(-pow(center_weight - (low_weight + i * step), 3) / std);
        linear_weight1_x[half_pos + i] = exp(-pow(i * step, 3) / std);
    }
    
    odd_flag = size2_x % 2;
    half_pos = size2_x / 2;
    step = (center_weight - low_weight) / half_pos;
    for(int i = 0; i < half_pos + odd_flag; i++){
        linear_weight2_x[i] = exp(-pow(center_weight - (low_weight + i * step), 3) / std);
        linear_weight2_x[half_pos + i] = exp(-pow(i * step, 3) / std);
    }
    
    odd_flag = size1_y % 2;
    half_pos = size1_y / 2;
    step = (center_weight - low_weight) / half_pos;
    for(int i = 0; i < half_pos + odd_flag; i++){
        linear_weight1_y[i] = exp(-pow(center_weight - (low_weight + i * step), 3) / std);
        linear_weight1_y[half_pos + i] = exp(-pow(i * step, 3) / std);
    }
    
    odd_flag = size2_y % 2;
    half_pos = size2_y / 2;
    step = (center_weight - low_weight) / half_pos;
    for(int i = 0; i < half_pos + odd_flag; i++){
        linear_weight2_y[i] = exp(-pow(center_weight - (low_weight + i * step), 3) / std);
        linear_weight2_y[half_pos + i] = exp(-pow(i * step, 3) / std);
    }
    
    for(int i = 0; i < size1_y; i++){
        for(int j = 0; j < size1_x; j++){
            (*out1)[i * size1_x + j] = linear_weight1_y[i] * linear_weight1_x[j];
        }
    }
    
    for(int i = 0; i < size1_y; i++){
        for(int j = 0; j < size2_x; j++){
            (*out2)[i * size2_x + j] = linear_weight1_y[i] * linear_weight2_x[j];
        }
    }
    
    for(int i = 0; i < size2_y; i++){
        for(int j = 0; j < size1_x; j++){
            (*out3)[i * size1_x + j] = linear_weight2_y[i] * linear_weight1_x[j];
        }
    }
    
    for(int i = 0; i < size2_y; i++){
        for(int j = 0; j < size2_x; j++){
            (*out4)[i * size2_x + j] = linear_weight2_y[i] * linear_weight2_x[j];
        }
    }
    
    free(linear_weight1_x);
    free(linear_weight2_x);
    free(linear_weight1_y);
    free(linear_weight2_y);
}

/*void getAverageImg(const char* dir, unsigned short* output, int height, int width){
    DIR *dp;
    struct dirent *ep;
    
    dp = opendir(dir);
    if (dp != NULL)
    {
        int counter = 0;
        int valid = 0;
        int* buffer = (int*)calloc(height * width, sizeof(int));
        char root[100];
        while ((ep = readdir(dp))){
            if(counter > 1 && strcmp(ep->d_name, ".DS_Store") != 0){
                puts(ep->d_name);
                strcpy(root, dir);
                strcat(root, "/");
                strcat(root, ep->d_name);
                unsigned short** fingerRaw = load_uint16_bin(root, width, height);
                for(int i = 0; i < height; i++){
                    for(int j = 0; j < width; j++){
                        buffer[i * width + j] += fingerRaw[i][j];
                    }
                }
                free(fingerRaw);
                valid++;
            }
            counter++;
        }
        (void)closedir(dp);
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                output[i * width + j] = (unsigned short)(buffer[i * width + j] / valid);
            }
        }
        free(buffer);
    }
    else
        perror("Couldn't open the directory");
}*/

void histeq_16bit_quantized(unsigned short* img, int height, int width, int max, int min){
    unsigned short* result = (unsigned short*)malloc(height * width * sizeof(unsigned short));
    unsigned char* img_quan = (unsigned char*)malloc(height * width * sizeof(unsigned char));
    int hist[256];
    int LUT[256];
    unsigned short s, k;
    
    max = max * 3;
    min = min;
//    printf("\n\thist max = %d, min = %d\n", max, min);
    
//    clock_t start, end;
//    start = clock();
    memset(hist, 0, sizeof(hist));
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            img_quan[i * width + j] = 255 * (img[i * width + j] - min) / (max - min);
            hist[img_quan[i * width + j]]++;
        }
    }
//    end = clock();
//    printf("\tcal hist time = %f ms\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    
//    start = clock();
    s = 0;
    for(int i = 0; i < 256; i++){
        s += hist[i];
        LUT[i] = s * 65535 / (height * width);
    }
//    end = clock();
//    printf("\tmake LUT time = %f ms\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    
//    start = clock();
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            k = img_quan[i * width + j];
            result[i * width + j] = LUT[k];
        }
    }
//    end = clock();
//    printf("\tfill value time = %f ms\n", (end - start) * 1000.0 / CLOCKS_PER_SEC);
    
    memcpy(img, result, height * width * sizeof(unsigned short));
    
    free(img_quan);
    free(result);
}

void fill_scaled_patch(unsigned short* img, int height, int width, float center_x, float center_y, unsigned short* scaled_patch, int scaled_patch_size, float scale_reciprocal){
    float center_x_round = roundf(center_x);
    float center_y_round = roundf(center_y);
    int scaled_patch_radius = scaled_patch_size / 2;
    float x_offset = center_x - (center_x_round - scaled_patch_radius - center_x) * scale_reciprocal;
    float y_offset = center_y - (center_y_round - scaled_patch_radius - center_y) * scale_reciprocal;
    
    float alpha, beta;
    int x0, x1, y0, y1;
    unsigned short Q11, Q12, Q21, Q22;
    
    float x, y; // coordinates before scaling and flipping
    //float x_round, y_round; // coordinates after scaling and flipping
    for(int i = 0; i < scaled_patch_size; i++){
        y = y_offset - i * scale_reciprocal;
        beta  = y - floorf(y);
        y0 = (int)floorf(y);
        y1 = (int)ceilf(y);
        for(int j = 0; j < scaled_patch_size; j++){
            //x_round = center_x_round - scaled_patch_radius + j; // roundf(center_x - scaled_patch_radius + j);
            //y_round = center_y_round - scaled_patch_radius + i; // roundf(center_y - scaled_patch_radius + i);
            //x = center_x - (x_round - center_x) / scale;
            //y = center_y - (y_round - center_y) / scale;
            x = x_offset - j * scale_reciprocal;
            alpha = x - floorf(x);
            x0 = (int)floorf(x);
            x1 = (int)ceilf(x);
            
            Q11 = img[y0 * width + x0];
            Q12 = img[y0 * width + x1];
            Q21 = img[y1 * width + x0];
            Q22 = img[y1 * width + x1];
            
            scaled_patch[i * scaled_patch_size + j] = \
            (1-alpha) * (1-beta) * Q11 + alpha * (1-beta) * Q12 \
            + (1-alpha) * beta * Q21 + alpha * beta * Q22;
        }
    }
}

void adjustCompensation(unsigned short* profile_patch, int patch_size){
    float weight_coeff = 0.4; // <= 1.0
    int patch_pixel_num = patch_size * patch_size;
    unsigned short profile_max = 0;
    unsigned short profile_min = 65535;
    
    for(int i = 0; i < patch_pixel_num; i++){
        if(profile_max < profile_patch[i])
            profile_max = profile_patch[i];
        if(profile_min > profile_patch[i])
            profile_min = profile_patch[i];
    }
    
    for(int i = 0; i < patch_pixel_num; i++){
        profile_patch[i] = profile_max - (unsigned short)roundf(profile_patch[i] * weight_coeff + profile_min * (1.0 - weight_coeff));
    }
}

void compensate_patch(unsigned short* patch, unsigned short* profile_patch, int patch_size, int* patch_max, int* patch_min){
    int patch_pixel_num = patch_size * patch_size;
    *patch_max = 0;
    *patch_min = 65536;
    for(int i = 0; i < patch_pixel_num; i++){
        patch[i] += profile_patch[i];
        if(*patch_max < patch[i])
            *patch_max = patch[i];
        if(*patch_min > patch[i])
            *patch_min = patch[i];
    }
}
