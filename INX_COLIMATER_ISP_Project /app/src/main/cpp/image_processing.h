//
//  image_processing.h
//  Mapis_stitch
//
//  Created by Luan Liu on 2019/8/14.
//  Copyright © 2019 Theia. All rights reserved.
//

#ifndef image_processing_h
#define image_processing_h

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
//#include <dirent.h>
#define PI 3.141592653589793238462643383
#define uint16_max 65536

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

unsigned char* imrotate(unsigned char *image, int height, int width, float angle_degree);
unsigned char* convolution(unsigned char *image, int height, int width, float *kernel, int kernel_size);
int* convolution1D(int *signal, int length, float *kernel, int kernel_size);

unsigned short* imrotate_16bit(unsigned short *image, int height, int width, float angle_degree);
unsigned short* imrotate_roi_16bit(unsigned short *image, int height, int width, float angle_degree, int* roi);
unsigned short* imresize2_16bit(unsigned short *image, int height, int width, int out_height, int out_width);

void removeBackground(unsigned short* fingerImage, unsigned short* backgroundImage, int height, int width);
void removeBackground_roi(unsigned short* fingerImage, unsigned short* backgroundImage, int height, int width, int* roi);
unsigned short* removeAvgBackground(unsigned short* fingerImage, unsigned short* backgroundImage, int height, int width);
unsigned char* convertBits(unsigned short* image, int height, int width, int* roi_coordinates);
unsigned short** load_uint16_bin(char *fileName, int width, int height);

void findpeaks(int* profile, int length, int** pks, int** locs, int* peaksNum, int distance);
void findpeaksf(float* profile, int length, float** pks, int** locs, int* peaksNum, int distance);
int* sum(unsigned char* image, int height, int width, int type);
float* sumf(float* image,int height, int width, int type);

int array_max(int* arr, int length);
int array_min(int* arr, int length);
float array_mean(int* arr, int length);
float array_meanf(float* arr, int length);
float* array_norm(int* arr, int length);
float find_precise(float* arr, int length, float target, char* position);

float detectCalibAngle(unsigned char* image, int height, int width, int safe_peak_dist, int deprecated_num_one_size);
void maskOut(unsigned char* img, unsigned char* out, int height, int width, int* roi);
void genWeightMask(float delta_x, float delta_y, float** out1, float** out2, float** out3, float** out4);

//void getAverageImg(const char* dir, unsigned short* output, int height, int width);

void histeq_16bit_quantized(unsigned short* img, int height, int width, int max, int min);
void fill_scaled_patch(unsigned short* img, int height, int width, float center_x, float center_y, unsigned short* scaled_patch, int scaled_patch_size, float scale_reciprocal);
void adjustCompensation(unsigned short* profile_patch, int patch_size);
void compensate_patch(unsigned short* patch, unsigned short* profile_patch, int patch_size, int* patch_max, int* patch_min);

#endif /* image_processing_h */


