//
//  statistics.h
//  ET713_Image_Preprocessing
//
//  Created by 黃詠淮 on 2019/1/28.
//  Copyright © 2019 黃詠淮. All rights reserved.
//

#ifndef statistics_h
#define statistics_h


int find_max(int *data, int size);
int find_min(int *data, int size);
float find_max_float(float *data, int size);
float find_min_float(float *data, int size);
int quantile(int *data, int p, int size);
float _cal_mean(float *data, int size);
void _cal_mean_std(float *data, int size, float *mean, float *std);




#endif /* statistics_h */
