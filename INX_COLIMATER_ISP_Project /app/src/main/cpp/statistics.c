//
//  statistics.c
//  ET713_Image_Preprocessing
//
//  Created by 黃詠淮 on 2019/1/28.
//  Copyright © 2019 黃詠淮. All rights reserved.
//

#include "statistics.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>



int find_max(int *data, int size)
{
    int i, max = data[0];
    
    for (i = 1; i < size; i++){
        if (max < data[i]) {
            max = data[i];
        }
    }
    return max;
}


int find_min(int *data, int size)
{
    int i, min = data[0];
    
    for (i = 1; i < size; i++){
        if (min > data[i]) {
            min = data[i];
        }
    }
    return min;
}


float find_max_float(float *data, int size)
{
    int i;
    float max = data[0];
    
    for (i = 1; i < size; i++){
        if (max < data[i]) {
            max = data[i];
        }
    }
    return max;
}


float find_min_float(float *data, int size)
{
    int i;
    float min = data[0];
    
    for (i = 1; i < size; i++){
        if (min > data[i]) {
            min = data[i];
        }
    }
    return min;
}



int quantile(int *data, int p, int size)
{
    int value = 0;
    int i, acc = 0;
    int *hist;
    int max;
    //float r = 100.0f / size;
    float t = p / 100.0f * size;
    
    max = find_max(data, size);
    
    
    hist = (int*)malloc(sizeof(int) * (max+1));
    memset(hist, 0, (max+1)*sizeof(int));
    
    if (p < 0)   p = 0;
    if (p > 100) p = 100;
    
    for (i = 0; i < size; i++) {
        hist[data[i]]++;
    }
    
    for ( i = 0; i <= max; i++){
        acc += hist[i];
        //if (acc * r >= p) {
        if (acc >= t) {
            value = i;
            break;
        }
    }
    
    free(hist);
    return value;
}

float _cal_mean(float *data, int size)
{
    float sum = 0.0f;
    int i;
    
    for (i = 0; i < size; i++) {
        sum += data[i];
    }
    
    return sum / size;
}

void _cal_mean_std(float *data, int size, float *mean, float *std)
{
    float diff, sum_sqrt_diff = 0.0f;
    int i;
    
    *mean = _cal_mean(data, size);
    
    for (i = 0; i < size; i++) {
        diff = data[i] - *mean;
        sum_sqrt_diff  += diff * diff;
    }

    *std = sqrt(sum_sqrt_diff / (size - 1));
}
