//
//  quality_level.c
//  ISP
//
//  Created by Luan Liu on 2019/10/30.
//  Copyright © 2019 Theia. All rights reserved.
//

#include "quality_level.h"

float calSignalPower(float* signal_map, float* offset_map, int height, int width){
    int pixel_num = height * width;
    double power = 0.0;
    for(int i = 0; i < pixel_num; i++){
        power += pow(signal_map[i] - offset_map[i], 2.0);
//        printf("%f, %f, diff = %f\n",signal_map[i], offset_map[i], pow(signal_map[i] - offset_map[i], 2.0));

    }
    power /= (double)pixel_num;
    return (float)power;
}

float calNoisePower(BYTE* img, float* signal_map, int height, int width){
    int pixel_num = height * width;
    double power = 0.0;
    for(int i = 0; i < pixel_num; i++){
        power += pow(img[i] - signal_map[i], 2.0);
//        printf("%u, %f, diff = %f\n",img[i], signal_map[i], pow(img[i] - signal_map[i], 2.0));
    }
    power /= (double)pixel_num;
    return (float)power;
}

float CreateIdealSignalMap(BYTE* ori_img, BYTE* ref_img, float* signal_map, int width, int height){
    // Find primary anlge of the image
    float* pixel_ort = (float*)malloc(width * height * sizeof(float));
    double* pixel_ort_reli = (double*)malloc(width * height * sizeof(double));
    unsigned char* mask = (unsigned char*)malloc(width * height * sizeof(unsigned char));
    memset(mask, 1, width * height);
    Detect_V3_reli(pixel_ort, pixel_ort_reli, 0, ref_img, mask, height, width, 0);
    int angle = Primary_Orientation(ref_img, pixel_ort, height, width);
    free(pixel_ort);
    free(pixel_ort_reli);
    free(mask);
    printf("primary angle = %d\n", angle);
    
    // Create ideal signal image
    int out_width, out_height;
    float* temp = ones(height, width);
    BYTE* ori_rotated = imrotate_full(ori_img, height, width, (float)(angle - 90), &out_height, &out_width);
    float* signal_map_rotated = imrotatef_full(signal_map, height, width, (float)(angle - 90), &out_height, &out_width);
    float* temp_rotated = imrotatef_full(temp, height, width, (float)(angle - 90), &out_height, &out_width);
    // printf("out height = %d, out width = %d\n", out_height, out_width);
    
    int* ori_row = sum(ori_rotated, out_height, out_width, 1);
    float* temp_row = sumf(temp_rotated, out_height, out_width, 1);
    float* raw_signal = sum_normf(ori_row, temp_row, out_width);
    float* clean_signal = GaussFilter(raw_signal, out_width, 2.0);
    
    
    // find wave period
    int *locs, peak_num, safe_distance = 10;
    float *peaks;
    findpeaksf(clean_signal, out_width, &peaks, &locs, &peak_num, safe_distance);
    float period = (float)(locs[peak_num - 1] - locs[0]) / (peak_num - 1);
    free(peaks);
    free(locs);
    // printf("peak dist = %f\n", period);
    
    // find signal offset
    float* offset = FindSignalTrend(clean_signal, out_width, period);
    MeanFilterf(&offset, out_width, 29);
    MeanFilterf(&offset, out_width, 29);
    float* offset_map_rotated = (float*)malloc(out_height * out_width * sizeof(float));
    float* offset_map = (float*)malloc(height * width * sizeof(float));
    
    // amplify clean_signal
    amplifySignal(clean_signal, raw_signal, offset, out_width);
    
    for(int i = 0; i < out_height; i++){
        for(int j = 0; j < out_width; j++){
            offset_map_rotated[i * out_width + j] = offset[j];
            signal_map_rotated[i * out_width + j] = clean_signal[j];
        }
    }
    
    inverse_imrotatef_full(signal_map_rotated, signal_map, height, width, (float)(angle - 90));
    inverse_imrotatef_full(offset_map_rotated, offset_map, height, width, (float)(angle - 90));
    float signal_power = calSignalPower(signal_map, offset_map, height, width);
    
    /*FILE* fp = fopen("raw_signal.txt", "w");
    for(int i = 0; i < out_width; i++){
        fprintf(fp, "%f ", raw_signal[i]);
    }
    fclose(fp);
    
    fp = fopen("signal.txt", "w");
    for(int i = 0; i < out_width; i++){
        fprintf(fp, "%f ", clean_signal[i]);
    }
    fclose(fp);

    fp = fopen("offset.txt", "w");
    for(int i = 0; i < out_width; i++){
        fprintf(fp, "%f ", offset[i]);
    }
    fclose(fp);
    
    fp = fopen("raw-offset.txt", "w");
    for(int i = 0; i < out_width; i++){
        fprintf(fp, "%f ", raw_signal[i] - offset[i]);
    }
    fclose(fp);
    
    fp = fopen("clean_signal-offset.txt", "w");
    for(int i = 0; i < out_width; i++){
        fprintf(fp, "%f ", clean_signal[i] - offset[i]);
    }
    fclose(fp);*/
    
    free(temp);
    free(temp_rotated);
    free(ori_rotated);
    free(signal_map_rotated);
    free(ori_row);
    free(temp_row);
    free(offset_map);
    free(offset_map_rotated);
    
    free(raw_signal);
    free(clean_signal);
    free(offset);
    return signal_power;
}

float CalQualityLevel(int *image, int *bkg, unsigned char *ref_img, int width, int height){
    
    int* BgRemoved = (int*)calloc(width * height, sizeof(int));
    for(int i = 0; i < width * height; i++)
        BgRemoved[i] = image[i] - bkg[i];
    
    BYTE* norm_img = (BYTE*)calloc(width * height, sizeof(BYTE));
    int img_max = array_max(BgRemoved, height * width);
    int img_min = array_min(BgRemoved, height * width);
    for(int i = 0; i < width * height; i++)
        norm_img[i] = (BYTE)roundf((BgRemoved[i] - img_min) * 255.0 / (img_max - img_min));
    // stbi_write_png("./collimator_ori.png", width, height, 1, norm_img, width * 1);
    
    float* signal_map = (float*)calloc(width * height, sizeof(float)); // ideal signal image
    float signal_power = CreateIdealSignalMap(norm_img, ref_img, signal_map, width, height);
    float noise_power = calNoisePower(norm_img, signal_map, height, width);
    printf("signal power = %f\n", signal_power);
    printf("noise power = %f\n", noise_power);
    //printf("snr = %f\n", 10.0 * log10(signal_power/noise_power));
    
    // calculate ipp snr
    /*printf("=============================================\n");
    float *ipp_signal_map = (float*)calloc(width * height, sizeof(float));
    float ipp_signal_power = CreateIdealSignalMap(ref_img, ref_img, ipp_signal_map, width, height);
    float ipp_noise_power = calNoisePower(ref_img, ipp_signal_map, height, width);
    printf("ipp signal power = %f\n", ipp_signal_power);
    printf("ipp noise power = %f\n", ipp_noise_power);
    printf("ipp snr = %f\n", 10.0 * log10(ipp_signal_power/ipp_noise_power));
    printf("=============================================\n");
    free(ipp_signal_map);*/
    
    
    free(BgRemoved);
    free(norm_img);
    free(signal_map);
    return 10.0 * log10(signal_power/noise_power);
}
