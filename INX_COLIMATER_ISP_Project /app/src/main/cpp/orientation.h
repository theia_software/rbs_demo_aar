//
//  orientation.h
//  ISP
//
//  Created by Luan Liu on 2019/10/28.
//  Copyright © 2019 Theia. All rights reserved.
//

#ifndef orientation_h
#define orientation_h

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "macros_allocate.h"
#include "image_processing.h"

#ifndef PI
#define PI 3.141592653589793238462643383
#endif

void amplifySignal(float* clean_signal, float* raw_signal, float* offset, int length);
void MeanFilter(int** signal, int length, int kernel_size);
void MeanFilterf(float** signal, int length, int kernel_size);
float* GaussFilter(float* signal, int length, float std);
float* InvertGaussFilter(float* signal, int length, float std);
float* FindSignalTrend(float* signal, int length, float period);
int FindMax(int* array, int length);
float* ones(int height, int width);
BYTE* imrotate_full(BYTE* image, int height, int width, float angle_degree, int *out_height, int *out_width);
float* imrotatef_full(float* image, int height, int width, float angle_degree, int *out_height, int *out_width);
void inverse_imrotate_full(BYTE* src, BYTE* target, int height, int width, float angle_degree);
void inverse_imrotatef_full(float* src, float* target, int height, int width, float angle);
int* sum_norm(int* src, float* denom, int length);
float* sum_normf(int* src, float* denom, int length);
int Primary_Orientation(unsigned char* img, float* pixel_ort, int height, int width);

#endif /* orientation_h */
