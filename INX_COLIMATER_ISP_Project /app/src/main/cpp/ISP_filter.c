//
//  ISP_filter.c
//  ET713_Image_Preprocessing
//
//  Created by 黃詠淮 on 2019/1/28.
//  Copyright © 2019 黃詠淮. All rights reserved.
//

#include "ISP_filter.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "statistics.h"


float *_pad_array(float *img, int height, int width, int p_y_size, int p_x_size)
{
    float *p_img, *p1, *p2;
    int p_height = (height + 2 * p_y_size);
    int p_width  = (width + 2 * p_x_size);
    int i, j;
    p_img = malloc(sizeof(float) * p_height  * p_width);
    
    for (i = 0; i < p_height; i++) {
        p1 = p_img + i * p_width + p_x_size;
        if (i < p_y_size){
            p2 = img;
        }
        else if (i >= p_height - p_y_size){
            p2 = img + (height-1) * width;
        }
        else{
            p2 = img + (i - p_y_size) * width;
        }
        memcpy (p1, p2, sizeof(float) * width);
    }
    
    
    
    for (i = 0; i < p_height; i++) {
        p1 = p_img + i * p_width;
        if (i < p_y_size) {
            p2 = img;
        }
        else if (i >= p_height - p_y_size){
            p2 = img + (height-1) * width;
        }
        else{
            p2 = img + (i - p_y_size) * width;
        }
        
        for (j = 0; j < p_x_size; j++) {
            p1[j] = *p2;
        }
    }
    
    for (i = 0; i < p_height; i++) {
        p1 = p_img + i * p_width + width + p_x_size;
        if (i < p_y_size) {
            p2 = img + width - 1;
        }
        else if (i >= p_height - p_y_size){
            p2 = img + (height-1) * width + width - 1;
        }
        else{
            p2 = img + (i - p_y_size) * width + width - 1;
        }
        for (j = 0; j < p_x_size; j++) {
            p1[j] = *p2;
        }
    }
    
    
    
    return p_img;
}


void _cal_integral_img(float *img, int height, int width)
{
    int i, j, index;
    float sum;
    
    for (i = 0; i < height; i++) {
        sum = 0;
        for (j = 0; j < width; j++) {
            index = i*width+j;
            sum += img[index];
            if (i == 0) {
                img[index] = sum;
            }
            else{
                //img[index] = img[(i-1) * width + j] + sum;
                img[index] = img[index-width] + sum;
            }
        }
    }
}


void mean_filter(float *img, int width, int height, int k_size)
{
    int i, j;
    int f_radius = (k_size - 1)/2;
    int p_width  = width + 2 * f_radius;
    float *int_img;
    int x1, y1, x2, y2;
    float v1, v2, v3, v4;
    float r = 1.0f/((f_radius*2+1) * (f_radius*2+1));
    
    
    int_img = _pad_array(img, height, width, f_radius, f_radius);
    _cal_integral_img(int_img, height + f_radius * 2, width + f_radius * 2);
    
    
    for (i = f_radius; i < height + f_radius; i++) {
        y1 = i - f_radius - 1;
        y2 = i + f_radius;
        
        
        for (j = f_radius; j < width + f_radius; j++) {
            x1 = j - f_radius - 1;
            x2 = j + f_radius;
            
            v1 = int_img[y2 * p_width + x2];
            if (y1 < 0) v2 = 0;
            else v2 = int_img[y1 * p_width + x2];
            
            if (x1 < 0) v3 = 0;
            else v3 = int_img[y2 * p_width + x1];
            
            if (v2 == 0 || v3 == 0) v4 = 0;
            else v4 = int_img[y1 * p_width + x1];
            
            *img = (v1 - v2 - v3 + v4) * r;
            img++;
        }
    }
    
    free(int_img);
}


void _sym_separable_filter(float *img, int width, int height, float *half_filter, int f_radius) {
    
    
    int         size = width * height;
    float       *temp = (float*)malloc(sizeof(float) * size);
    float       *p1 = img;
    float       *p2 = temp;
    int         width2 = width + width;
    int         height2 = height + height;
    int         i, j, k, l, k1, k2;
    float       value;
    
    //Horizontal Convolution
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++, p1++) {
            value = 0.0f;
            
            for (k = j - f_radius, l = 0; k < j; k++, l++) {
                //Position to process
                k1 = k;
                k2 = j + f_radius - l;
                
                //Bounday checking. If exceeds boundary, process based on mirror reflection
                if (k1 < 0) {
                    k1 = -k1 - 1;
                }
                if (k2 >= width) {
                    k2 = width2 - k2 - 2 + 1;
                }
                //Result derived from Gaussian symmetric weight
                value += (*(p1 + k1 - j) + *(p1 + k2 - j)) * *(half_filter + l);
            }
            //Result derived from  Gaussian middle weight
            value += (*p1) * *(half_filter + f_radius);
            *(p2 + j * height + i) = value;
        }
    }
    
    p1 = img;
    
    
    
    //Vertical Convolution based on the Horizontal result processd above
    //The process is same as above, simply changed the proceesed direction and input
    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++, p2++) {
            value = 0.0;
            for (k = j - f_radius, l = 0; k < j; k++, l++) {
                k1 = k;
                k2 = j + f_radius - l;
                
                if (k1 < 0) {
                    k1 = -k1 - 1;
                }
                if (k2 >= height) {
                    k2 = height2 - k2 - 2 + 1;
                }
                
                value += (*(p2 + k1 - j) + *(p2 + k2 - j)) * *(half_filter + l);
            }
            value += (*p2) * *(half_filter + f_radius);
            *(p1 + j * width + i) = value;
        }
    }
    
    free(temp);
}


void _band_pass_filter(float *img, int width, int height, int f_radius1, int f_radius2)
{
    int i, j;
    int p_width  = width + 2 * f_radius2;
    float *int_img;
    int x1, y1, x2, y2, x3, y3, x4, y4;
    float v1, v2, v3, v4, sum1, sum2;
    float r1 = 1.0f/((f_radius1*2+1) * (f_radius1*2+1));
    float r2 = 1.0f/((f_radius2*2+1) * (f_radius2*2+1));
    

    int_img = _pad_array(img, height, width, f_radius2, f_radius2);
    _cal_integral_img(int_img, height + f_radius2 * 2, width + f_radius2 * 2);
    
    
    
    for (i = f_radius2; i < height + f_radius2; i++) {
        y1 = i - f_radius1 - 1;
        y2 = i + f_radius1;
        
        y3 = i - f_radius2 - 1;
        y4 = i + f_radius2;

        
        for (j = f_radius2; j < width + f_radius2; j++) {
            x1 = j - f_radius1 - 1;
            x2 = j + f_radius1;
            
            x3 = j - f_radius2 - 1;
            x4 = j + f_radius2;
            
            v1 = int_img[y2 * p_width + x2];
            if (y1 < 0) v2 = 0;
            else v2 = int_img[y1 * p_width + x2];
            
            if (x1 < 0) v3 = 0;
            else v3 = int_img[y2 * p_width + x1];
            
            if (v2 == 0 || v3 == 0) v4 = 0;
            else v4 = int_img[y1 * p_width + x1];
            
            sum1 = (v1 - v2 - v3 + v4) * r1;
            
            
            
            v1 = int_img[y4 * p_width + x4];
            if (y3 < 0) v2 = 0;
            else v2 = int_img[y3 * p_width + x4];
            
            if (x3 < 0) v3 = 0;
            else v3 = int_img[y4 * p_width + x3];
            
            if (v2 == 0 || v3 == 0) v4 = 0;
            else v4 = int_img[y3 * p_width + x3];

            sum2 = (v1 - v2 - v3 + v4) * r2;
            
            //img[index++] = (sum1 - sum2);
            *img = (sum1 - sum2);
            img++;
        }
    }
    
    free(int_img);
}


void insertion_sort(float *data, int len)
{
    int i,j;
    float key;
    for (i=1;i<len;i++){
        key = data[i];
        j = i - 1;
        while((j>=0) && (data[j]>key)) {
            data[j+1] = data[j];
            j--;
        }
        data[j+1] = key;
    }
}



void remove_impulse_noise(float *img, int width, int height, float threshold)
{
    int size = width * height;
    int i, j, x, y, p_width = width + 2;
    float max, min, r, diff, temp[9];
    float *temp1 = malloc(sizeof(float) * size);
    float *temp2;
    memcpy (temp1, img, sizeof(float) * size);
    
    max = min = temp1[0];
    for (i = 1; i < width * height; i++) {
        if (max < temp1[i]) max = temp1[i];
        if (min > temp1[i]) min = temp1[i];
    }
    
    r = 1.0f / (max - min);
    
    for (i = 0; i < size; i++) {
        temp1[i] = (temp1[i] - min) * r;
    }
    
    temp2 = _pad_array(temp1, height, width, 1, 1);
    free(temp1);
    
    temp1 = _pad_array(img, height, width, 1, 1);
    
    
    for (i = 0; i < height; i++) {
        y = i + 1;
        for (j = 0; j < width; j++) {
            x = j + 1;
            
            diff = -temp2[(y-1) * p_width + (x-1)]-
                    temp2[(y-1) * p_width + x]-
                    temp2[(y-1) * p_width + (x+1)]-
                    temp2[y * p_width + (x-1)]+
                    temp2[y * p_width + x] * 9-
                    temp2[y * p_width + (x+1)] -
                    temp2[(y+1) * p_width + (x-1)]-
                    temp2[(y+1) * p_width + x]-
                    temp2[(y+1) * p_width + (x+1)];
            
            diff = diff < 0.0f ? -diff : diff;
            
            if (diff > threshold) {
                temp[0] = temp1[(y-1) * p_width + (x-1)];
                temp[1] = temp1[(y-1) * p_width + x];
                temp[2] = temp1[(y-1) * p_width + (x+1)];
                temp[3] = temp1[y * p_width + (x-1)];
                temp[4] = temp1[y * p_width + x];
                temp[5] = temp1[y * p_width + (x+1)];
                temp[6] = temp1[(y+1) * p_width + (x-1)];
                temp[7] = temp1[(y+1) * p_width + x];
                temp[8] = temp1[(y+1) * p_width + (x+1)];
                
                insertion_sort(temp, 9);
                img[i*width+j] = temp[4];
            }
        }
    }
    
    
    free(temp1);
    free(temp2);
    
}



void remove_impulse_noise_1(float *img, int width, int height)
{
    int size = width * height;
    int i, j, x, y, p_width = width + 2;
    float max, min, r, diff, temp[9];
    float mean, std;
    float *temp1 = malloc(sizeof(float) * size);
    float *temp2;
    float threshold;
    memcpy (temp1, img, sizeof(float) * size);
    
    max = min = temp1[0];
    for (i = 1; i < width * height; i++) {
        if (max < temp1[i]) max = temp1[i];
        if (min > temp1[i]) min = temp1[i];
    }
    
    r = 1.0f / (max - min);
    
    for (i = 0; i < size; i++) {
        temp1[i] = (temp1[i] - min) * r;
    }
    
    temp2 = _pad_array(temp1, height, width, 1, 1);
    
    
    for (i = 0; i < height; i++) {
        y = i + 1;
        for (j = 0; j < width; j++) {
            x = j + 1;
            
            diff = -temp2[(y-1) * p_width + (x-1)]-
                    temp2[(y-1) * p_width + x]-
                    temp2[(y-1) * p_width + (x+1)]-
                    temp2[y * p_width + (x-1)]+
                    temp2[y * p_width + x] * 9-
                    temp2[y * p_width + (x+1)] -
                    temp2[(y+1) * p_width + (x-1)]-
                    temp2[(y+1) * p_width + x]-
                    temp2[(y+1) * p_width + (x+1)];
            
            temp1[i*width+j] = diff < 0.0f ? -diff : diff;
        }
    }
    
    free(temp2);
    temp2 = _pad_array(img, height, width, 1, 1);
    
    _cal_mean_std(temp1, size, &mean, &std);
    
    threshold = mean + 3.5f * std;

    
    for (i = 0; i < height; i++) {
        y = i + 1;
        for (j = 0; j < width; j++) {
            x = j + 1;
            
            
            if (temp1[i*width+j] > threshold) {
                temp[0] = temp2[(y-1) * p_width + (x-1)];
                temp[1] = temp2[(y-1) * p_width + x];
                temp[2] = temp2[(y-1) * p_width + (x+1)];
                temp[3] = temp2[y * p_width + (x-1)];
                temp[4] = temp2[y * p_width + x];
                temp[5] = temp2[y * p_width + (x+1)];
                temp[6] = temp2[(y+1) * p_width + (x-1)];
                temp[7] = temp2[(y+1) * p_width + x];
                temp[8] = temp2[(y+1) * p_width + (x+1)];
                
                insertion_sort(temp, 9);
                img[i*width+j] = temp[4];
            }
        }
    }

    
    
    
    free(temp1);
    free(temp2);
    
}





//void remove_impulse_noise_1(float *img, int width, int height, float threshold)
//{
//    int size = width * height;
//    int i, j, x, y, p_width = width + 2;
//    float diff, temp[9], mean, std;
//    float max, min, r;
//    float *temp1 = malloc(sizeof(float) * size);
//    float *temp2 = malloc(sizeof(float) * size);;
//    memcpy (temp1, img, sizeof(float) * size);
//
//    max = min = temp1[0];
//    for (i = 1; i < width * height; i++) {
//        if (max < temp1[i]) max = temp1[i];
//        if (min > temp1[i]) min = temp1[i];
//    }
//
//    r = 1.0f / (max - min);
//
//    for (i = 0; i < size; i++) {
//        temp1[i] = (temp1[i] - min) * r;
//    }
//
//    memcpy (temp2, temp1, sizeof(float) * size);
//    mean_filter(temp1, width, height, 3);
//
//
//    for (i = 0; i < size; i++) {
//        temp1[i] = (temp1[i] * 9.0f - temp2[i]);
//        diff = temp2[i] * 9.0f - temp1[i];
//        temp1[i] = diff < 0.0f ? -diff : diff;
//    }
//
//    _cal_mean_std(temp1, size, &mean, &std);
//    threshold = 1.8f;//mean + std * 4.0f;
//
//    free(temp2);
//    temp2 = pad_array(img, height, width, 1, 1);
//    int c = 0;
//
//    for (i = 0; i < height; i++) {
//        y = i + 1;
//        for (j = 0; j < width; j++) {
//            x = j + 1;
//
//            if (temp1[i * width + j] > threshold) {
//                temp[0] = temp2[(y-1) * p_width + (x-1)];
//                temp[1] = temp2[(y-1) * p_width + x];
//                temp[2] = temp2[(y-1) * p_width + (x+1)];
//                temp[3] = temp2[y * p_width + (x-1)];
//                temp[4] = temp2[y * p_width + x];
//                temp[5] = temp2[y * p_width + (x+1)];
//                temp[6] = temp2[(y+1) * p_width + (x-1)];
//                temp[7] = temp2[(y+1) * p_width + x];
//                temp[8] = temp2[(y+1) * p_width + (x+1)];
//                c++;
//
//                insertion_sort(temp, 9);
//                img[i*width+j] = temp[4];
//            }
//        }
//    }
//
//    printf("%f, ", threshold);
//
//    free(temp1);
//    free(temp2);
//}
//



void wiener_filter(float *img, int width, int height, int k_size, float noise)
{
    
    int size = width * height;
    int i;
    float *local_mean = malloc(sizeof(float) * size);
    float *local_var = malloc(sizeof(float) * size);
    float *temp = malloc(sizeof(float) * size);
    float e;
    
    memcpy (local_mean, img, sizeof(float) * size);
    mean_filter(local_mean, width, height, k_size);
    
    memcpy (local_var, img, sizeof(float) * size);
    
    for (i = 0; i < size; i++) {
        local_var[i] = local_var[i] * local_var[i];
    }
    mean_filter(local_var, width, height, k_size);
    
    for (i = 0; i < size; i++) {
        local_var[i] = local_var[i] - local_mean[i] * local_mean[i];
    }
    
    if (noise < 0) {
        noise = _cal_mean(local_var, size);
    }
    
    for (i = 0; i < size; i++) {
        temp[i] = img[i] - local_mean[i];
        img[i] = local_var[i] - noise;
        img[i] = img[i] > 0 ? img[i] : 0;
    }
    
    for (i = 0; i < size; i++) {
        e = local_var[i] > noise ? local_var[i] : noise;
        //img[i] = local_mean[i] + temp[i] / e * img[i] ;
        img[i] = local_mean[i] +  img[i] / e * temp[i] ;
    }
    
    
    free(local_mean);
    free(local_var);
    free(temp);
}

