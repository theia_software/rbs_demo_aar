//
//  ISP_normalize.c
//  ET713_Image_Preprocessing
//

#include "ISP_normalize.h"
#include "ISP_filter.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "statistics.h"


void _local_normalize(float *img, int width, int height, float *bkg_estimate_filter, int f_radius)
{
    
    int i, size = width * height;
    float *temp = (float*)malloc(sizeof(float) * size);
    
    memcpy(temp, img, sizeof(float) * size);
	_sym_separable_filter(temp, width, height, bkg_estimate_filter, f_radius);
    
    for (i = 0; i < size; i++) {
        img[i] -= temp[i];
    }
    
    free(temp);
}


void _mean_std_normalize_roi(float *img, int size, float mean_out, float std_out, unsigned char *mask_roi)
{
    int i, roi_pixel = 0;
    float mean_in, std_in, var_in, var_out;
    float *temp = (float*)malloc(sizeof(float) * size);
    
    
    for (i = 0; i < size; i++) {
        if (mask_roi[i] != 0) {
            temp[roi_pixel] = img[i];
            roi_pixel++;
        }
    }
        
    _cal_mean_std(temp, roi_pixel, &mean_in, &std_in);
    var_in = std_in * std_in;
    var_out = std_out * std_out;
    
    for (i = 0; i < size; i++) {
        if (img[i] > mean_in) {
            img[i] = mean_out + sqrt(var_out * (img[i] - mean_in) * (img[i] - mean_in) / var_in);
        }
        else{
            img[i] = mean_out - sqrt(var_out * (img[i] - mean_in) * (img[i] - mean_in) / var_in);
        }
        
    }
    free(temp);
}





void _drop_data_out_of_range(float *data, int size, float max, float min)
{
    int i;
    
    for (i = 0; i < size; i++) {
        data[i] = data[i] >= min ? data[i] : min;
        data[i] = data[i] <= max ? data[i] : max;
    }
}


void linear_normalize(float *img, int size, float max_out, float min_out)
{
    int i;
    float max_in = find_max_float(img, size);
    float min_in = find_min_float(img, size);
    float range = max_out - min_out;
    float r = range / (max_in - min_in);
    
    
    
    for (i = 0; i < size; i++) {
        img[i] = (img[i] - min_in) * r + min_out;
    }
    
}

void moving_normalize(float *img, int width, int height, int win_size, float max_out, float min_out)
{
    int size = width * height, i, j, m, n, half_win_size;
    int i_start, i_end, j_start, j_end, b_size;
    float *img_smooth = (float*)malloc(sizeof(float) * size);
    float *img_diff = (float*)malloc(sizeof(float) * size), diff;
    float *img_normalize = (float*)malloc(sizeof(float) * size);
    float filter[6] = {0.07021647, 0.08147882, 0.09147316, 0.09935410,       0.10440495, 0.10614499};
    float g_sum, g_ave, g_var, g_max, g_min, g_delta, g;
    
    win_size = win_size + (win_size %2 == 0);
    half_win_size = (win_size - 1) >> 1;
    
    memcpy(img_smooth, img, sizeof(float) * size);
	_sym_separable_filter(img_smooth, width, height, filter, 5);
    
    for (i = 0; i < size; i++) {
            diff = img[i] - img_smooth[i];
            img_diff[i] = diff >= 0 ? diff : -diff;
    }

    
    for (i = 0 ; i < height; i++) {
        i_start = i - half_win_size;
        i_end   = i + half_win_size;
        i_start = i_start < 0 ? 0 : i_start;
        i_end   = i_end   > height - 1 ? height - 1 : i_end;
        
        for (j = 0; j < width; j++) {
            j_start = j - half_win_size;
            j_end   = j + half_win_size;
            j_start = j_start < 0 ? 0 : j_start;
            j_end   = j_end   > width - 1  ? width - 1 : j_end;
            
            g_sum = 0;
            for (m = i_start; m <= i_end; m++) {
                for (n = j_start; n <= j_end; n++) {
                    g_sum += img_diff[m * width + n];
                }
            }
            b_size = (i_end - i_start + 1) * (j_end - j_start + 1);
            g_var = g_sum / b_size * 4;
            g_ave = img_smooth[i * width + j];
            
            g_min = g_ave < g_var ? min_out : g_ave - g_var;
            g_max = g_ave > max_out - g_var ? max_out : g_ave + g_var;

            g_delta = g_max - g_min;
            g = img[i * width + j];
            
            if(g_delta == 0) {
                img_normalize[i * width + j] = g;
            }
            else{
                if(g <= g_min){
                    img_normalize[i * width + j]  = min_out;
                }
                else{
                    if(g >= g_max)
                        img_normalize[i * width + j] = max_out;
                    else
                        img_normalize[i * width + j] = (g - g_min) * max_out / g_delta;
                }
            }

        }
    }

    memcpy(img, img_normalize, sizeof(float) * size);
    
    free(img_normalize);
    free(img_smooth);
    free(img_diff);
    
}


void roi_normalize(float *img, int x1, int y1, int x2, int y2, int width, int height, float max_out, float min_out)
{
    
    int  i, j, size = width * height, idx;
    float temp, max_roi, min_roi, r;
    float range = max_out - min_out;
    float value;

    
    
    if (x1 > x2) {
        temp = x2;
        x2 = x1;
        x1 = temp;
    }
    
    if (y1 > y2) {
        temp = y2;
        y2 = y1;
        y1 = temp;
    }
    
    max_roi = img[y1*width+x1];
    min_roi = max_roi;
    
    
    for (i = y1; i <= y2; i++) {
        for (j = x1; j <= x2; j++) {
            idx = i*width+j;
            value = img[idx];
            if (value > max_roi) {
                max_roi = value;
            }
            if (value < min_roi) {
                min_roi = value;
            }
        }
    }

    
    r = range / (max_roi - min_roi);
    
    for (i = 0; i < size; i++) {
        value = img[i];
        value = (value - min_roi) * r + min_out;
        if (value < 0.0f) value = 0.0f;
        if (value > 255.0f) value = 255.0f;
        
        img[i] = value;
    }
    
    
}
