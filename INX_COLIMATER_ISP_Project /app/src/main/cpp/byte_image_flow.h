#ifndef BYTE_IMAGE_flow_h
#define byte_image_flow_h

#include <stdio.h>
#include "byte_image.h"

#ifdef WIN32
#define EGIS __declspec(dllexport)
#else
#define EGIS
#endif

EGIS int ExtractLocalMin(int** local_min_x, int** local_min_y, BYTE* dns, int image_h, int image_w, int radius, int half, int stride, int border, int nms, BYTE* mask, int patch_size);

EGIS int ExtractLocalMin_V2(int *local_min_x, int *local_min_y, BYTE* dns, int image_h, int image_w, int radius, int half, int stride, int border, int nms, BYTE* mask, int patch_size);

EGIS void ExtractAngle(float* local_min_ort, BYTE* ipp_image, int image_h, int image_w, int local_min_cnt, int radius, int angle_switch, int* local_min_x, int* local_min_y, BYTE* mask);

EGIS void ExtractAngle_small(float* local_min_ort, BYTE* ipp_image, int image_h, int image_w, int local_min_cnt, int radius, int angle_switch, int* local_min_x, int* local_min_y, BYTE* mask);

//EGIS void ExtractAngle_v2(float* local_min_ort,BYTE* ipp_image, int image_h, int image_w, int local_min_cnt, int radius, int angle_switch, int* local_min_x, int* local_min_y, BYTE* mask);

EGIS void DestroyKptsInfo(int* local_min_x, int* local_min_y, float* local_min_ort);

EGIS void ExtractAngleReliImg(float* ort_img, double* reli_img, BYTE* ipp_image, int image_h, int image_w, int local_min_cnt, int radius, int angle_switch, int* local_min_x, int* local_min_y, BYTE* mask);
#endif
