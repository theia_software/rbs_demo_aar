#include <stddef.h>
#include <math.h>
#include <stdio.h>
//#include <stdlib.h>
#include "fft.h"
#include "byte_image.h"

//#include "time.h"

#ifdef __cplusplus
extern "C" {
#endif

float **temp_G3AllocFloatImage(int width, int height) {
    int ptr_len = sizeof(float *) * height;
    BYTE *mem_buf = (BYTE *) G3Alloc(ptr_len + width * sizeof(float) * height);
    float **image = (float **) mem_buf;
    int i;
    if (!image) return NULL;
    mem_buf += ptr_len;

    for (i = 0; i < height; i++, mem_buf += width * sizeof(float)) {
        image[i] = mem_buf;
    }
    return image;
}

BYTE *CreateMaskByte(int image_h, int image_w, int center_trig, int radius_trig, float center_x, float center_y, float radius) {
    if (center_trig) {
        center_x = (int)image_w/2;
        center_y = (int)image_h/2;
    }
    if (radius_trig) {
        float min1 = G3Min(image_w, image_h);
        float min2 = G3Min(center_x, min1);
        radius = G3Min(center_y, min2);
    }

//    float **dst = temp_G3AllocFloatImage(image_w, image_h);
    float **dst = (float**)G3CAlloc(image_h, sizeof(float*));
    for (size_t i=0; i<image_h; i++){
        dst[i] = (float*)G3CAlloc(image_w, sizeof(float));
    }
    BYTE *mask = (BYTE *)G3CAlloc(image_h * image_w, sizeof(BYTE));

    for (int y = 0; y < image_h; y++) {
        for (int x = 0; x < image_w; x++) {
            dst[y][x] = sqrtf(G3Sqr(y - center_y) + G3Sqr(x - center_x));
            mask[y * image_w + x] = dst[y][x] <= radius ? 1 : 0;
        }
    }

    for (size_t i=0; i<image_h; i++){
        G3Free(dst[i]);
    }
    G3Free(dst);

    return mask;
}

void SetMaskByte(BYTE *image, int image_h, int image_w, int center_trig, int radius_trig, float center_x, float center_y, float radius) {
    if (center_trig) {
        center_x = (int)image_w/2;
        center_y = (int)image_h/2;
    }
    if (radius_trig) {
        float min1 = G3Min(image_w, image_h);
        float min2 = G3Min(center_x, min1);
        radius = G3Min(center_y, min2);
    }

//    TODO: Use G3AllocFloatImage
//    float **dst = temp_G3AllocFloatImage(image_w, image_h);
    float **dst = (float**)G3CAlloc(image_h, sizeof(float*));
    for (size_t i=0; i<image_h; i++){
        dst[i] = (float*)G3CAlloc(image_w, sizeof(float));
    }

    for (int y = 0; y < image_h; y++) {
        for (int x = 0; x < image_w; x++) {
            dst[y][x] = sqrtf(G3Sqr(y - center_y) + G3Sqr(x - center_x));
            image[y * image_w + x] = dst[y][x] <= radius ? 1 : 0;
        }
    }

    for (size_t i=0; i<image_h; i++){
        G3Free(dst[i]);
    }
    G3Free(dst);
}

void FindKeypointsNumber(int *point_i, int *point_j, int *point_cnt, int *local_min_cnt, const BYTE *image, size_t image_h, size_t image_w, int half, int stride, int ex_border, int nms, const BYTE *mask, int patch_size) {

//    int reserve_size = ((image_h - 2*half - 2*ex_border) > 0) ? image_h - 2*half - 2*ex_border : 0;
//    int max_reserve_size = 400;
    int tmp1 = patch_size / 2 + ex_border - half;
    int tmp2 = half + 1;
    int point_size;
    int alive;
    int i, j, x, y;
    int min_;
    int skip;
    int tmp3 = -stride;
    int center_x = image_w / 2;
    int center_y = image_h / 2;
    int *suppressed;
    float dist;

    *point_cnt = 0;
    *local_min_cnt = 0;

    for (i = tmp1; i < image_h - tmp1; i += stride) {
        for (j = tmp1; j < image_w - tmp1; j += stride) {
            if (mask != NULL && mask[i * image_w + j] == 0) {
                continue;
            } else {
                min_ = 300;
                for (y = i - half; y < i + tmp2; y++) {
                    skip = tmp3;
                    for (x = j - half; x < j + tmp2; x++) {
                        if (mask != NULL && mask[image_w * y + x] == 0) {
                            skip++;
                        } else {
                            if (image[image_w * y + x] < min_) {
                                min_ = image[image_w * y + x];
                            }
                        }
                    }
                    if (skip != tmp3) {
                        break;
                    }
                }

                if (skip == tmp3) {
                    if (min_ == image[image_w * i + j] && (mask != NULL || ((i - center_y) * (i - center_y) +
                                                                            (j - center_x) * (j - center_x)) <
                                                                           patch_size * patch_size)) {
//                        if (*point_cnt >= reserve_size) {
//                            printf("point_cnt = %d\n", *point_cnt);
//                            point_i = (int*)realloc(point_i, (*point_cnt)*sizeof(int));
//                            point_j = (int*)realloc(point_j, (*point_cnt)*sizeof(int));
//                            printf("%p,%p\n", point_i, point_j);
//                        }
                        point_i[*point_cnt] = i;
                        point_j[*point_cnt] = j;
                        (*point_cnt)++;
                    }
                } else {
                    if (skip > 0) {
                        j += skip;
                    }
                }
            }
        }
    }

    point_size = *point_cnt;
    suppressed = (int *)G3CAlloc(point_size, sizeof(int));

    for (i = 0; i < point_size; i++) {
        alive = 1;
        for (j = 0; j < point_size; j++) {
            if (point_i[i] == point_i[j] && point_j[i] == point_j[j]) {
                continue;
            }
            if (suppressed[j] == 1) {
                continue;
            }

            dist = sqrt(pow(point_i[i] - point_i[j], 2) + pow(point_j[i] - point_j[j], 2));
            if (dist <= nms && image[image_w * point_i[i] + point_j[i]] >= image[image_w * point_i[j] + point_j[j]]) {
                suppressed[i] = 1;
                alive = 0;
                break;
            }
        }

        if (alive == 1) {
            (*local_min_cnt)++;
        }
    }
    G3Free(suppressed);
}

void FindLocalMin(int *local_min_x, int *local_min_y, const BYTE *image, size_t image_h, size_t image_w, int nms, int point_cnt, int *point_i, int *point_j) {

    int i, j;
    int alive;
    int point_size;
    int locmin_cnt = 0;
    int *suppressed;
    float dist;

    point_size = point_cnt;
    suppressed = (int *)G3CAlloc(point_size, sizeof(int));

    for (i = 0; i < point_size; i++) {
        alive = 1;
        for (j = 0; j < point_size; j++) {
            if (point_i[i] == point_i[j] && point_j[i] == point_j[j]) {
                continue;
            }
            if (suppressed[j] == 1) {
                continue;
            }

            dist = sqrt(pow(point_i[i] - point_i[j], 2) + pow(point_j[i] - point_j[j], 2));
            if (dist <= nms && image[image_w * point_i[i] + point_j[i]] >= image[image_w * point_i[j] + point_j[j]]) {
                suppressed[i] = 1;
                alive = 0;
                break;
            }
        }

        if (alive == 1) {
            local_min_y[locmin_cnt] = point_i[i];
            local_min_x[locmin_cnt] = point_j[i];
            locmin_cnt++;
        }
    }
    G3Free(suppressed);
}

void TBinary8Bits(const BYTE* img, BYTE* out, int h, int w, int radius, const BYTE* mask)
{
    int force_break;

    for(int hh=0; hh < h-10; hh=hh+10)
    {
        for(int ww=0; ww < w-10; ww=ww+10)
        {
            float mean = 0.0;
            force_break = 0;
            for(int j=hh; j<hh+10; j++)
            {
                for(int i=ww; i<ww+10; i++)
                {
                    if(mask!= NULL && mask[w*j+i] == 0)//invalid
                    {
                        force_break = 1;
                        break;
                    }
                    mean += img[w*j + i];
                }

                if(force_break == 1)
                {
                    break;
                }
            }

            if(force_break == 1)
            {
                for(int j=hh; j<hh+10; j++)
                {
                    for(int i=ww; i<ww+10; i++)
                    {
                        out[w*j+i] = 0;
                    }
                }
                continue;
            }

            mean = mean / 100.0;
            float std = 0.0;
            for(int j=hh; j<hh+10; j++)
            {
                for(int i=ww; i<ww+10; i++)
                {
                    std += fabsf(img[w*j + i] - mean) * fabsf(img[w*j + i] - mean);
                }
            }
            std = sqrt(std / 100.0);

            for(int j=hh; j<hh+10; j++)
            {
                for(int i=ww; i<ww+10; i++)
                {
                    if((float)(img[w*j + i]) > mean - 0.25*std)
                    {
                        out[j*w+i] = 255;
                    }
                    else if((float)(img[w*j + i]) < mean - 0.25*std)
                    {
                        out[j*w+i] = 0;
                    }
                    else
                    {
                        out[j*w+i] = img[w*j + i];
                    }
                }
            }
        }
    }

//    for(int j=0; j<h; j++)
//    {
//        for(int i=0; i<w; i++)
//        {
//            if(((i-w/2)*(i-w/2) + (j-h/2)*(j-h/2)) > radius*radius)
//            {
//                out[w*j + i] = 0;
//            }
//        }
//    }
}

void CreateGaussianKernel(float* gauss,int size,float param)
{
    float* gauss1d = (float*)malloc(size * sizeof(float));
    float kernel_sum = 0.0;
    float x;

    for (int i = 0; i < size; i++)
    {
        x = i-(size-1)/2;
        gauss1d[i] = exp(-(x*x / (2 * param*param)));
        kernel_sum += gauss1d[i];
    }
    for (int i = 0; i < size; i++)
    {
        gauss1d[i] /= kernel_sum;
    }

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            gauss[size*j + i] = gauss1d[i] * gauss1d[j];
        }
    }

    free(gauss1d);
}

void CreateGaussianKernelFp(int* fp_gauss,int size,float param)
{
    float* gauss1d = (float*)malloc(size * sizeof(float));
    float kernel_sum = 0.0;
    float x;

    for (int i = 0; i < size; i++)
    {
        x = i-(size-1)/2;
        gauss1d[i] = exp(-(x*x / (2 * param*param)));
        kernel_sum += gauss1d[i];
    }
    for (int i = 0; i < size; i++)
    {
        gauss1d[i] /= kernel_sum;
    }

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            fp_gauss[size*j + i] = FP_VALUE(gauss1d[i] * gauss1d[j]);
        }
    }

    free(gauss1d);

}

void CreateGraident(float* f, float* gauss, int size, unsigned int direction)
{
    unsigned int i, j;
    if (direction == 0)//y
    {
        for (j = 0; j < size; j++)
        {
            for (i = 0; i < size; i++)
            {
                if (j == 0)
                {
                    f[size*j + i] = gauss[size*(j + 1) + i] - gauss[size*j + i];
                }
                else if (j == size - 1)
                {
                    f[size*j + i] = gauss[size*j + i] - gauss[size*(j - 1) + i];
                }
                else
                {
                    f[size*j + i] = (gauss[size*(j + 1) + i] - gauss[size*(j - 1) + i]) / 2;
                }
            }
        }
    }
    else // x
    {
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                if (i == 0)
                {
                    f[size*j + i] = gauss[size*j + i + 1] - gauss[size*j + i];
                }
                else if (i == size - 1)
                {
                    f[size*j + i] = gauss[size*j + i] - gauss[size*j + (i - 1)];
                }
                else
                {
                    f[size*j + i] = (gauss[size*(j)+i + 1] - gauss[size*(j)+i - 1]) / 2;
                }
            }
        }
    }
}

void CreateGraidentFp(int* fp_f, float* gauss, int size, unsigned int direction)
{
    unsigned int i, j;
    if (direction == 0)//y
    {
        for (j = 0; j < size; j++)
        {
            for (i = 0; i < size; i++)
            {
                if (j == 0)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*(j + 1) + i] - gauss[size*j + i]);
                }
                else if (j == size - 1)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*j + i] - gauss[size*(j - 1) + i]);
                }
                else
                {
                    fp_f[size*j + i] = FP_VALUE((gauss[size*(j + 1) + i] - gauss[size*(j - 1) + i]) / 2);
                }
            }
        }
    }
    else // x
    {
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                if (i == 0)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*j + i + 1] - gauss[size*j + i]);
                }
                else if (i == size - 1)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*j + i] - gauss[size*j + (i - 1)]);
                }
                else
                {
                    fp_f[size*j + i] = FP_VALUE((gauss[size*(j)+i + 1] - gauss[size*(j)+i - 1]) / 2);
                }
            }
        }
    }
}


void ConvolutionWithPaddingZero(unsigned short* image, unsigned int image_w, unsigned int image_h, float* Gx, float* Gy, float* Gxy_, float* fx, float* fy,unsigned int size)
{
    unsigned int x, y;
    unsigned int i, j;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            double element = 0.0;
            double element2 = 0.0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;
                    if (im_x >= 0 && im_y >= 0 && im_x < image_w && im_y < image_h)
                    {
                        element += image[image_w * im_y + im_x] * fx[(temp - 1) - (j*size + i)];
                        element2 += image[image_w * im_y + im_x] * fy[(temp - 1) - (j*size + i)];
                    }
                }
            }
            Gx[image_w * y + x] = element * element;
            Gy[image_w * y + x] = element2 * element2;
            Gxy_[image_w * y + x] = element * element2;
        }
    }
}


void ConvolutionWithPaddingZeroFloat(float* image, unsigned int image_w, unsigned int image_h, float* Gx, float* Gy, float* Gxy_, float* fx, float* fy,unsigned int size)
{
    unsigned int x, y;
    unsigned int i, j;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            double element = 0.0;
            double element2 = 0.0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;
                    if (im_x >= 0 && im_y >= 0 && im_x < image_w && im_y < image_h)
                    {
                        element += image[image_w * im_y + im_x] * fx[(temp - 1) - (j*size + i)];
                        element2 += image[image_w * im_y + im_x] * fy[(temp - 1) - (j*size + i)];
                    }
                }
            }
            Gx[image_w * y + x] = element * element;
            Gy[image_w * y + x] = element2 * element2;
            Gxy_[image_w * y + x] = element * element2;
        }
    }
}

void ConvolutionWithPaddingZeroFloatFp(int* fp_image, unsigned int image_w, unsigned int image_h, int* fp_Gx, int* fp_Gy, int* fp_Gxy_, int* fp_fx, int* fp_fy,unsigned int size)
{
    unsigned int x, y;
    unsigned int i, j;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    unsigned int tmp1,tmp2,tmp3;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            int fp_element = 0;
            int fp_element2 = 0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;
                    if (im_x >= 0 && im_y >= 0 && im_x < image_w && im_y < image_h)
                    {
                        tmp1 = image_w * im_y + im_x;
                        tmp2 = (temp - 1) - (j*size + i);
                        fp_element = FP_ADD(fp_element,FP_MULTI(fp_image[tmp1],fp_fx[tmp2 ]));
                        fp_element2 = FP_ADD(fp_element2,FP_MULTI(fp_image[tmp1],fp_fy[tmp2 ]));
                    }
                }
            }
            tmp3 = image_w * y + x;
            fp_Gx[tmp3] = (FP_MULTI(fp_element,fp_element));
            fp_Gy[tmp3] = (FP_MULTI(fp_element2,fp_element2));
            fp_Gxy_[tmp3] = (FP_MULTI(fp_element,fp_element2));
        }
    }
}

void ConvolutionWithPaddingMirror(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* Gx, float* Gy, float* Gxy_, float* gauss, unsigned int size)
{
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    float element = 0.0;
    float element2 = 0.0;
    float element3 = 0.0;

    for (int y = 0; y < image_h; y++)
    {
        for (int x = 0; x < image_w; x++)
        {
            element = 0.0;
            element2 = 0.0;
            element3 = 0.0;
            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;

                    if (im_x < 0)
                    {
                        im_x = -(im_x + 1);
                    }
                    else if (im_x >= image_w)
                    {
                        im_x = (image_w - 1) - (im_x - image_w);
                    }

                    if (im_y < 0)
                    {
                        im_y = -(im_y + 1);
                    }
                    else if (im_y >= image_h)
                    {
                        im_y = (image_h - 1) - (im_y - image_h);
                    }

                    element += Gx[image_w * im_y + im_x] * gauss[(temp - 1) - (j*size + i)];
                    element2 += Gy[image_w * im_y + im_x] * gauss[(temp - 1) - (j*size + i)];
                    element3 += Gxy_[image_w * im_y + im_x] * gauss[(temp - 1) - (j*size + i)];

                }
            }

            Gxx[image_w * y + x] = element;
            Gyy[image_w * y + x] = element2;
            Gxy[image_w * y + x] = element3 * 2;
        }
    }
}

void ConvolutionWithPaddingMirrorFp(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, int* fp_Gx, int* fp_Gy, int* fp_Gxy_, int* fp_gauss, unsigned int size)
{
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    int element = 0;
    int element2 = 0;
    int element3 = 0;
    unsigned int tmp1,tmp2,tmp3,tmp4,tmp5;

    tmp1 = 2*image_w;
    tmp2 = 2*image_h;

    for (int y = 0; y < image_h; y++)
    {
        for (int x = 0; x < image_w; x++)
        {
            element = 0;
            element2 = 0;
            element3 = 0;
            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;

                    if (im_x < 0)
                    {
                        im_x = -1-im_x;
                    }
                    else if (im_x >= image_w)
                    {
                        im_x = tmp1-1-im_x;
                    }

                    if (im_y < 0)
                    {
                        im_y = -1-im_y;
                    }
                    else if (im_y >= image_h)
                    {
                        im_y = tmp2-1-im_y;
                    }

                    tmp3 = image_w * im_y + im_x;
                    tmp4 = (temp - 1) - (j*size + i);
                    element = FP_ADD(element,FP_MULTI(fp_Gx[tmp3],fp_gauss[tmp4]));
                    element2 = FP_ADD(element2,FP_MULTI(fp_Gy[tmp3],fp_gauss[tmp4]));
                    element3 = FP_ADD(element3,FP_MULTI(fp_Gxy_[tmp3],fp_gauss[tmp4]));

                }
            }

            tmp5 = image_w * y + x;
            Gxx[tmp5] = (element);
            Gyy[tmp5] = (element2);
            Gxy[tmp5] = (element3*2);
        }
    }
}

void ConvolutionWithPaddingMirrorWith2(unsigned int image_w, unsigned int image_h, float* sin2theta, float* cos2theta, float* pixel_ort, unsigned int size)
{
    unsigned i, j,x,y;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    float element;
    float element2;
    int im_x;
    int im_y;
    unsigned int tmp1,tmp2,tmp3;
    float tmp4;

    tmp1 = 2*image_w;
    tmp2 = 2*image_h;
    tmp4 = M_PI / 2.0;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            element = 0.0;
            element2 = 0.0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    im_x = x + i - radius;
                    im_y = y + j - radius;
                    //border mode reflect
                    if (im_x < 0)
                    {
                        im_x = -1-im_x;
                    }
                    else if (im_x >= image_w)
                    {
                        im_x = tmp1 - 1 - im_x;
                    }
                    if (im_y < 0)
                    {
                        im_y = -1-im_y;
                    }
                    else if (im_y >= image_h)
                    {
                        im_y = tmp2 - 1 - im_y;
                    }

                    tmp3 = image_w * im_y + im_x;
                    element += sin2theta[tmp3];
                    element2 += cos2theta[tmp3];
                }
            }

            pixel_ort[image_w * y + x] = tmp4 + atan2(element / (temp), element2 / (temp)) / 2.0;
        }
    }
}

unsigned int CheckPowerOfTwo(unsigned int in)
{
    if(in > 32 && in <=64)
    {
        return 64;
    }
    else if(in > 64 && in <=128)
    {
        return 128;
    }
    else if(in > 128 && in <=256)
    {
        return 256;
    }
    else if(in > 256 && in <=512)
    {
        return 512;
    }
    else
    {
        printf("fft size is over 1024, too big\n");
        return 1024;
    }
}

void FFTWithPaddingMirrorWith2(fft_data* pParam)
{
    unsigned int i, j;
    unsigned int tmp1,tmp2;
    float tmp4,tmp5,tmp6,tmp7;

    //copy gauss data
    tmp7 = 1.0/(pParam->kernel_reference_size*pParam->kernel_reference_size);
    for (i=0;i<pParam->kernel_reference_size;i++)
    {
        for (j=0;j<pParam->kernel_reference_size;j++)
        {
            pParam->kernel_in[i*pParam->fft_size+j].Re = tmp7;
        }
    }

    //do kernel fft translation
    FFT2D(pParam->kernel_in, pParam->kernel_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft

    //copy center part
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy center-left part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(pParam->mirror_extra_half_size-j-1);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy center-right part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->raw1_reference_size+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy upper part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy down part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(j);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy upper-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy upper-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy down-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy down-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //do raw fft translation
    FFT2D(pParam->raw1_in, pParam->raw1_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw2_in, pParam->raw2_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft

    // do dot-multi of raw and kernel fft.
    for (i=0;i<pParam->fft_size;i++)
    {
        for (j=0;j<pParam->fft_size;j++)
        {
            tmp1 = i*pParam->fft_size+j;
            EE(&(pParam->raw1_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw1_out[tmp1]));
            EE(&(pParam->raw2_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw2_out[tmp1]));
        }
    }

    //do ifft translation
    iFFT2D_transpose(pParam->raw1_out, pParam->raw1_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw2_out, pParam->raw2_in,pParam->fft_size);

    //crop answer part to out
    tmp1 = pParam->mirror_extra_half_size*2;
    tmp4 = M_PI / 2.0;
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            tmp5 = pParam->raw1_in[(i + tmp1)*pParam->fft_size+j+tmp1].Re;
            tmp6 = pParam->raw2_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re;
            pParam->out1[i*pParam->raw1_reference_size+j] = tmp4+atan2(tmp5, tmp6)/2.0;
        }
    }

}

void FFTWithPaddingMirrorWith3(fft_data* pParam)
{
    unsigned int i, j;
    unsigned int tmp1,tmp2;

    //copy gauss data
    for (i=0;i<pParam->kernel_reference_size;i++)
    {
        for (j=0;j<pParam->kernel_reference_size;j++)
        {
            pParam->kernel_in[i*pParam->fft_size+j].Re = pParam->kernel_reference[i*pParam->kernel_reference_size+j];
        }
    }

    //do kernel fft translation
    FFT2D(pParam->kernel_in, pParam->kernel_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft

    //copy center part
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy center-left part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(pParam->mirror_extra_half_size-j-1);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy center-right part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->raw1_reference_size+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy upper part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy down part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(j);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy upper-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy upper-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy down-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy down-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //do raw fft translation

    FFT2D(pParam->raw1_in, pParam->raw1_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw2_in, pParam->raw2_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw3_in, pParam->raw3_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft


    // do dot-multi of raw and kernel fft.
    for (i=0;i<pParam->fft_size;i++)
    {
        for (j=0;j<pParam->fft_size;j++)
        {
            tmp1 = i*pParam->fft_size+j;
            EE(&(pParam->raw1_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw1_out[tmp1]));
            EE(&(pParam->raw2_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw2_out[tmp1]));
            EE(&(pParam->raw3_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw3_out[tmp1]));
        }
    }

    //do ifft translation
    iFFT2D_transpose(pParam->raw1_out, pParam->raw1_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw2_out, pParam->raw2_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw3_out, pParam->raw3_in,pParam->fft_size);

    //crop answer part to Gxx,Gyy,Gxy

    tmp1 = pParam->mirror_extra_half_size*2;
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            pParam->out1[i*pParam->raw1_reference_size+j]=  pParam->raw1_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re;
            pParam->out2[i*pParam->raw1_reference_size+j] = pParam->raw2_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re;
            pParam->out3[i*pParam->raw1_reference_size+j] = pParam->raw3_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re*2;
        }
    }
}

void CalTheta(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* sin2theta, float* cos2theta)
{
    double denom;
    double gxx_yy;
    double gxx_yy_2;
    double gxy_2;
    unsigned int x, y;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            gxx_yy = (double)(Gxx[image_w * y + x]) - (double)(Gyy[image_w * y + x]);
            gxx_yy_2 = gxx_yy * gxx_yy;
            gxy_2 = (double)(Gxy[image_w * y + x]) * (double)(Gxy[image_w * y + x]);
            denom = sqrt(gxy_2 + gxx_yy_2) ;//+ numeric_limits<double>::epsilon();
            sin2theta[image_w * y + x] = Gxy[image_w * y + x] / denom;
            cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / denom;
        }
    }
}

void CalThetaFp(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* sin2theta, float* cos2theta)
{
//    double denom;
//    double gxx_yy;
//    double gxx_yy_2;
//    double gxy_2;
    unsigned int x, y;

    int long long fp_denom;
    int long long fp_gxx_yy;
    int long long fp_gxx_yy_2;
    int long long fp_gxy_2;

    float sin_tmp;
//    float cos_tmp;


    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
#if 0
            gxx_yy = (double)(Gxx[image_w * y + x]) - (double)(Gyy[image_w * y + x]);
            gxx_yy_2 = gxx_yy * gxx_yy;
            gxy_2 = (double)(Gxy[image_w * y + x]) * (double)(Gxy[image_w * y + x]);
            denom = sqrt(gxy_2 + gxx_yy_2) ;//+ numeric_limits<double>::epsilon();
            sin2theta[image_w * y + x] = Gxy[image_w * y + x] / denom;
            cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / denom;
#endif
#if 1
            fp_gxx_yy = (int long long)(Gxx[image_w * y + x] - Gyy[image_w * y + x]);
            fp_gxx_yy_2 = fp_gxx_yy * fp_gxx_yy;
            fp_gxy_2 = (int long long)(Gxy[image_w * y + x] * Gxy[image_w * y + x]);
            fp_denom = sqrt(fp_gxy_2 + fp_gxx_yy_2) ;//+ numeric_limits<double>::epsilon();
            //if(fp_denom != 0)
            {
                sin2theta[image_w * y + x] = Gxy[image_w * y + x] / fp_denom;
                cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / fp_denom;
            }
#if 0
            printf("[%d][%d](%f,%f) (%f,%f)\n",y,x,
                   sin2theta[image_w * y + x],
                   cos2theta[image_w * y + x],
                   sin_tmp,
                   cos_tmp);
            getchar();
#endif
            //sin2theta[image_w * y + x] = sin_tmp;
            //cos2theta[image_w * y + x] = cos_tmp;
#endif
        }
    }
}

void create_gaussian_kernel(float* gauss,int size,float param)
{
    float* gauss1d = (float*)malloc(size * sizeof(float));
    float kernel_sum = 0.0;
    float x;

    for (int i = 0; i < size; i++)
    {
        x = i-(size-1)/2;
        gauss1d[i] = exp(-(x*x / (2 * param*param)));
        kernel_sum += gauss1d[i];
    }
    for (int i = 0; i < size; i++)
    {
        gauss1d[i] /= kernel_sum;
    }

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            gauss[size*j + i] = gauss1d[i] * gauss1d[j];
        }
    }

    free(gauss1d);

}

void create_gaussian_kernel_fp(int* fp_gauss,int size,float param)
{
    float* gauss1d = (float*)malloc(size * sizeof(float));
    float kernel_sum = 0.0;
    float x;

    for (int i = 0; i < size; i++)
    {
        x = i-(size-1)/2;
        gauss1d[i] = exp(-(x*x / (2 * param*param)));
        kernel_sum += gauss1d[i];
    }
    for (int i = 0; i < size; i++)
    {
        gauss1d[i] /= kernel_sum;
    }

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            fp_gauss[size*j + i] = FP_VALUE(gauss1d[i] * gauss1d[j]);
        }
    }

    free(gauss1d);

}

void create_graident_function(float* f, float* gauss, int size, unsigned int direction)
{
    unsigned int i, j;
    if (direction == 0)//y
    {
        for (j = 0; j < size; j++)
        {
            for (i = 0; i < size; i++)
            {
                if (j == 0)
                {
                    f[size*j + i] = gauss[size*(j + 1) + i] - gauss[size*j + i];
                }
                else if (j == size - 1)
                {
                    f[size*j + i] = gauss[size*j + i] - gauss[size*(j - 1) + i];
                }
                else
                {
                    f[size*j + i] = (gauss[size*(j + 1) + i] - gauss[size*(j - 1) + i]) / 2;
                }
            }
        }
    }
    else // x
    {
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                if (i == 0)
                {
                    f[size*j + i] = gauss[size*j + i + 1] - gauss[size*j + i];
                }
                else if (i == size - 1)
                {
                    f[size*j + i] = gauss[size*j + i] - gauss[size*j + (i - 1)];
                }
                else
                {
                    f[size*j + i] = (gauss[size*(j)+i + 1] - gauss[size*(j)+i - 1]) / 2;
                }
            }
        }
    }
}

void create_graident_function_fp(int* fp_f, float* gauss, int size, unsigned int direction)
{
    unsigned int i, j;
    if (direction == 0)//y
    {
        for (j = 0; j < size; j++)
        {
            for (i = 0; i < size; i++)
            {
                if (j == 0)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*(j + 1) + i] - gauss[size*j + i]);
                }
                else if (j == size - 1)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*j + i] - gauss[size*(j - 1) + i]);
                }
                else
                {
                    fp_f[size*j + i] = FP_VALUE((gauss[size*(j + 1) + i] - gauss[size*(j - 1) + i]) / 2);
                }
            }
        }
    }
    else // x
    {
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                if (i == 0)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*j + i + 1] - gauss[size*j + i]);
                }
                else if (i == size - 1)
                {
                    fp_f[size*j + i] = FP_VALUE(gauss[size*j + i] - gauss[size*j + (i - 1)]);
                }
                else
                {
                    fp_f[size*j + i] = FP_VALUE((gauss[size*(j)+i + 1] - gauss[size*(j)+i - 1]) / 2);
                }
            }
        }
    }
}

void convolution_with_padding_zero(unsigned short* image, unsigned int image_w, unsigned int image_h, float* Gx, float* Gy, float* Gxy_, float* fx, float* fy,unsigned int size)
{
    unsigned int x, y;
    unsigned int i, j;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            double element = 0.0;
            double element2 = 0.0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;
                    if (im_x >= 0 && im_y >= 0 && im_x < image_w && im_y < image_h)
                    {
                        element += image[image_w * im_y + im_x] * fx[(temp - 1) - (j*size + i)];
                        element2 += image[image_w * im_y + im_x] * fy[(temp - 1) - (j*size + i)];
                    }
                }
            }
            Gx[image_w * y + x] = element * element;
            Gy[image_w * y + x] = element2 * element2;
            Gxy_[image_w * y + x] = element * element2;
        }
    }
}


void convolution_with_padding_zero_float(float* image, unsigned int image_w, unsigned int image_h, float* Gx, float* Gy, float* Gxy_, float* fx, float* fy,unsigned int size)
{
    unsigned int x, y;
    unsigned int i, j;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            double element = 0.0;
            double element2 = 0.0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;
                    if (im_x >= 0 && im_y >= 0 && im_x < image_w && im_y < image_h)
                    {
                        element += image[image_w * im_y + im_x] * fx[(temp - 1) - (j*size + i)];
                        element2 += image[image_w * im_y + im_x] * fy[(temp - 1) - (j*size + i)];
                    }
                }
            }
            Gx[image_w * y + x] = element * element;
            Gy[image_w * y + x] = element2 * element2;
            Gxy_[image_w * y + x] = element * element2;
        }
    }
}

void convolution_with_padding_zero_float_fp(int* fp_image, unsigned int image_w, unsigned int image_h, int* fp_Gx, int* fp_Gy, int* fp_Gxy_, int* fp_fx, int* fp_fy,unsigned int size)
{
    unsigned int x, y;
    unsigned int i, j;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    unsigned int tmp1,tmp2,tmp3;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            int fp_element = 0;
            int fp_element2 = 0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;
                    if (im_x >= 0 && im_y >= 0 && im_x < image_w && im_y < image_h)
                    {
                        tmp1 = image_w * im_y + im_x;
                        tmp2 = (temp - 1) - (j*size + i);
                        fp_element = FP_ADD(fp_element,FP_MULTI(fp_image[tmp1],fp_fx[tmp2 ]));
                        fp_element2 = FP_ADD(fp_element2,FP_MULTI(fp_image[tmp1],fp_fy[tmp2 ]));
                    }
                }
            }
            tmp3 = image_w * y + x;
            fp_Gx[tmp3] = (FP_MULTI(fp_element,fp_element));
            fp_Gy[tmp3] = (FP_MULTI(fp_element2,fp_element2));
            fp_Gxy_[tmp3] = (FP_MULTI(fp_element,fp_element2));
        }
    }
}



void convolution_with_padding_mirror_with_3(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* Gx, float* Gy, float* Gxy_, float* gauss, unsigned int size)
{
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    float element = 0.0;
    float element2 = 0.0;
    float element3 = 0.0;

    for (int y = 0; y < image_h; y++)
    {
        for (int x = 0; x < image_w; x++)
        {
            element = 0.0;
            element2 = 0.0;
            element3 = 0.0;
            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;

                    if (im_x < 0)
                    {
                        im_x = -(im_x + 1);
                    }
                    else if (im_x >= image_w)
                    {
                        im_x = (image_w - 1) - (im_x - image_w);
                    }

                    if (im_y < 0)
                    {
                        im_y = -(im_y + 1);
                    }
                    else if (im_y >= image_h)
                    {
                        im_y = (image_h - 1) - (im_y - image_h);
                    }

                    element += Gx[image_w * im_y + im_x] * gauss[(temp - 1) - (j*size + i)];
                    element2 += Gy[image_w * im_y + im_x] * gauss[(temp - 1) - (j*size + i)];
                    element3 += Gxy_[image_w * im_y + im_x] * gauss[(temp - 1) - (j*size + i)];

                }
            }

            Gxx[image_w * y + x] = element;
            Gyy[image_w * y + x] = element2;
            Gxy[image_w * y + x] = element3 * 2;
        }
    }
}

void convolution_with_padding_mirror_with_3_fp(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, int* fp_Gx, int* fp_Gy, int* fp_Gxy_, int* fp_gauss, unsigned int size)
{
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    int element = 0;
    int element2 = 0;
    int element3 = 0;
    unsigned int tmp1,tmp2,tmp3,tmp4,tmp5;

    tmp1 = 2*image_w;
    tmp2 = 2*image_h;

    for (int y = 0; y < image_h; y++)
    {
        for (int x = 0; x < image_w; x++)
        {
            element = 0;
            element2 = 0;
            element3 = 0;
            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    int im_x = x + i - radius;
                    int im_y = y + j - radius;

                    if (im_x < 0)
                    {
                        im_x = -1-im_x;
                    }
                    else if (im_x >= image_w)
                    {
                        im_x = tmp1-1-im_x;
                    }

                    if (im_y < 0)
                    {
                        im_y = -1-im_y;
                    }
                    else if (im_y >= image_h)
                    {
                        im_y = tmp2-1-im_y;
                    }

                    tmp3 = image_w * im_y + im_x;
                    tmp4 = (temp - 1) - (j*size + i);
                    element = FP_ADD(element,FP_MULTI(fp_Gx[tmp3],fp_gauss[tmp4]));
                    element2 = FP_ADD(element2,FP_MULTI(fp_Gy[tmp3],fp_gauss[tmp4]));
                    element3 = FP_ADD(element3,FP_MULTI(fp_Gxy_[tmp3],fp_gauss[tmp4]));

                }
            }

            tmp5 = image_w * y + x;
            Gxx[tmp5] = (element);
            Gyy[tmp5] = (element2);
            Gxy[tmp5] = (element3*2);
        }
    }
}

unsigned int check_power_of_two(unsigned int in)
{
    if(in > 32 && in <=64)
    {
        return 64;
    }
    else if(in > 64 && in <=128)
    {
        return 128;
    }
    else if(in > 128 && in <=256)
    {
        return 256;
    }
    else if(in > 256 && in <=512)
    {
        return 512;
    }
    else
    {
        printf("fft size is over 1024, too big\n");
        return 1024;
    }
}

void fft_with_padding_mirror_with_2(fft_data* pParam)
{
    unsigned int i, j;
    unsigned int tmp1,tmp2,tmp3;
    float tmp4,tmp5,tmp6,tmp7;

    //copy gauss data
    tmp7 = 1.0/(pParam->kernel_reference_size*pParam->kernel_reference_size);
    for (i=0;i<pParam->kernel_reference_size;i++)
    {
        for (j=0;j<pParam->kernel_reference_size;j++)
        {
            pParam->kernel_in[i*pParam->fft_size+j].Re = tmp7;
        }
    }

    //do kernel fft translation
    FFT2D(pParam->kernel_in, pParam->kernel_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft

    //copy center part
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy center-left part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(pParam->mirror_extra_half_size-j-1);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy center-right part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->raw1_reference_size+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy upper part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy down part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(j);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy upper-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy upper-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy down-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //copy down-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
        }
    }

    //do raw fft translation
    FFT2D(pParam->raw1_in, pParam->raw1_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw2_in, pParam->raw2_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft

    // do dot-multi of raw and kernel fft.
    for (i=0;i<pParam->fft_size;i++)
    {
        for (j=0;j<pParam->fft_size;j++)
        {
            tmp1 = i*pParam->fft_size+j;
            EE(&(pParam->raw1_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw1_out[tmp1]));
            EE(&(pParam->raw2_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw2_out[tmp1]));
        }
    }

    //do ifft translation
    iFFT2D_transpose(pParam->raw1_out, pParam->raw1_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw2_out, pParam->raw2_in,pParam->fft_size);

    //crop answer part to out
    tmp1 = pParam->mirror_extra_half_size*2;
    tmp4 = M_PI / 2.0;
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            tmp5 = pParam->raw1_in[(i + tmp1)*pParam->fft_size+j+tmp1].Re;
            tmp6 = pParam->raw2_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re;
            pParam->out1[i*pParam->raw1_reference_size+j] = tmp4+atan2(tmp5, tmp6)/2.0;
        }
    }

}


void fft_with_padding_mirror_with_3(fft_data* pParam)
{
    unsigned int i, j;
    unsigned int tmp1,tmp2,tmp3;

    //copy gauss data
    for (i=0;i<pParam->kernel_reference_size;i++)
    {
        for (j=0;j<pParam->kernel_reference_size;j++)
        {
            pParam->kernel_in[i*pParam->fft_size+j].Re = pParam->kernel_reference[i*pParam->kernel_reference_size+j];
        }
    }

    //do kernel fft translation
    FFT2D(pParam->kernel_in, pParam->kernel_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft

    //copy center part
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy center-left part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(pParam->mirror_extra_half_size-j-1);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy center-right part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->raw1_reference_size+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy upper part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+j;
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy down part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(j);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy upper-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy upper-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy down-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //copy down-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = pParam->raw1_reference[tmp2];
            pParam->raw2_in[tmp1].Re = pParam->raw2_reference[tmp2];
            pParam->raw3_in[tmp1].Re = pParam->raw3_reference[tmp2];
        }
    }

    //do raw fft translation

    FFT2D(pParam->raw1_in, pParam->raw1_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw2_in, pParam->raw2_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw3_in, pParam->raw3_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft


    // do dot-multi of raw and kernel fft.
    for (i=0;i<pParam->fft_size;i++)
    {
        for (j=0;j<pParam->fft_size;j++)
        {
            tmp1 = i*pParam->fft_size+j;
            EE(&(pParam->raw1_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw1_out[tmp1]));
            EE(&(pParam->raw2_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw2_out[tmp1]));
            EE(&(pParam->raw3_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw3_out[tmp1]));
        }
    }

    //do ifft translation
    iFFT2D_transpose(pParam->raw1_out, pParam->raw1_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw2_out, pParam->raw2_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw3_out, pParam->raw3_in,pParam->fft_size);

    //crop answer part to Gxx,Gyy,Gxy

    tmp1 = pParam->mirror_extra_half_size*2;
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            pParam->out1[i*pParam->raw1_reference_size+j]=  pParam->raw1_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re;
            pParam->out2[i*pParam->raw1_reference_size+j] = pParam->raw2_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re;
            pParam->out3[i*pParam->raw1_reference_size+j] = pParam->raw3_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re*2;
        }
    }
}

void fft_with_padding_mirror_with_3_fp(fft_data* pParam)
{
    unsigned int i, j;
    unsigned int tmp1,tmp2,tmp3;

    //copy gauss data
    for (i=0;i<pParam->kernel_reference_size;i++)
    {
        for (j=0;j<pParam->kernel_reference_size;j++)
        {
            pParam->kernel_in[i*pParam->fft_size+j].Re = pParam->kernel_reference[i*pParam->kernel_reference_size+j];
        }
    }

    //do kernel fft translation
    FFT2D(pParam->kernel_in, pParam->kernel_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft

    //copy center part
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy center-left part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(pParam->mirror_extra_half_size-j-1);
            tmp2 = i*pParam->raw1_reference_size+j;
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy center-right part
    for (i=0;i<pParam->raw1_reference_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->raw1_reference_size+pParam->mirror_extra_half_size);
            tmp2 = i*pParam->raw1_reference_size+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy upper part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+j;
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy down part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->raw1_reference_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+(j+pParam->mirror_extra_half_size);
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(j);
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy upper-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy upper-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = (i*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->mirror_extra_half_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy down-left part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j+pParam->raw1_reference_size+pParam->mirror_extra_half_size;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->mirror_extra_half_size-j-1);
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //copy down-right part
    for (i=0;i<pParam->mirror_extra_half_size;i++) //height direction
    {
        for (j=0;j<pParam->mirror_extra_half_size;j++) //width direction
        {
            tmp1 = ((i+pParam->raw1_reference_size+pParam->mirror_extra_half_size)*pParam->fft_size)+j;
            tmp2 = ((pParam->raw1_reference_size-i-1)*pParam->raw1_reference_size)+(pParam->raw1_reference_size-j-1);
            pParam->raw1_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw1_reference[tmp2]);
            pParam->raw2_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw2_reference[tmp2]);
            pParam->raw3_in[tmp1].Re = FP_TO_NONFP(pParam->fp_raw3_reference[tmp2]);
        }
    }

    //do raw fft translation

    FFT2D(pParam->raw1_in, pParam->raw1_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw2_in, pParam->raw2_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft
    FFT2D(pParam->raw3_in, pParam->raw3_out,pParam->fft_size); // pParam->kernel_out is transpose matrix of 2d fft


    // do dot-multi of raw and kernel fft.
    for (i=0;i<pParam->fft_size;i++)
    {
        for (j=0;j<pParam->fft_size;j++)
        {
            tmp1 = i*pParam->fft_size+j;
            EE(&(pParam->raw1_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw1_out[tmp1]));
            EE(&(pParam->raw2_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw2_out[tmp1]));
            EE(&(pParam->raw3_out[tmp1]),&(pParam->kernel_out[tmp1]),&(pParam->raw3_out[tmp1]));
        }
    }

    //do ifft translation
    iFFT2D_transpose(pParam->raw1_out, pParam->raw1_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw2_out, pParam->raw2_in,pParam->fft_size);
    iFFT2D_transpose(pParam->raw3_out, pParam->raw3_in,pParam->fft_size);

    //crop answer part to Gxx,Gyy,Gxy

    tmp1 = pParam->mirror_extra_half_size*2;
    for (i=0;i<pParam->raw1_reference_size;i++)
    {
        for (j=0;j<pParam->raw1_reference_size;j++)
        {
            pParam->out1[i*pParam->raw1_reference_size+j] = (pParam->raw1_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re);
            pParam->out2[i*pParam->raw1_reference_size+j] = (pParam->raw2_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re);
            pParam->out3[i*pParam->raw1_reference_size+j] = (pParam->raw3_in[(i+tmp1)*pParam->fft_size+j+tmp1].Re*2);
        }
    }
}


void cal_theata(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* sin2theta, float* cos2theta)
{
    double denom;
    double gxx_yy;
    double gxx_yy_2;
    double gxy_2;
    unsigned int x, y;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            gxx_yy = (double)(Gxx[image_w * y + x]) - (double)(Gyy[image_w * y + x]);
            gxx_yy_2 = gxx_yy * gxx_yy;
            gxy_2 = (double)(Gxy[image_w * y + x]) * (double)(Gxy[image_w * y + x]);
            denom = sqrt(gxy_2 + gxx_yy_2+1.175494351e-38f); //+ numeric_limits<double>::epsilon();
            sin2theta[image_w * y + x] = Gxy[image_w * y + x] / denom;
            cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / denom;
            
            //double Imin = ((double)Gyy[image_w * y + x]+(double)Gxx[image_w * y + x])/2 - ((double)Gxx[image_w * y + x]-(double)Gyy[image_w * y + x])*cos2theta[image_w * y + x]/2 - (double)Gxy[image_w * y + x]*sin2theta[image_w * y + x]/2;
            //double Imax = (double)Gyy[image_w * y + x]+(double)Gxx[image_w * y + x] - Imin;
        
            //reliability[image_w * y + x] = (1 - Imin/(Imax+.001));
        }
    }
}

void cal_theata_reli(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* sin2theta, float* cos2theta, double* reliability)
{
    double denom;
    double gxx_yy;
    double gxx_yy_2;
    double gxy_2;
    unsigned int x, y;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            gxx_yy = (double)(Gxx[image_w * y + x]) - (double)(Gyy[image_w * y + x]);
            gxx_yy_2 = gxx_yy * gxx_yy;
            gxy_2 = (double)(Gxy[image_w * y + x]) * (double)(Gxy[image_w * y + x]);
            denom = sqrt(gxy_2 + gxx_yy_2); //+ numeric_limits<double>::epsilon();
            if(denom == 0){
                sin2theta[image_w * y + x] = 0;
                cos2theta[image_w * y + x] = 0;
            }
            else{
                sin2theta[image_w * y + x] = Gxy[image_w * y + x] / denom;
                cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / denom;
            }
            double Imin = ((double)Gyy[image_w * y + x]+(double)Gxx[image_w * y + x])/2 - ((double)Gxx[image_w * y + x]-(double)Gyy[image_w * y + x])*cos2theta[image_w * y + x]/2 - (double)Gxy[image_w * y + x]*sin2theta[image_w * y + x]/2;
            double Imax = (double)Gyy[image_w * y + x]+(double)Gxx[image_w * y + x] - Imin;
        
            reliability[image_w * y + x] = (1 - Imin/(Imax+.001));
            //printf("\n %f %f", cos2theta[image_w * y + x],sin2theta[image_w * y + x]);
        }
    }
}

void cal_theata_fp(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* sin2theta, float* cos2theta)
{
    double denom;
    double gxx_yy;
    double gxx_yy_2;
    double gxy_2;
    unsigned int x, y;

    int long long fp_denom;
    int long long fp_gxx_yy;
    int long long fp_gxx_yy_2;
    int long long fp_gxy_2;

    float sin_tmp,cos_tmp;


    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
#if 0
            gxx_yy = (double)(Gxx[image_w * y + x]) - (double)(Gyy[image_w * y + x]);
            gxx_yy_2 = gxx_yy * gxx_yy;
            gxy_2 = (double)(Gxy[image_w * y + x]) * (double)(Gxy[image_w * y + x]);
            denom = sqrt(gxy_2 + gxx_yy_2) ;//+ numeric_limits<double>::epsilon();
            sin2theta[image_w * y + x] = Gxy[image_w * y + x] / denom;
            cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / denom;
#endif
#if 1
            fp_gxx_yy = (int long long)(Gxx[image_w * y + x] - Gyy[image_w * y + x]);
            fp_gxx_yy_2 = fp_gxx_yy * fp_gxx_yy;
            fp_gxy_2 = (int long long)(Gxy[image_w * y + x] * Gxy[image_w * y + x]);
            fp_denom = sqrt(fp_gxy_2 + fp_gxx_yy_2) ;//+ numeric_limits<double>::epsilon();
            //if(fp_denom != 0)
            {
                sin2theta[image_w * y + x] = Gxy[image_w * y + x] / fp_denom;
                cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / fp_denom;
            }
#if 0
            printf("[%d][%d](%f,%f) (%f,%f)\n",y,x,
                   sin2theta[image_w * y + x],
                   cos2theta[image_w * y + x],
                   sin_tmp,
                   cos_tmp);
            getchar();
#endif
            //sin2theta[image_w * y + x] = sin_tmp;
            //cos2theta[image_w * y + x] = cos_tmp;
#endif
        }
    }
}

void cal_theata_fp_reli(unsigned int image_w, unsigned int image_h, float* Gxx, float* Gyy, float* Gxy, float* sin2theta, float* cos2theta, double* reliability)
{
    double denom;
    double gxx_yy;
    double gxx_yy_2;
    double gxy_2;
    unsigned int x, y;

    int long long fp_denom;
    int long long fp_gxx_yy;
    int long long fp_gxx_yy_2;
    int long long fp_gxy_2;

    float sin_tmp,cos_tmp;


    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
#if 0
            gxx_yy = (double)(Gxx[image_w * y + x]) - (double)(Gyy[image_w * y + x]);
            gxx_yy_2 = gxx_yy * gxx_yy;
            gxy_2 = (double)(Gxy[image_w * y + x]) * (double)(Gxy[image_w * y + x]);
            denom = sqrt(gxy_2 + gxx_yy_2) ;//+ numeric_limits<double>::epsilon();
            sin2theta[image_w * y + x] = Gxy[image_w * y + x] / denom;
            cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / denom;
#endif
#if 1
            fp_gxx_yy = (int long long)(Gxx[image_w * y + x] - Gyy[image_w * y + x]);
            fp_gxx_yy_2 = fp_gxx_yy * fp_gxx_yy;
            fp_gxy_2 = (int long long)(Gxy[image_w * y + x] * Gxy[image_w * y + x]);
            fp_denom = sqrt(fp_gxy_2 + fp_gxx_yy_2) ;//+ numeric_limits<double>::epsilon();
            if(fp_denom != 0)
            {
                sin2theta[image_w * y + x] = Gxy[image_w * y + x] / fp_denom;
                cos2theta[image_w * y + x] = (Gxx[image_w * y + x] - Gyy[image_w * y + x]) / fp_denom;
            }
            else{
                sin2theta[image_w * y + x] = 0;
                cos2theta[image_w * y + x] = 0;
            }
            
            double Imin = ((double)Gyy[image_w * y + x]+(double)Gxx[image_w * y + x])/2 - ((double)Gxx[image_w * y + x]-(double)Gyy[image_w * y + x])*cos2theta[image_w * y + x]/2 - (double)Gxy[image_w * y + x]*sin2theta[image_w * y + x]/2;
            double Imax = (double)Gyy[image_w * y + x]+(double)Gxx[image_w * y + x] - Imin;
        
            reliability[image_w * y + x] = (1 - Imin/(Imax+.001));
#if 0
            printf("[%d][%d](%f,%f) (%f,%f)\n",y,x,
                   sin2theta[image_w * y + x],
                   cos2theta[image_w * y + x],
                   sin_tmp,
                   cos_tmp);
            getchar();
#endif
            //sin2theta[image_w * y + x] = sin_tmp;
            //cos2theta[image_w * y + x] = cos_tmp;
#endif
        }
    }
}

void convolution_with_padding_mirror_with_2(unsigned int image_w, unsigned int image_h, float* sin2theta, float* cos2theta, float* pixel_ort, unsigned int size)
{
    unsigned i, j,x,y;
    unsigned int temp = size * size;
    int radius = (size - 1) / 2;
    float element;
    float element2;
    int im_x;
    int im_y;
    unsigned int tmp1,tmp2,tmp3;
    float tmp4;

    tmp1 = 2*image_w;
    tmp2 = 2*image_h;
    tmp4 = M_PI / 2.0;

    for (y = 0; y < image_h; y++)
    {
        for (x = 0; x < image_w; x++)
        {
            element = 0.0;
            element2 = 0.0;
            for (j = 0; j < size; j++)
            {
                for (i = 0; i < size; i++)
                {
                    im_x = x + i - radius;
                    im_y = y + j - radius;
                    //border mode reflect
                    if (im_x < 0)
                    {
                        im_x = -1-im_x;
                    }
                    else if (im_x >= image_w)
                    {
                        im_x = tmp1 - 1 - im_x;
                    }
                    if (im_y < 0)
                    {
                        im_y = -1-im_y;
                    }
                    else if (im_y >= image_h)
                    {
                        im_y = tmp2 - 1 - im_y;
                    }

                    tmp3 = image_w * im_y + im_x;
                    element += sin2theta[tmp3];
                    element2 += cos2theta[tmp3];
                }
            }

            pixel_ort[image_w * y + x] = tmp4 + atan2(element / (temp), element2 / (temp)) / 2.0;
        }
    }
}

typedef struct _Image_{
		int *data;
			unsigned int col;
				unsigned int row;
					unsigned int vzp;
						unsigned int hzp;
							unsigned int size;

}Image;
void __Mapping(void *buff,void *data,unsigned int size) {
	char *out=(char*)buff,*in=(char*)data;
	int i=size+1;
	for(;--i;++out,++in)
		*out=*in;
}
Image *__initImage(unsigned int col,unsigned int row,unsigned int hzp,unsigned int vzp) {
		Image *img=(Image*)malloc(sizeof(Image));
			img->col=col;
				img->row=row;
					img->vzp=vzp;
						img->hzp=hzp;
							img->size=(col+(hzp<<1))*(row+(vzp<<1));
								img->data=(int*)calloc(img->size,4);
									return img;

}
Image *__cloneImage(Image *img) {
		Image *out=__initImage(img->col,img->row,0,0);
			__Mapping(out->data,img->data,img->size<<2);
				return out;

}
float *__get1DGaussianKernel(int scale,float sigma) {
		int half=scale>>1,i=0;
			float *out=(float*)malloc(sizeof(float)*scale),sum=0.0f;
				for(;i<scale;sum+=out[i],i++)
							out[i]=expf(-(1.0f)*(((i-half)*(i-half))/(2.0f*sigma*sigma)));
					for(i=0;i<scale;i++)
								out[i]/=sum;
						return out;

}

void __freeImage(Image *img) {
		free(img->data);
			free(img);

}
Image __setImage(int *data,unsigned int col,unsigned int row){
	Image out;
			out.data=data;
				out.col=col;
					out.row=row;
						out.hzp=out.vzp=0;
							out.size=col*row;
								return out;

}
void __setBlockData(Image *output,Image *img,int x,int y) {
		int *data=img->data,*out=output->data+x+y*output->col;
			int col=img->col,jmp=output->col-col,i=img->row+1,j;
				for(;--i;out+=jmp)
							for(j=col+1;--j;++data,++out)
											*out=*data;

}
Image *__Mirror(Image *img,int left,int top,int right,int down) {
		Image *out=__initImage(img->col+left+right,img->row+top+down,0,0);
			__setBlockData(out,img,left,top);
				int col=out->col,row=out->row,jmp=col*2,i,j,*data,*in;
					for(data=out->data,in=data+col*(top*2-1),i=top+1,j;--i;in-=jmp)
								for(j=col+1;--j;++data,++in)
												*data=*in;
						for(data=out->data+out->size-1,in=data-col*(down*2-1),i=down+1,j;--i;in+=jmp)
									for(j=col+1;--j;--data,--in)
													*data=*in;
							jmp=row*col-1;
								for(data=out->data,in=data+(left*2-1),i=left+1,j;--i;data-=jmp,in-=jmp+2)
											for(j=row+1;--j;data+=col,in+=col)
															*data=*in;
									for(data=out->data+col-1,in=data-(right*2-1),i=right+1,j;--i;data-=jmp+2,in-=jmp)
												for(j=row+1;--j;data+=col,in+=col)
																*data=*in;
										return out;

}
void __Symmetric(Image *out,Image *img,int left,int top,int right,int down){
		out->col=img->col+left+right;
			out->row=img->row+top+down;
				out->size=out->col*out->row;
					__setBlockData(out,img,left,top);
						int col=out->col,row=out->row,jmp=col*2,i,j,*data,*in;
							for(data=out->data,in=data+col*(top*2-1),i=top+1,j;--i;in-=jmp)
										for(j=col+1;--j;++data,++in)
														*data=*in;
								for(data=out->data+out->size-1,in=data-col*(down*2-1),i=down+1,j;--i;in+=jmp)
											for(j=col+1;--j;--data,--in)
															*data=*in;
									jmp=row*col-1;
										for(data=out->data,in=data+(left*2-1),i=left+1,j;--i;data-=jmp,in-=jmp+2)
													for(j=row+1;--j;data+=col,in+=col)
																	*data=*in;
											for(data=out->data+col-1,in=data-(right*2-1),i=right+1,j;--i;data-=jmp+2,in-=jmp)
														for(j=row+1;--j;data+=col,in+=col)
																		*data=*in;

}
void Zero2(void *io,unsigned int size) {
		unsigned char* ptr=(unsigned char*)io;
			unsigned int i=(size&7)+1;
				for(;--i;++ptr)
							*ptr^=*ptr;
					size=(size>>3)+1;
						unsigned long long *z=(unsigned long long*)ptr;
							for(;--size;++z)
										*z^=*z;

}

void SymmetryConvolutionf(Image *img,Image *kernel) {
		float *ptr=(float*)kernel->data,*out=0,*in=0,w;
		int col=img->col,bcol,row=img->row,scale=kernel->col,jmp0=img->size-1,jmp1,i=0,j,k;
		Image *buf=__Mirror(img,scale>>1,0,scale>>1,0);
		Zero2(img->data,img->size<<2);
		for(bcol=buf->col;scale>i;++ptr,++i)
			for(out=(float*)img->data,in=(float*)buf->data+i,w=*ptr,j=row+1;--j;in+=scale-1)
				for(k=col+1;--k;++out,++in)
					*out+=*in*w;
		//printf("\n SymmetryConvolutionf 1 %d,%d,%d\n",buf->col,buf->row,buf->size);

        //__Symmetric(buf,img,0,scale>>1,0,scale>>1);
        __freeImage(buf);
        buf=__Mirror(img,0,scale>>1,0,scale>>1);
        //printf("\n SymmetryConvolutionf 2\n");

		Zero2(img->data,img->size<<2);

        //printf("\n SymmetryConvolutionf 3\n");

		for(bcol=buf->col,jmp1=row*bcol-1,out=(float*)img->data,ptr-=scale,i=0;scale>i;out=(float*)img->data,++ptr,++i)
			for(in=(float*)buf->data+i*bcol,w=*ptr,j=col+1;--j;out-=jmp0,in-=jmp1)
				for(k=row+1;--k;out+=col,in+=bcol)
					*out+=*in*w;
		//printf("\n SymmetryConvolutionf 4\n");

        
        __freeImage(buf);
        //printf("\n SymmetryConvolutionf 5\n");


}
void __setFloat(float *io,const float value,unsigned int size) {
	for(++size;--size;++io)
		*io=value;

}
void __Integral2Df(float *in,unsigned int col,unsigned int row) {
	unsigned int i=col;
	for(++in;--i;++in)
		*in+=*(in-1);
	float reg;
	for(;--row;)
		for(reg=*in,*in+=*(in-col),++in,i=col;--i;*in=reg+*(in-col),++in)
			reg+=*in;

}
#define __InvIntegral(ptr,w,h) (*(ptr)-*(ptr+w)-*(ptr+h)+*(ptr+w+h))
void getAngle(Image *angle,Image *imgsin,Image *imgcos,int kw){
	Image *_sin=__Mirror(imgsin,(kw>>1)+1,(kw>>1)+1,kw>>1,kw>>1),
		*_cos=__Mirror(imgcos,(kw>>1)+1,(kw>>1)+1,kw>>1,kw>>1);
	__Integral2Df((float*)_sin->data,_sin->col,_sin->row);
	__Integral2Df((float*)_cos->data,_cos->col,_cos->row);
	float *out=(float*)angle->data,*ptr0=(float*)_sin->data,*ptr1=(float*)_cos->data;
	int col=angle->col,in_col=_sin->col,kh=in_col*kw,i=angle->row+1,j;
	__setFloat(out,1.570796f,angle->size);
	for(i=angle->row+1;--i;ptr0+=kw,ptr1+=kw)
		for(j=col+1;--j;++out,++ptr0,++ptr1)
			*out+=atan2(__InvIntegral(ptr0,kw,kh),__InvIntegral(ptr1,kw,kh))*0.5f;
	__freeImage(_sin);
	__freeImage(_cos);
}
void __Mulf(float *out,float value,int size) {
	for(++size;--size;++out)
		*out*=value;

}


void RidgeOrient_V3(BYTE* image, int image_h, int image_w, int gradientsigma, int blocksigma, int orientsmoothsigma, int fft_flag, unsigned int resize, unsigned int fix_point,unsigned int turbo, float* pixel_ort)
{
    fft_data stfft_data;
    int rows = image_h;
    int cols = image_w;
    int sze;
    float* resize_image = NULL;
    int* fp_resize_image = NULL;
    unsigned int re_size;
    float fgradientsigma = gradientsigma;
    float fblocksigma = blocksigma;
    float forientsmoothsigma = orientsmoothsigma;
    float *fy = NULL;
    float *fx = NULL;
    int *fp_fy = NULL;
    int *fp_fx = NULL;
    int radius;
    float* gauss = NULL;
    int* fp_gauss = NULL;
    float* buffer1 = NULL;
    float* buffer2 = NULL;
    float* buffer3 = NULL;
    float* Gx = NULL;
    float* Gy = NULL;
    float* Gxy_ = NULL;
    int* fp_buffer1 = NULL;
    int* fp_buffer2 = NULL;
    int* fp_buffer3 = NULL;
    int* fp_Gx = NULL;
    int* fp_Gy = NULL;
    int* fp_Gxy_ = NULL;
    float* buffer4 = NULL;
    float* buffer5 = NULL;
    float* buffer6 = NULL;
    float* Gxx = NULL;
    float* Gyy = NULL;
    float* Gxy = NULL;
    int* fp_buffer4 = NULL;
    int* fp_buffer5 = NULL;
    int* fp_buffer6 = NULL;
    int* fp_Gxx = NULL;
    int* fp_Gyy = NULL;
    int* fp_Gxy = NULL;
    float* sin2theta = NULL;
    float* cos2theta = NULL;
    //double* reliability = NULL;
    float* pixel_ort_tmp = NULL; // for resize
    unsigned short* u8image = (unsigned short*)malloc(image_w*image_h*sizeof(unsigned short));

    for(int i=0;i<image_h;i++)
    {
        for(int j=0;j<image_w;j++)
        {
            u8image[i*image_w+j] = (unsigned short)image[i*image_w+j];
        }
    }

    if(resize == 1)
    {
        re_size = 100;// default is 100
        rows = cols = re_size;
        //rows = re_size_h;
        //cols = re_size_w;
        if(fix_point == 0)
        {
            resize_image = (float*)malloc(image_w*image_h*sizeof(float));
            bilinear_image(u8image,NULL,resize_image,NULL,image_w,re_size);
            //resize_bilinear_image(u8image,NULL,resize_image,NULL,image_w,re_size_w,re_size_h);
        }
        else
        {
            fp_resize_image = (int*)malloc(image_w*image_h*sizeof(int));
            bilinear_image_fp(u8image,fp_resize_image,NULL,image_w,re_size);
            //resize_bilinear_image_fp(u8image,fp_resize_image,NULL,image_w,re_size_w,re_size_h);
        }

        fgradientsigma = ((float)fgradientsigma)*((float)re_size)/((float)image_w);
        fblocksigma = ((float)fblocksigma)*((float)re_size)/((float)image_w);
        forientsmoothsigma = ((float)forientsmoothsigma)*((float)re_size)/((float)image_w);
    }

    sze = (int)(6 * fgradientsigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    radius = (sze - 1) / 2;

    gauss = (float*)malloc(sze*sze*sizeof(float));

    create_gaussian_kernel(gauss, sze, fgradientsigma);

    if(fix_point == 0)
    {
        fy = (float *)malloc(sze*sze*sizeof(float)); // y gradient
        fx = (float *)malloc(sze*sze*sizeof(float));
        create_graident_function(fy, gauss, sze,0);
        create_graident_function(fx, gauss, sze,1);
    }
    else
    {
        fp_fy = (int *)malloc(sze*sze*sizeof(int)); // y gradient
        fp_fx = (int *)malloc(sze*sze*sizeof(int));
        create_graident_function_fp(fp_fy, gauss, sze,0);
        create_graident_function_fp(fp_fx, gauss, sze,1);
    }

    free(gauss);

    if(fix_point == 0)
    {
        buffer1 = (float*)malloc(cols*rows*sizeof(float));
        buffer2 = (float*)malloc(cols*rows*sizeof(float));
        buffer3 = (float*)malloc(cols*rows*sizeof(float));
        //buffer7 = (double*)malloc(cols*rows*sizeof(double));
        Gx = buffer1;
        Gy = buffer2;
        Gxy_ = buffer3;
    }
    else
    {
        fp_buffer1 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer2 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer3 = (int*)malloc(cols*rows*sizeof(int));
        fp_Gx = fp_buffer1;
        fp_Gy = fp_buffer2;
        fp_Gxy_ = fp_buffer3;
    }

    if (fft_flag == 0 || fft_flag == 1)
    {
        if(resize == 0)
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero(u8image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                //todo
            }
        }
        else
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero_float(resize_image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                convolution_with_padding_zero_float_fp(fp_resize_image, cols, rows, fp_Gx, fp_Gy, fp_Gxy_, fp_fx,fp_fy,sze);
            }
        }
    }

    sze = (int)(6 * fblocksigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    if(fix_point == 0)
    {
        gauss = (float*)malloc(sze*sze * sizeof(float));
        create_gaussian_kernel(gauss, sze, fblocksigma);
    }
    else
    {
        fp_gauss = (int*)malloc(sze*sze * sizeof(int));
        create_gaussian_kernel_fp(fp_gauss, sze, fblocksigma);
    }

    buffer4 = (float*)malloc(cols*rows*sizeof(float));
    buffer5 = (float*)malloc(cols*rows*sizeof(float));
    buffer6 = (float*)malloc(cols*rows*sizeof(float));

    Gxx = buffer4;
    Gyy = buffer5;
    Gxy = buffer6;

    if (fft_flag == 0)
    {
        if(fix_point == 0)
        {
            if(turbo == 1)
            {
                float *w=__get1DGaussianKernel(21,3.5);
                Image kernel=__setImage((int*)w,21,1),
                imgxx=__setImage((int*)buffer1,cols,rows),imgyy=__setImage((int*)buffer2,cols,rows),imgxy=__setImage((int*)buffer3,cols,rows);
                SymmetryConvolutionf(&imgxx,&kernel);
                SymmetryConvolutionf(&imgyy,&kernel);
                SymmetryConvolutionf(&imgxy,&kernel);
                __Mulf((float*)imgxy.data,2.0f,imgxy.size);
                free(w);
            }
			else
			{
			    convolution_with_padding_mirror_with_3(cols, rows, Gxx, Gyy, Gxy, Gx, Gy, Gxy_, gauss, sze); 
			}
        }
        else
        {
            convolution_with_padding_mirror_with_3_fp(cols, rows, Gxx, Gyy, Gxy, fp_Gx, fp_Gy, fp_Gxy_, fp_gauss, sze);
        }
    }
    else if (fft_flag == 1)
    {
        if(fix_point == 0)
        {
            unsigned int temp;

            stfft_data.out1 = Gxx;
            stfft_data.out2 = Gyy;
            stfft_data.out3 = Gxy;

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = stfft_data.raw3_reference_size = cols;

            stfft_data.raw1_in = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw1_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw1_reference = Gx;

            stfft_data.raw2_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw2_out = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw2_reference = Gy;

            stfft_data.raw3_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_reference = Gxy_;

            stfft_data.kernel_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_reference = gauss;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_3(&stfft_data);
        }
        else
        {
            //todo
        }
    }

    if(fix_point == 0)
    {
        free(gauss);
    }
    else
    {
        free(fp_gauss);
    }


    if(fix_point == 0)
    {
        if(turbo == 0)
        {
            sin2theta = buffer1;
            cos2theta = buffer2;
        }
        pixel_ort_tmp = buffer3;
        //reliability = buffer7;
    }
    else
    {
        sin2theta = (float*)malloc(cols*rows*sizeof(float));
        cos2theta = (float*)malloc(cols*rows*sizeof(float));
        pixel_ort_tmp = (float*)malloc(cols*rows*sizeof(float));
    }

	Image *__sin=0,*__cos=0;

    if(fix_point == 0)
    {
        if(turbo == 1)
        {
		    __sin=__initImage(cols,rows,0,0);
		    __cos=__initImage(cols,rows,0,0);

		    cal_theata(cols,rows,buffer1,buffer2,buffer3,(float*)__sin->data,(float*)__cos->data);
        }
		else
		{
            cal_theata(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta);
        }
    }
    else
    {
        cal_theata_fp(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta);
    }

    if(forientsmoothsigma > 0)
    {
        sze = (int)(6 * forientsmoothsigma);
        if (sze % 2 == 0)
        {
            sze = sze + 1;
        }

        if (fft_flag == 0)
        {
            if(resize == 0)
            {
                convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort, sze);
            }
            else
            {
                if(turbo == 1)
                {
					Image __out=__setImage((int*)pixel_ort_tmp,cols,rows);
	                getAngle(&__out,__sin,__cos,sze);
					__freeImage(__sin);
					__freeImage(__cos);
                }
				else
				{
                    convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort_tmp, sze);
				}
            }
        }
        else if (fft_flag == 1)
        {
            unsigned int temp;
            temp = stfft_data.fft_size*stfft_data.fft_size*sizeof(complex);

            memset(stfft_data.raw1_in,0,temp);
            memset(stfft_data.raw1_out,0,temp);
            memset(stfft_data.raw2_in,0,temp);
            memset(stfft_data.raw2_out,0,temp);
            memset(stfft_data.kernel_in,0,temp);
            memset(stfft_data.kernel_out,0,temp);

            if(resize == 0)
            {
                stfft_data.out1 = pixel_ort;
            }
            else
            {
                stfft_data.out1 = pixel_ort_tmp;
            }

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = cols;

            stfft_data.raw1_reference = sin2theta;

            stfft_data.raw2_reference = cos2theta;

            stfft_data.kernel_reference = NULL;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_2(&stfft_data);

            if(fix_point == 0)
            {
                free(stfft_data.raw1_in);
                free(stfft_data.raw1_out);
                free(stfft_data.raw2_in);
                free(stfft_data.raw2_out);
                free(stfft_data.raw3_in);
                free(stfft_data.raw3_out);
                free(stfft_data.kernel_in);
                free(stfft_data.kernel_out);
            }
        }
    }

    if(resize == 1)
    {
        float* out_tmp = (float*)malloc((image_w+1)*(image_w+1)*sizeof(float));
        bilinear_image(NULL,pixel_ort_tmp,pixel_ort,out_tmp,re_size,image_w);
        if(fix_point == 0)
        {
            free(resize_image);
        }
        else
        {
            free(fp_resize_image);
        }
        free(out_tmp);
    }

    if(fix_point == 0)
    {
        free(fx);
        free(fy);
    }
    else
    {
        free(fp_fx);
        free(fp_fy);

        free(fp_buffer1);
        free(fp_buffer2);
        free(fp_buffer3);
    }

    free(u8image);
    free(sin2theta);
    free(cos2theta);
    free(pixel_ort_tmp);
    free(buffer4);
    free(buffer5);
    free(buffer6);
    //free(buffer7)
}

void RidgeOrient_V3_reli(BYTE* image, int image_h, int image_w, int gradientsigma, int blocksigma, int orientsmoothsigma, int fft_flag, unsigned int resize, unsigned int fix_point,unsigned int turbo, float* pixel_ort, double* pixel_ort_r)
{
    fft_data stfft_data;
    int rows = image_h;
    int cols = image_w;
    int sze;
    float* resize_image = NULL;
    int* fp_resize_image = NULL;
    unsigned int re_size;
    float fgradientsigma = gradientsigma;
    float fblocksigma = blocksigma;
    float forientsmoothsigma = orientsmoothsigma;
    float *fy = NULL;
    float *fx = NULL;
    int *fp_fy = NULL;
    int *fp_fx = NULL;
    int radius;
    float* gauss = NULL;
    int* fp_gauss = NULL;
    float* buffer1 = NULL;
    float* buffer2 = NULL;
    float* buffer3 = NULL;
    float* Gx = NULL;
    float* Gy = NULL;
    float* Gxy_ = NULL;
    int* fp_buffer1 = NULL;
    int* fp_buffer2 = NULL;
    int* fp_buffer3 = NULL;
    int* fp_Gx = NULL;
    int* fp_Gy = NULL;
    int* fp_Gxy_ = NULL;
    float* buffer4 = NULL;
    float* buffer5 = NULL;
    float* buffer6 = NULL;
    float* Gxx = NULL;
    float* Gyy = NULL;
    float* Gxy = NULL;
    int* fp_buffer4 = NULL;
    int* fp_buffer5 = NULL;
    int* fp_buffer6 = NULL;
    int* fp_Gxx = NULL;
    int* fp_Gyy = NULL;
    int* fp_Gxy = NULL;
    float* sin2theta = NULL;
    float* cos2theta = NULL;
    //double* reliability = NULL;
    float* pixel_ort_tmp = NULL; // for resize
    unsigned short* u8image = (unsigned short*)malloc(image_w*image_h*sizeof(unsigned short));

    for(int i=0;i<image_h;i++)
    {
        for(int j=0;j<image_w;j++)
        {
            u8image[i*image_w+j] = (unsigned short)image[i*image_w+j];
        }
    }

    if(resize == 1)
    {
        re_size = 100;// default is 100
        rows = cols = re_size;
        if(fix_point == 0)
        {
            resize_image = (float*)malloc(image_w*image_h*sizeof(float));
            bilinear_image(u8image,NULL,resize_image,NULL,image_w,re_size);
        }
        else
        {
            fp_resize_image = (int*)malloc(image_w*image_h*sizeof(int));
            bilinear_image_fp(u8image,fp_resize_image,NULL,image_w,re_size);
        }

        fgradientsigma = ((float)fgradientsigma)*((float)re_size)/((float)image_w);
        fblocksigma = ((float)fblocksigma)*((float)re_size)/((float)image_w);
        forientsmoothsigma = ((float)forientsmoothsigma)*((float)re_size)/((float)image_w);
    }

    sze = (int)(6 * fgradientsigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    radius = (sze - 1) / 2;

    gauss = (float*)malloc(sze*sze*sizeof(float));

    create_gaussian_kernel(gauss, sze, fgradientsigma);

    if(fix_point == 0)
    {
        fy = (float *)malloc(sze*sze*sizeof(float)); // y gradient
        fx = (float *)malloc(sze*sze*sizeof(float));
        create_graident_function(fy, gauss, sze,0);
        create_graident_function(fx, gauss, sze,1);
    }
    else
    {
        fp_fy = (int *)malloc(sze*sze*sizeof(int)); // y gradient
        fp_fx = (int *)malloc(sze*sze*sizeof(int));
        create_graident_function_fp(fp_fy, gauss, sze,0);
        create_graident_function_fp(fp_fx, gauss, sze,1);
    }

    free(gauss);

    if(fix_point == 0)
    {
        buffer1 = (float*)malloc(cols*rows*sizeof(float));
        buffer2 = (float*)malloc(cols*rows*sizeof(float));
        buffer3 = (float*)malloc(cols*rows*sizeof(float));
        //buffer7 = (double*)malloc(cols*rows*sizeof(double));
        Gx = buffer1;
        Gy = buffer2;
        Gxy_ = buffer3;
    }
    else
    {
        fp_buffer1 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer2 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer3 = (int*)malloc(cols*rows*sizeof(int));
        fp_Gx = fp_buffer1;
        fp_Gy = fp_buffer2;
        fp_Gxy_ = fp_buffer3;
    }

    if (fft_flag == 0 || fft_flag == 1)
    {
        if(resize == 0)
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero(u8image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                //todo
            }
        }
        else
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero_float(resize_image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                convolution_with_padding_zero_float_fp(fp_resize_image, cols, rows, fp_Gx, fp_Gy, fp_Gxy_, fp_fx,fp_fy,sze);
            }
        }
    }

    sze = (int)(6 * fblocksigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    if(fix_point == 0)
    {
        gauss = (float*)malloc(sze*sze * sizeof(float));
        create_gaussian_kernel(gauss, sze, fblocksigma);
    }
    else
    {
        fp_gauss = (int*)malloc(sze*sze * sizeof(int));
        create_gaussian_kernel_fp(fp_gauss, sze, fblocksigma);
    }

    buffer4 = (float*)malloc(cols*rows*sizeof(float));
    buffer5 = (float*)malloc(cols*rows*sizeof(float));
    buffer6 = (float*)malloc(cols*rows*sizeof(float));

    Gxx = buffer4;
    Gyy = buffer5;
    Gxy = buffer6;

    if (fft_flag == 0)
    {
        if(fix_point == 0)
        {
            if(turbo == 1)
            {
                float *w=__get1DGaussianKernel(21,3.5);
                Image kernel=__setImage((int*)w,21,1),
                imgxx=__setImage((int*)buffer1,cols,rows),imgyy=__setImage((int*)buffer2,cols,rows),imgxy=__setImage((int*)buffer3,cols,rows);
                SymmetryConvolutionf(&imgxx,&kernel);
                SymmetryConvolutionf(&imgyy,&kernel);
                SymmetryConvolutionf(&imgxy,&kernel);
                __Mulf((float*)imgxy.data,2.0f,imgxy.size);
                free(w);
            }
			else
			{
			    convolution_with_padding_mirror_with_3(cols, rows, Gxx, Gyy, Gxy, Gx, Gy, Gxy_, gauss, sze); 
			}
        }
        else
        {
            convolution_with_padding_mirror_with_3_fp(cols, rows, Gxx, Gyy, Gxy, fp_Gx, fp_Gy, fp_Gxy_, fp_gauss, sze);
        }
    }
    else if (fft_flag == 1)
    {
        if(fix_point == 0)
        {
            unsigned int temp;

            stfft_data.out1 = Gxx;
            stfft_data.out2 = Gyy;
            stfft_data.out3 = Gxy;

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = stfft_data.raw3_reference_size = cols;

            stfft_data.raw1_in = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw1_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw1_reference = Gx;

            stfft_data.raw2_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw2_out = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw2_reference = Gy;

            stfft_data.raw3_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_reference = Gxy_;

            stfft_data.kernel_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_reference = gauss;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_3(&stfft_data);
        }
        else
        {
            //todo
        }
    }

    if(fix_point == 0)
    {
        free(gauss);
    }
    else
    {
        free(fp_gauss);
    }


    if(fix_point == 0)
    {
        if(turbo == 0)
        {
            sin2theta = buffer1;
            cos2theta = buffer2;
        }
        pixel_ort_tmp = buffer3;
        //reliability = buffer7;
    }
    else
    {
        sin2theta = (float*)malloc(cols*rows*sizeof(float));
        cos2theta = (float*)malloc(cols*rows*sizeof(float));
        pixel_ort_tmp = (float*)malloc(cols*rows*sizeof(float));
    }

	Image *__sin=0,*__cos=0;

    if(fix_point == 0)
    {
        if(turbo == 1)
        {
		    __sin=__initImage(cols,rows,0,0);
		    __cos=__initImage(cols,rows,0,0);

		    cal_theata_reli(cols,rows,buffer1,buffer2,buffer3,(float*)__sin->data,(float*)__cos->data, pixel_ort_r);
        }
		else
		{
            cal_theata_reli(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta, pixel_ort_r);
        }
    }
    else
    {
        cal_theata_fp_reli(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta, pixel_ort_r);
    }

    if(forientsmoothsigma > 0)
    {
        sze = (int)(6 * forientsmoothsigma);
        if (sze % 2 == 0)
        {
            sze = sze + 1;
        }

        if (fft_flag == 0)
        {
            if(resize == 0)
            {
                convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort, sze);
            }
            else
            {
                if(turbo == 1)
                {
					Image __out=__setImage((int*)pixel_ort_tmp,cols,rows);
	                getAngle(&__out,__sin,__cos,sze);
					__freeImage(__sin);
					__freeImage(__cos);
                }
				else
				{
                    convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort_tmp, sze);
				}
            }
        }
        else if (fft_flag == 1)
        {
            unsigned int temp;
            temp = stfft_data.fft_size*stfft_data.fft_size*sizeof(complex);

            memset(stfft_data.raw1_in,0,temp);
            memset(stfft_data.raw1_out,0,temp);
            memset(stfft_data.raw2_in,0,temp);
            memset(stfft_data.raw2_out,0,temp);
            memset(stfft_data.kernel_in,0,temp);
            memset(stfft_data.kernel_out,0,temp);

            if(resize == 0)
            {
                stfft_data.out1 = pixel_ort;
            }
            else
            {
                stfft_data.out1 = pixel_ort_tmp;
            }

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = cols;

            stfft_data.raw1_reference = sin2theta;

            stfft_data.raw2_reference = cos2theta;

            stfft_data.kernel_reference = NULL;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_2(&stfft_data);

            if(fix_point == 0)
            {
                free(stfft_data.raw1_in);
                free(stfft_data.raw1_out);
                free(stfft_data.raw2_in);
                free(stfft_data.raw2_out);
                free(stfft_data.raw3_in);
                free(stfft_data.raw3_out);
                free(stfft_data.kernel_in);
                free(stfft_data.kernel_out);
            }
        }
    }

    if(resize == 1)
    {
        float* out_tmp = (float*)malloc((image_w+1)*(image_w+1)*sizeof(float));
        bilinear_image(NULL,pixel_ort_tmp,pixel_ort,out_tmp,re_size,image_w);
        if(fix_point == 0)
        {
            free(resize_image);
        }
        else
        {
            free(fp_resize_image);
        }
        free(out_tmp);
    }

    if(fix_point == 0)
    {
        free(fx);
        free(fy);
    }
    else
    {
        free(fp_fx);
        free(fp_fy);

        free(fp_buffer1);
        free(fp_buffer2);
        free(fp_buffer3);
    }

    free(u8image);
    free(sin2theta);
    free(cos2theta);
    free(pixel_ort_tmp);
    free(buffer4);
    free(buffer5);
    free(buffer6);
    //free(buffer7)
}

void RidgeOrient_V4(BYTE* image, int image_h, int image_w, int gradientsigma, int blocksigma, int orientsmoothsigma, int fft_flag, unsigned int resize, unsigned int fix_point,unsigned int turbo, float* pixel_ort)
{
    fft_data stfft_data;
    int rows = image_h;
    int cols = image_w;
    int sze;
    float* resize_image = NULL;
    int* fp_resize_image = NULL;
    unsigned int re_size;
    float fgradientsigma = gradientsigma;
    float fblocksigma = blocksigma;
    float forientsmoothsigma = orientsmoothsigma;
    float *fy = NULL;
    float *fx = NULL;
    int *fp_fy = NULL;
    int *fp_fx = NULL;
    int radius;
    float* gauss = NULL;
    int* fp_gauss = NULL;
    float* buffer1 = NULL;
    float* buffer2 = NULL;
    float* buffer3 = NULL;
    float* Gx = NULL;
    float* Gy = NULL;
    float* Gxy_ = NULL;
    int* fp_buffer1 = NULL;
    int* fp_buffer2 = NULL;
    int* fp_buffer3 = NULL;
    int* fp_Gx = NULL;
    int* fp_Gy = NULL;
    int* fp_Gxy_ = NULL;
    float* buffer4 = NULL;
    float* buffer5 = NULL;
    float* buffer6 = NULL;
    float* Gxx = NULL;
    float* Gyy = NULL;
    float* Gxy = NULL;
    int* fp_buffer4 = NULL;
    int* fp_buffer5 = NULL;
    int* fp_buffer6 = NULL;
    int* fp_Gxx = NULL;
    int* fp_Gyy = NULL;
    int* fp_Gxy = NULL;
    float* sin2theta = NULL;
    float* cos2theta = NULL;
    //double* reliability = NULL;
    float* pixel_ort_tmp = NULL; // for resize
    unsigned short* u8image = (unsigned short*)malloc(image_w*image_h*sizeof(unsigned short));

    for(int i=0;i<image_h;i++)
    {
        for(int j=0;j<image_w;j++)
        {
            u8image[i*image_w+j] = (unsigned short)image[i*image_w+j];
        }
    }

    /*if(resize == 1)
    {
        //re_size = ;// default is 100
        //rows = cols = re_size;
        //printf("\n fukking 1\n");
        rows = image_h/2;
        cols = image_w/2;
        if(fix_point == 0)
        {
            resize_image = (float*)malloc(image_w*image_h*sizeof(float));
            //bilinear_image(u8image,NULL,resize_image,NULL,image_w,re_size);
            resize_bilinear_image(u8image,NULL,resize_image,NULL,image_w,cols,rows);
        }
        else
        {
            fp_resize_image = (int*)malloc(image_w*image_h*sizeof(int));
            //bilinear_image_fp(u8image,fp_resize_image,NULL,image_w,re_size);
            resize_bilinear_image_fp(u8image,fp_resize_image,NULL,image_w,cols,rows);
        }
        //printf("\n fukking 2\n");

        //fgradientsigma = ((float)fgradientsigma)*((float)re_size)/((float)image_w);
        //fblocksigma = ((float)fblocksigma)*((float)re_size)/((float)image_w);
        //forientsmoothsigma = ((float)forientsmoothsigma)*((float)re_size)/((float)image_w);
        fgradientsigma = ((float)fgradientsigma)*0.5;
        fblocksigma = ((float)fblocksigma)*0.5;
        forientsmoothsigma = ((float)forientsmoothsigma)*0.5;
    }*/

    sze = (int)(6 * fgradientsigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    radius = (sze - 1) / 2;

    gauss = (float*)malloc(sze*sze*sizeof(float));

    create_gaussian_kernel(gauss, sze, fgradientsigma);

    if(fix_point == 0)
    {
        fy = (float *)malloc(sze*sze*sizeof(float)); // y gradient
        fx = (float *)malloc(sze*sze*sizeof(float));
        create_graident_function(fy, gauss, sze,0);
        create_graident_function(fx, gauss, sze,1);
    }
    else
    {
        fp_fy = (int *)malloc(sze*sze*sizeof(int)); // y gradient
        fp_fx = (int *)malloc(sze*sze*sizeof(int));
        create_graident_function_fp(fp_fy, gauss, sze,0);
        create_graident_function_fp(fp_fx, gauss, sze,1);
    }

    free(gauss);

    if(fix_point == 0)
    {
        buffer1 = (float*)malloc(cols*rows*sizeof(float));
        buffer2 = (float*)malloc(cols*rows*sizeof(float));
        buffer3 = (float*)malloc(cols*rows*sizeof(float));
        //buffer7 = (double*)malloc(cols*rows*sizeof(double));
        Gx = buffer1;
        Gy = buffer2;
        Gxy_ = buffer3;
    }
    else
    {
        fp_buffer1 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer2 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer3 = (int*)malloc(cols*rows*sizeof(int));
        fp_Gx = fp_buffer1;
        fp_Gy = fp_buffer2;
        fp_Gxy_ = fp_buffer3;
    }

    //printf("\n fukking 3\n");

    if (fft_flag == 0 || fft_flag == 1)
    {
        if(resize == 0)
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero(u8image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                //todo
            }
        }
        else
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero_float(resize_image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                convolution_with_padding_zero_float_fp(fp_resize_image, cols, rows, fp_Gx, fp_Gy, fp_Gxy_, fp_fx,fp_fy,sze);
            }
        }
    }

    //printf("\n fukking 4\n");

    sze = (int)(6 * fblocksigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    if(fix_point == 0)
    {
        gauss = (float*)malloc(sze*sze * sizeof(float));
        create_gaussian_kernel(gauss, sze, fblocksigma);
    }
    else
    {
        fp_gauss = (int*)malloc(sze*sze * sizeof(int));
        create_gaussian_kernel_fp(fp_gauss, sze, fblocksigma);
    }

    buffer4 = (float*)malloc(cols*rows*sizeof(float));
    buffer5 = (float*)malloc(cols*rows*sizeof(float));
    buffer6 = (float*)malloc(cols*rows*sizeof(float));

    Gxx = buffer4;
    Gyy = buffer5;
    Gxy = buffer6;

    //printf("\n fukking 5\n");

    if (fft_flag == 0)
    {
        if(fix_point == 0)
        {
            if(turbo == 1)
            {
                float *w=__get1DGaussianKernel(21,3.5);
                Image kernel=__setImage((int*)w,21,1),
                imgxx=__setImage((int*)buffer1,cols,rows),imgyy=__setImage((int*)buffer2,cols,rows),imgxy=__setImage((int*)buffer3,cols,rows);
                
                //printf("\n tim 0 %d %d\n", cols, rows);

                SymmetryConvolutionf(&imgxx,&kernel);

                //printf("\n tim 1\n");

                SymmetryConvolutionf(&imgyy,&kernel);
                
                //printf("\n tim 2\n");

                SymmetryConvolutionf(&imgxy,&kernel);
                
                //printf("\n tim 3\n");

                __Mulf((float*)imgxy.data,2.0f,imgxy.size);
                
                //printf("\n tim 4\n");

                free(w);
            }
            else
            {
                convolution_with_padding_mirror_with_3(cols, rows, Gxx, Gyy, Gxy, Gx, Gy, Gxy_, gauss, sze); 
            }
        }
        else
        {
            convolution_with_padding_mirror_with_3_fp(cols, rows, Gxx, Gyy, Gxy, fp_Gx, fp_Gy, fp_Gxy_, fp_gauss, sze);
        }
    }
    else if (fft_flag == 1)
    {
        if(fix_point == 0)
        {
            unsigned int temp;

            stfft_data.out1 = Gxx;
            stfft_data.out2 = Gyy;
            stfft_data.out3 = Gxy;

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = stfft_data.raw3_reference_size = cols;

            stfft_data.raw1_in = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw1_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw1_reference = Gx;

            stfft_data.raw2_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw2_out = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw2_reference = Gy;

            stfft_data.raw3_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_reference = Gxy_;

            stfft_data.kernel_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_reference = gauss;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_3(&stfft_data);
        }
        else
        {
            //todo
        }
    }

    //printf("\n fukking 6\n");

    if(fix_point == 0)
    {
        free(gauss);
    }
    else
    {
        free(fp_gauss);
    }


    if(fix_point == 0)
    {
        if(turbo == 0)
        {
            sin2theta = buffer1;
            cos2theta = buffer2;
        }
        pixel_ort_tmp = buffer3;
        //reliability = buffer7;
    }
    else
    {
        sin2theta = (float*)malloc(cols*rows*sizeof(float));
        cos2theta = (float*)malloc(cols*rows*sizeof(float));
        pixel_ort_tmp = (float*)malloc(cols*rows*sizeof(float));
    }

    Image *__sin=0,*__cos=0;

    if(fix_point == 0)
    {
        if(turbo == 1)
        {
            __sin=__initImage(cols,rows,0,0);
            __cos=__initImage(cols,rows,0,0);

            cal_theata(cols,rows,buffer1,buffer2,buffer3,(float*)__sin->data,(float*)__cos->data);
        }
        else
        {
            cal_theata(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta);
        }
    }
    else
    {
        cal_theata_fp(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta);
    }

    if(forientsmoothsigma > 0)
    {
        sze = (int)(6 * forientsmoothsigma);
        if (sze % 2 == 0)
        {
            sze = sze + 1;
        }

        if (fft_flag == 0)
        {
            if(resize == 0)
            {
                if(turbo == 1)
                {
                    Image __out=__setImage((int*)pixel_ort,cols,rows);
                    getAngle(&__out,__sin,__cos,sze);
                    __freeImage(__sin);
                    __freeImage(__cos);
                }
                else{
                    convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort, sze);
                }
            }
            else
            {
                if(turbo == 1)
                {
                    Image __out=__setImage((int*)pixel_ort_tmp,cols,rows);
                    getAngle(&__out,__sin,__cos,sze);
                    __freeImage(__sin);
                    __freeImage(__cos);
                }
                else
                {
                    convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort_tmp, sze);
                }
            }
        }
        else if (fft_flag == 1)
        {
            unsigned int temp;
            temp = stfft_data.fft_size*stfft_data.fft_size*sizeof(complex);

            memset(stfft_data.raw1_in,0,temp);
            memset(stfft_data.raw1_out,0,temp);
            memset(stfft_data.raw2_in,0,temp);
            memset(stfft_data.raw2_out,0,temp);
            memset(stfft_data.kernel_in,0,temp);
            memset(stfft_data.kernel_out,0,temp);

            if(resize == 0)
            {
                stfft_data.out1 = pixel_ort;
            }
            else
            {
                stfft_data.out1 = pixel_ort_tmp;
            }

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = cols;

            stfft_data.raw1_reference = sin2theta;

            stfft_data.raw2_reference = cos2theta;

            stfft_data.kernel_reference = NULL;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_2(&stfft_data);

            if(fix_point == 0)
            {
                free(stfft_data.raw1_in);
                free(stfft_data.raw1_out);
                free(stfft_data.raw2_in);
                free(stfft_data.raw2_out);
                free(stfft_data.raw3_in);
                free(stfft_data.raw3_out);
                free(stfft_data.kernel_in);
                free(stfft_data.kernel_out);
            }
        }
    }

    /*if(resize == 1)
    {
        float* out_tmp = (float*)calloc((image_w+1)*(image_w+1),sizeof(float));
        //bilinear_image(NULL,pixel_ort_tmp,pixel_ort,out_tmp,re_size,image_w);
        //printf("\n fukkkkkkkkkkkkkkkkkkkkk \n");
        resize_back_bilinear_image(NULL,pixel_ort_tmp,pixel_ort,out_tmp,cols,rows,image_w,image_h);
        //printf("\n debug \n");
        if(fix_point == 0)
        {
            free(resize_image);
        }
        else
        {
            free(fp_resize_image);
        }
        free(out_tmp);
    }*/

    if(fix_point == 0)
    {
        free(fx);
        free(fy);
    }
    else
    {
        free(fp_fx);
        free(fp_fy);

        free(fp_buffer1);
        free(fp_buffer2);
        free(fp_buffer3);
    }

    free(u8image);
    free(sin2theta);
    free(cos2theta);
    free(pixel_ort_tmp);
    free(buffer4);
    free(buffer5);
    free(buffer6);
    //free(buffer7)
}

/*void RidgeOrient_V4_reli(BYTE* image, int image_h, int image_w, int gradientsigma, int blocksigma, int orientsmoothsigma, int fft_flag, unsigned int resize, unsigned int fix_point,unsigned int turbo, float* pixel_ort, double* pixel_ort_r)
{
    fft_data stfft_data;
    int rows = image_h;
    int cols = image_w;
    int sze;
    float* resize_image = NULL;
    int* fp_resize_image = NULL;
    unsigned int re_size;
    float fgradientsigma = gradientsigma;
    float fblocksigma = blocksigma;
    float forientsmoothsigma = orientsmoothsigma;
    float *fy = NULL;
    float *fx = NULL;
    int *fp_fy = NULL;
    int *fp_fx = NULL;
    int radius;
    float* gauss = NULL;
    int* fp_gauss = NULL;
    float* buffer1 = NULL;
    float* buffer2 = NULL;
    float* buffer3 = NULL;
    float* Gx = NULL;
    float* Gy = NULL;
    float* Gxy_ = NULL;
    int* fp_buffer1 = NULL;
    int* fp_buffer2 = NULL;
    int* fp_buffer3 = NULL;
    int* fp_Gx = NULL;
    int* fp_Gy = NULL;
    int* fp_Gxy_ = NULL;
    float* buffer4 = NULL;
    float* buffer5 = NULL;
    float* buffer6 = NULL;
    float* Gxx = NULL;
    float* Gyy = NULL;
    float* Gxy = NULL;
    int* fp_buffer4 = NULL;
    int* fp_buffer5 = NULL;
    int* fp_buffer6 = NULL;
    int* fp_Gxx = NULL;
    int* fp_Gyy = NULL;
    int* fp_Gxy = NULL;
    float* sin2theta = NULL;
    float* cos2theta = NULL;
    //double* reliability = NULL;
    float* pixel_ort_tmp = NULL; // for resize
    unsigned short* u8image = (unsigned short*)malloc(image_w*image_h*sizeof(unsigned short));

    for(int i=0;i<image_h;i++)
    {
        for(int j=0;j<image_w;j++)
        {
            u8image[i*image_w+j] = (unsigned short)image[i*image_w+j];
        }
    }

    if(resize == 1)
    {
        //re_size = ;// default is 100
        //rows = cols = re_size;
        //printf("\n fukking 1\n");
        rows = image_h/2;
        cols = image_w/2;
        if(fix_point == 0)
        {
            resize_image = (float*)malloc(image_w*image_h*sizeof(float));
            //bilinear_image(u8image,NULL,resize_image,NULL,image_w,re_size);
            resize_bilinear_image(u8image,NULL,resize_image,NULL,image_w,cols,rows);
        }
        else
        {
            fp_resize_image = (int*)malloc(image_w*image_h*sizeof(int));
            //bilinear_image_fp(u8image,fp_resize_image,NULL,image_w,re_size);
            resize_bilinear_image_fp(u8image,fp_resize_image,NULL,image_w,cols,rows);
        }
        //printf("\n fukking 2\n");

        //fgradientsigma = ((float)fgradientsigma)*((float)re_size)/((float)image_w);
        //fblocksigma = ((float)fblocksigma)*((float)re_size)/((float)image_w);
        //forientsmoothsigma = ((float)forientsmoothsigma)*((float)re_size)/((float)image_w);
        fgradientsigma = ((float)fgradientsigma)*0.5;
        fblocksigma = ((float)fblocksigma)*0.5;
        forientsmoothsigma = ((float)forientsmoothsigma)*0.5;
    }

    sze = (int)(6 * fgradientsigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    radius = (sze - 1) / 2;

    gauss = (float*)malloc(sze*sze*sizeof(float));

    create_gaussian_kernel(gauss, sze, fgradientsigma);

    if(fix_point == 0)
    {
        fy = (float *)malloc(sze*sze*sizeof(float)); // y gradient
        fx = (float *)malloc(sze*sze*sizeof(float));
        create_graident_function(fy, gauss, sze,0);
        create_graident_function(fx, gauss, sze,1);
    }
    else
    {
        fp_fy = (int *)malloc(sze*sze*sizeof(int)); // y gradient
        fp_fx = (int *)malloc(sze*sze*sizeof(int));
        create_graident_function_fp(fp_fy, gauss, sze,0);
        create_graident_function_fp(fp_fx, gauss, sze,1);
    }

    free(gauss);

    if(fix_point == 0)
    {
        buffer1 = (float*)malloc(cols*rows*sizeof(float));
        buffer2 = (float*)malloc(cols*rows*sizeof(float));
        buffer3 = (float*)malloc(cols*rows*sizeof(float));
        //buffer7 = (double*)malloc(cols*rows*sizeof(double));
        Gx = buffer1;
        Gy = buffer2;
        Gxy_ = buffer3;
    }
    else
    {
        fp_buffer1 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer2 = (int*)malloc(cols*rows*sizeof(int));
        fp_buffer3 = (int*)malloc(cols*rows*sizeof(int));
        fp_Gx = fp_buffer1;
        fp_Gy = fp_buffer2;
        fp_Gxy_ = fp_buffer3;
    }

    //printf("\n fukking 3\n");

    if (fft_flag == 0 || fft_flag == 1)
    {
        if(resize == 0)
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero(u8image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                //todo
            }
        }
        else
        {
            if(fix_point == 0)
            {
                convolution_with_padding_zero_float(resize_image, cols, rows, Gx, Gy, Gxy_, fx,fy,sze);
            }
            else
            {
                convolution_with_padding_zero_float_fp(fp_resize_image, cols, rows, fp_Gx, fp_Gy, fp_Gxy_, fp_fx,fp_fy,sze);
            }
        }
    }

    //printf("\n fukking 4\n");

    sze = (int)(6 * fblocksigma);

    if (sze % 2 == 0)
    {
        sze = sze + 1;
    }

    if(fix_point == 0)
    {
        gauss = (float*)malloc(sze*sze * sizeof(float));
        create_gaussian_kernel(gauss, sze, fblocksigma);
    }
    else
    {
        fp_gauss = (int*)malloc(sze*sze * sizeof(int));
        create_gaussian_kernel_fp(fp_gauss, sze, fblocksigma);
    }

    buffer4 = (float*)malloc(cols*rows*sizeof(float));
    buffer5 = (float*)malloc(cols*rows*sizeof(float));
    buffer6 = (float*)malloc(cols*rows*sizeof(float));

    Gxx = buffer4;
    Gyy = buffer5;
    Gxy = buffer6;

    //printf("\n fukking 5\n");

    if (fft_flag == 0)
    {
        if(fix_point == 0)
        {
            if(turbo == 1)
            {
                float *w=__get1DGaussianKernel(21,3.5);
                Image kernel=__setImage((int*)w,21,1),
                imgxx=__setImage((int*)buffer1,cols,rows),imgyy=__setImage((int*)buffer2,cols,rows),imgxy=__setImage((int*)buffer3,cols,rows);
                
                //printf("\n tim 0 %d %d\n", cols, rows);

                SymmetryConvolutionf(&imgxx,&kernel);

                //printf("\n tim 1\n");

                SymmetryConvolutionf(&imgyy,&kernel);
                
                //printf("\n tim 2\n");

                SymmetryConvolutionf(&imgxy,&kernel);
                
                //printf("\n tim 3\n");

                __Mulf((float*)imgxy.data,2.0f,imgxy.size);
                
                //printf("\n tim 4\n");

                free(w);
            }
            else
            {
                convolution_with_padding_mirror_with_3(cols, rows, Gxx, Gyy, Gxy, Gx, Gy, Gxy_, gauss, sze); 
            }
        }
        else
        {
            convolution_with_padding_mirror_with_3_fp(cols, rows, Gxx, Gyy, Gxy, fp_Gx, fp_Gy, fp_Gxy_, fp_gauss, sze);
        }
    }
    else if (fft_flag == 1)
    {
        if(fix_point == 0)
        {
            unsigned int temp;

            stfft_data.out1 = Gxx;
            stfft_data.out2 = Gyy;
            stfft_data.out3 = Gxy;

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = stfft_data.raw3_reference_size = cols;

            stfft_data.raw1_in = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw1_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw1_reference = Gx;

            stfft_data.raw2_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw2_out = (complex*)calloc(temp, sizeof(complex));
            stfft_data.raw2_reference = Gy;

            stfft_data.raw3_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.raw3_reference = Gxy_;

            stfft_data.kernel_in = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_out = (complex*)calloc(temp,sizeof(complex));
            stfft_data.kernel_reference = gauss;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_3(&stfft_data);
        }
        else
        {
            //todo
        }
    }

    //printf("\n fukking 6\n");

    if(fix_point == 0)
    {
        free(gauss);
    }
    else
    {
        free(fp_gauss);
    }


    if(fix_point == 0)
    {
        if(turbo == 0)
        {
            sin2theta = buffer1;
            cos2theta = buffer2;
        }
        pixel_ort_tmp = buffer3;
        //reliability = buffer7;
    }
    else
    {
        sin2theta = (float*)malloc(cols*rows*sizeof(float));
        cos2theta = (float*)malloc(cols*rows*sizeof(float));
        pixel_ort_tmp = (float*)malloc(cols*rows*sizeof(float));
    }

    Image *__sin=0,*__cos=0;

    if(fix_point == 0)
    {
        if(turbo == 1)
        {
            __sin=__initImage(cols,rows,0,0);
            __cos=__initImage(cols,rows,0,0);

            cal_theata_reli(cols,rows,buffer1,buffer2,buffer3,(float*)__sin->data,(float*)__cos->data, pixel_ort_r);
        }
        else
        {
            cal_theata_reli(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta, pixel_ort_r);
        }
    }
    else
    {
        cal_theata_fp_reli(cols, rows, Gxx, Gyy, Gxy, sin2theta, cos2theta, pixel_ort_r);
    }

    if(forientsmoothsigma > 0)
    {
        sze = (int)(6 * forientsmoothsigma);
        if (sze % 2 == 0)
        {
            sze = sze + 1;
        }

        if (fft_flag == 0)
        {
            if(resize == 0)
            {
                if(turbo == 1)
                {
                    Image __out=__setImage((int*)pixel_ort,cols,rows);
                    getAngle(&__out,__sin,__cos,sze);
                    __freeImage(__sin);
                    __freeImage(__cos);
                }
                else{
                    convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort, sze);
                }
            }
            else
            {
                if(turbo == 1)
                {
                    Image __out=__setImage((int*)pixel_ort_tmp,cols,rows);
                    getAngle(&__out,__sin,__cos,sze);
                    __freeImage(__sin);
                    __freeImage(__cos);
                }
                else
                {
                    convolution_with_padding_mirror_with_2(cols, rows, sin2theta, cos2theta, pixel_ort_tmp, sze);
                }
            }
        }
        else if (fft_flag == 1)
        {
            unsigned int temp;
            temp = stfft_data.fft_size*stfft_data.fft_size*sizeof(complex);

            memset(stfft_data.raw1_in,0,temp);
            memset(stfft_data.raw1_out,0,temp);
            memset(stfft_data.raw2_in,0,temp);
            memset(stfft_data.raw2_out,0,temp);
            memset(stfft_data.kernel_in,0,temp);
            memset(stfft_data.kernel_out,0,temp);

            if(resize == 0)
            {
                stfft_data.out1 = pixel_ort;
            }
            else
            {
                stfft_data.out1 = pixel_ort_tmp;
            }

            stfft_data.mirror_extra_half_size = sze / 2;

            stfft_data.fft_size = cols + (2*stfft_data.mirror_extra_half_size) + sze - 1;
            stfft_data.fft_size = check_power_of_two(stfft_data.fft_size);

            temp = stfft_data.fft_size*stfft_data.fft_size;

            stfft_data.raw1_reference_size = stfft_data.raw2_reference_size = cols;

            stfft_data.raw1_reference = sin2theta;

            stfft_data.raw2_reference = cos2theta;

            stfft_data.kernel_reference = NULL;
            stfft_data.kernel_reference_size = sze;

            fft_with_padding_mirror_with_2(&stfft_data);

            if(fix_point == 0)
            {
                free(stfft_data.raw1_in);
                free(stfft_data.raw1_out);
                free(stfft_data.raw2_in);
                free(stfft_data.raw2_out);
                free(stfft_data.raw3_in);
                free(stfft_data.raw3_out);
                free(stfft_data.kernel_in);
                free(stfft_data.kernel_out);
            }
        }
    }

    if(resize == 1)
    {
        float* out_tmp = (float*)calloc((image_w+1)*(image_w+1),sizeof(float));
        //bilinear_image(NULL,pixel_ort_tmp,pixel_ort,out_tmp,re_size,image_w);
        //printf("\n fukkkkkkkkkkkkkkkkkkkkk \n");
        resize_back_bilinear_image(NULL,pixel_ort_tmp,pixel_ort,out_tmp,cols,rows,image_w,image_h);
        //printf("\n debug \n");
        if(fix_point == 0)
        {
            free(resize_image);
        }
        else
        {
            free(fp_resize_image);
        }
        free(out_tmp);
    }

    if(fix_point == 0)
    {
        free(fx);
        free(fy);
    }
    else
    {
        free(fp_fx);
        free(fp_fy);

        free(fp_buffer1);
        free(fp_buffer2);
        free(fp_buffer3);
    }

    free(u8image);
    free(sin2theta);
    free(cos2theta);
    free(pixel_ort_tmp);
    free(buffer4);
    free(buffer5);
    free(buffer6);
    //free(buffer7)
}*/


float Orient_V2(int image_h, int image_w, float py, float px, float* pixel_ort){
    /*
    float orient_y = 0;
    float orient_x = 0;
    
    orient_y = sin(pixel_ort[(int)(image_w * py + px)]);
    orient_x = cos(pixel_ort[(int)(image_w * py + px)]);
    */
    return pixel_ort[(int)(image_w * py + px)];
}

void OrientOfPoints_V2(float* local_min_ort, int image_h, int image_w, int local_min_cnt, int* local_min_x, int* local_min_y, float* pixel_ort) {
    float orient_y = 0;
    float orient_x = 0;
    for(int i=0; i<local_min_cnt; i++){
        //local_min_ort[i] = Orient_V2(image_h, image_w, local_min_y[i], local_min_x[i], pixel_ort);
        local_min_ort[i] = pixel_ort[(int)(image_w * local_min_y[i] + local_min_x[i])];
       
        //local_min_r[i] = pixel_ort_r[(int)(image_w * local_min_y[i] + local_min_x[i])];
    }
}

void OrientOfPoints_V2_small(float* local_min_ort, int image_h, int image_w, int local_min_cnt, int* local_min_x, int* local_min_y, float* pixel_ort) {
    float orient_y = 0;
    float orient_x = 0;
    printf("\nc ori\n");
    for(int i=0; i<local_min_cnt; i++){
        //local_min_ort[i] = Orient_V2(image_h, image_w, local_min_y[i], local_min_x[i], pixel_ort);
        local_min_ort[i] = pixel_ort[(int)((image_w * local_min_y[i] + local_min_x[i])/2)];
        printf("%f ",local_min_ort[i]);
        //local_min_r[i] = pixel_ort_r[(int)(image_w * local_min_y[i] + local_min_x[i])];
    }
    printf("\n");
}

void Detect_V3(float* pixel_ort, int radius, BYTE* img_uint8, BYTE* mask, int image_h, int image_w, int switch_angle_calculation){

    int blocksigma = 7;
    int gradientsigma = 1;
    int orientsmoothsigma = 7;

    BYTE* image = (BYTE*)G3CAlloc(image_h*image_w, sizeof(BYTE));
    TBinary8Bits(img_uint8, image, image_h, image_w, radius, mask);
    /*
    printf("\nc image\n");
    for(int i=0; i<image_w*image_h; i++){
        printf("%d ", image[i]);
    }
    printf("\n");
    */
    //clock_t start = clock();
    if (switch_angle_calculation == 0)
    {
        RidgeOrient_V3(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 0, 0,0, pixel_ort);
    }
	else if(switch_angle_calculation == 1)
	{
	    //RidgeOrient_V3(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 1, 1,0, pixel_ort);
        RidgeOrient_V4(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 1, 1,0, pixel_ort);
	}
    else if(switch_angle_calculation == 2)
    {
        RidgeOrient_V4(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 1, 0,1, pixel_ort);
    }
    else //if(switch_angle_calculation == 3)
    {
        RidgeOrient_V4(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 0, 0,1, pixel_ort);
    }
    /*
    else
    {
        RidgeOrient_V4_small(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 0, 0,1, pixel_ort);
    }
    */
	//clock_t end = clock();
	//printf("mode %d orient %f ms\n",switch_angle_calculation,(end-start)*1000.0/CLOCKS_PER_SEC);

    G3Free(image);

    //printf("\n finish detect v3 \n");
}

void Detect_V3_reli(float* pixel_ort, double* pixel_ort_r,int radius, BYTE* img_uint8, BYTE* mask, int image_h, int image_w, int switch_angle_calculation){

    int blocksigma = 7;
    int gradientsigma = 1;
    int orientsmoothsigma = 7;

    BYTE* image = (BYTE*)G3CAlloc(image_h*image_w, sizeof(BYTE));
    TBinary8Bits(img_uint8, image, image_h, image_w, radius, mask);
    
    if (switch_angle_calculation == 0)
    {
        RidgeOrient_V3_reli(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 0, 0,0, pixel_ort, pixel_ort_r);
    }
//    else if(switch_angle_calculation == 1)
//    {
//        RidgeOrient_V4_reli(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 1, 1,0, pixel_ort, pixel_ort_r);
//    }
//    else if(switch_angle_calculation == 2)
//    {
//        RidgeOrient_V4_reli(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 1, 0,1, pixel_ort, pixel_ort_r);
//    }
//    else
//    {
//        RidgeOrient_V4_reli(image, image_h, image_w, gradientsigma, blocksigma, orientsmoothsigma, 0, 0, 0,1, pixel_ort, pixel_ort_r);
//    }
    
    G3Free(image);
}

/*int extract_patches_v3(unsigned char *patches,
                               unsigned char *image, 
                               float *pts_x, 
                               float *pts_y, 
                               double *pts_angle_radians, 
                               BYTE *valid_index, 
                               unsigned long pts_count, 
                               int image_h, 
                               int image_w,
                               int patch_height,
                               int resize_patch_height,
                               bool with_flip,
                               int fp_flag,
                               float patch_th,
                               BYTE* mask)
{
    int valid_cnt; 

    //cal_mode == 5
    
    valid_cnt = crop_patches_opencv_with_mask(image, patches, pts_x, pts_y, pts_angle_radians, patch_height, resize_patch_height,pts_count, patch_th, valid_index, with_flip, image_h, image_w, 0, mask);
    

    if(with_flip == true)
    {
        for(int i=0; i<valid_cnt; i++)
        {
            for(int j=0; j<resize_patch_height*resize_patch_height; j++)
            {
                patches[resize_patch_height*resize_patch_height * (i * 2 + 1) + j] = patches[i*resize_patch_height*resize_patch_height*2 + resize_patch_height*resize_patch_height-1-j];
            }
        }
    }
    
    return valid_cnt;
}*/


#ifdef __cplusplus
}
#endif
