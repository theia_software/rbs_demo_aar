AUO大面積sensor ISP, 主要函數: 

void AUO_ISP(int input_img, int bkg, unsigned char *result_img, int width, int height, int denoise_hv);

input_img    : 16bit影像
bkg:         : 16bit bkg, 如果不需要扣背請傳入NULL
result_img   : 8 bit影像 
width, height: 影像寬高
denoise_hv   = 1, 去除水平/垂直雜訊 
             = 0, 不去除水平/垂直雜訊    