#ifndef macros_allocate_h
#define macros_allocate_h

#include "g3_sys.h"

#ifndef BYTE
typedef unsigned char BYTE;
#endif

#ifndef G3FillMem
#define G3FillMem(data, value, count) memset(data, value, count)
#endif

#ifndef G3CopyMem
#define G3CopyMem(dst, src, count) memcpy(dst, src, count)
#endif

#ifndef G3Alloc
#define G3Alloc(size) malloc(size)
#endif

#ifndef G3CAlloc
#define G3CAlloc(count, size) calloc(count, size)
#endif

#ifndef G3ReAlloc
#define G3ReAlloc(ptr, size) realloc(ptr, size)
#endif

#ifndef G3Free
#define G3Free(data) free(data)
#endif

#ifdef WIN32
#define EGIS __declspec(dllexport)
#else
#define EGIS
#endif

#endif /* macros_allocate_h */
