//
//  INX_EP_ISP.c
//  INX_EP_ISP
//
//  Created by 黃詠淮 on 2019/4/2.
//  Copyright © 2019 黃詠淮. All rights reserved.
//


#include "kiss_fft.h"
#include "kiss_fftr.h"
#include "kiss_fftnd.h"
#include "kiss_fftndr.h"
#include "ISP_filter.h"
#include "ISP_normalize.h"

#define WIDTH 105
#define HEIGHT 210

int main(int argc, char *argv[]){

    int i ;
    int frame[WIDTH * HEIGHT] ;
    int background[WIDTH * HEIGHT] ; // all zero is OK
    unsigned char ret[WIDTH * HEIGHT] ;

    // if need unlimit loop
    //while(1) {
    //  INX_EP_ISP(frame, background, ret, WIDTH, HEIGHT) ;
    //}

    int cnt = 0 ;
    for ( int h = 0 ; h < HEIGHT; h++ ) {
      for ( int w = 0 ; w < WIDTH ; w++ ) {
        scanf("%d", &frame[cnt]) ;
        background[cnt] = 0 ;
        cnt ++ ;
      }
    }
    //AUO_ISP(frame, background, ret, WIDTH, HEIGHT) ;
    cnt = 0 ;
    for ( int h = 0 ; h < HEIGHT; h++ ) {
      for ( int w = 0 ; w < WIDTH ; w++ ) {
        printf("%d ", ret[cnt]) ;
        cnt ++ ;
      }
      printf("\n") ;
    }
    return 0 ;
}


