LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)

LOCAL_MODULE := GOMatcherAlgorithm
LOCAL_SRC_FILES := \
	match_lib/libGOMatcherAlgorithm_arm64-v8a.a
include $(PREBUILT_STATIC_LIBRARY)
endif


ifeq ($(TARGET_ARCH_ABI),x86)

LOCAL_MODULE := GOMatcherAlgorithm
LOCAL_SRC_FILES := \
	match_lib/libGOMatcherAlgorithm_x86.a
include $(PREBUILT_STATIC_LIBRARY)
endif

include $(CLEAR_VARS)
LOCAL_MODULE := JNIMethod
LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)
LOCAL_SRC_FILES := \
	$(LOCAL_PATH)/jni_sample.cpp \
	$(LOCAL_PATH)/AUO_ISP.c \
	$(LOCAL_PATH)/ISP_fft.c \
	$(LOCAL_PATH)/ISP_filter.c \
	$(LOCAL_PATH)/ISP_normalize.c \
	$(LOCAL_PATH)/kiss_fft.c \
	$(LOCAL_PATH)/statistics.c \
	$(LOCAL_PATH)/clahe.c \
	$(LOCAL_PATH)/kiss_fftnd.c \
	$(LOCAL_PATH)/kiss_fftndr.c \
	$(LOCAL_PATH)/kiss_fftr.c

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_STATIC_LIBRARIES := GOMatcherAlgorithm
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/match_lib/
LOCAL_SRC_FILES += \
	$(LOCAL_PATH)/match_lib/egis_log.c \
	$(LOCAL_PATH)/match_lib/linux_plat.c \
	$(LOCAL_PATH)/match_lib/algo_api.c \
	$(LOCAL_PATH)/match_lib/sys_heap.c

LOCAL_CFLAGS += -DGOMATCHER_ANDROID
endif



ifeq ($(TARGET_ARCH_ABI),x86)
LOCAL_STATIC_LIBRARIES := GOMatcherAlgorithm
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/match_lib/
LOCAL_SRC_FILES += \
	$(LOCAL_PATH)/match_lib/egis_log.c \
	$(LOCAL_PATH)/match_lib/linux_plat.c \
	$(LOCAL_PATH)/match_lib/algo_api.c \
	$(LOCAL_PATH)/match_lib/sys_heap.c

LOCAL_CFLAGS += -DGOMATCHER_ANDROID
endif

LOCAL_LDLIBS += -llog
LOCAL_LDLIBS += -lm

include $(BUILD_SHARED_LIBRARY)
