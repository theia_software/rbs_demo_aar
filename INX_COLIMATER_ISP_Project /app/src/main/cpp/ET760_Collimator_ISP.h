
#ifndef ET760_COLLIMATOR_ISP_H
#define ET760_COLLIMATOR_ISP_H

#ifdef __cplusplus
extern "C" {
#endif

void ET760_Collimator_ISP(int *input_img, int *bkg, unsigned char *result_img, int width, int height);
void ET760_Collimator_ISP_binning(int *input_img, int *bkg, unsigned char *result_img, int width, int height);

int sub_best_bkg(int *img, int *bkg_set, int *current_bkg_num, int max_bkg_num, int *img_sb, int width, int height, int k_size, int enable_block, int block_w, int block_h, int block_w_num, int block_h_num, int bypass_adjust_bkg, float learning_rate);

float cal_snr(int *image, int *bkg, unsigned char *ref_img, int width, int height);

char* check_version();

#ifdef __cplusplus
}
#endif

#endif /* AUO_ISP_h */
