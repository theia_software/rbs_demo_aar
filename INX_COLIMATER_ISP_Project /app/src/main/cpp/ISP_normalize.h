//
//  ISP_normalize.h
//  ET713_Image_Preprocessing
//
//

#ifndef ISP_normalize_h
#define ISP_normalize_h


void _local_normalize(float *img, int width, int height, float *bkg_estimate_filter, int f_radius);

void _mean_std_normalize_roi(float *img, int size, float mean_out, float std_out, unsigned char *mask_roi);

void _drop_data_out_of_range(float *data, int size, float max, float min);

void linear_normalize(float *img, int size, float max_out, float min_out);

void moving_normalize(float *img, int width, int height, int win_size, float max_out, float min_out);

void roi_normalize(float *img, int x1, int y1, int x2, int y2, int width, int height, float max_out, float min_out);

#endif /* ISP_normalize_h */
