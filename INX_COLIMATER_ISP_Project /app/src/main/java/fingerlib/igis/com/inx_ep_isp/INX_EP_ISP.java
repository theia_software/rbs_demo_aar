package fingerlib.igis.com.inx_ep_isp;


import java.util.ArrayList;

/**
 * Created by yougangkuo on 2019/7/2.
 */

public class INX_EP_ISP {

    static public String TAG = "INX_EP_ISP" ;
    static {
        System.loadLibrary("inx_ep_isp");
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public static native String stringFromJNI();
    public static native String init(int w, int h);
    public native static void INX_EP_ISP_JNI(int [] frame, int [] background, int w, int h );

    public static native String getVersion();

    private static native float cal_snr(int []image, int []bkg, int[] ref_img, int width, int height);



    /**
     * int *img,
     * int *bkg_set,
     * int *current_bkg_num,
     * int max_bkg_num,
     * int *img_sb,
     *
     *
     * int width, int height,
     * int k_size, int enable_block,
     *
     * int block_w, int block_h, int block_w_num, int block_h_num,
     * int bypass_adjust_bkg, float learning_rate);
     *
     *
     */
    public native static int sub_best_bkg(int [] img,
                                    int []bkg_set,
                                    int []current_bkg_num,
                                    int max_bkg_num,
                                    int []img_sb,
                                    int w, int h,
                                    int ksize, int enable_block, int black_w, int black_h,
                                    int black_w_num, int black_h_num, int bypass_adjust_bkg, float learning_rate) ; ;
    /**
     * 對外改成不傳BACKGROUND
     * 注意，直接改FRAME的REFERENCE !!!
     * @param frame
     * @param w
     * @param h
     */
    static public int [] temp_background ; //

    /**
     * 指紋影像增強 FOR AUO F1 PANEL 含BACKGROUND 的處理
     * @param frame main frame
     * @param background can be null
     * @param height 240
     * @param width 375
     */
    static public void INX_EP_ISP(int [][] frame, int [][] background, int height, int width ) {

        int len = width * height;
        int[] frame_1d = new int[len];
        int[] background_1d = new int[len];
        int i = 0;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                frame_1d[i] = frame[h][w];
                i++;
            }
        }
        if (background != null) {
            i=0 ;
            for (int h = 0; h < height; h++) {
                for (int w = 0; w < width; w++) {
                    background_1d[i] = background[h][w];
                    i++;
                }
            }
        }
        INX_EP_ISP_JNI(frame_1d ,background_1d, width, height);

        // 擺回去2D ARRAY
        i = 0 ;
        int sum = 0 ;
        for ( int h = 0 ; h < height ; h++ ) {
            for ( int w = 0 ; w < width ; w++ ) {
                frame[h][w] = frame_1d[i++] ;
                sum = sum + frame[h][w] ;
            }
        }
    }


    /**
     * 預計不會亂動的設定們
     */

    static public float learning_rate = 0.1f;

    static public int k_size = 15;
    static public int enable_block = 1;
    static public int block_w = 25;
    static public int block_h = 20;
    static public int block_w_num = 15 ; //width/block_w+(((width%block_w) == 0)?0:1); // 15
    static public int block_h_num = 12 ; // height/block_h+(((height%block_h) == 0)?0:1); // 12
    static public int bypass_adjust_bkg = 0 ;

    static public int MAX_BKG_NUM = 10 ; // 定義最大的BKG SIZE


    /**
     *
     * @param img raw finger frame
     * @param bkg_set 1 or more background
     * @param width
     * @param height
     * @return
     */
    static public int sub_best_bkg_Default(int [][] img,
                                    ArrayList<int[][]> bkg_set,
                                    int width, int height) {


        int [] current_bkg_num = new int[1] ; // 當指標用，往下傳的
        int [][] img_sb = new int [height][width] ;
        int len = width * height;
        int[] frame_1d = new int[len];
        int[] background_1d = new int[len * bkg_set.size()];
        int i = 0;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                frame_1d[i] = img[h][w];
                i++;
            }
        }
        if (bkg_set != null) {
            i=0 ;
            for ( int arrIndex = 0 ; arrIndex < bkg_set.size() ; arrIndex++ ) {
                int [][] background = bkg_set.get(arrIndex) ;
                for (int h = 0; h < height; h++) {
                    for (int w = 0; w < width; w++) {
                        background_1d[i] = background[h][w];
                        i++;
                    }
                }
            }

        }

        current_bkg_num[0] = bkg_set.size() ;
        int select_best_index = sub_best_bkg(
                frame_1d ,
                background_1d,
                current_bkg_num,
                INX_EP_ISP.MAX_BKG_NUM,
                frame_1d, width, height,
                k_size,enable_block ,block_w,block_h,block_w_num,block_h_num,bypass_adjust_bkg, learning_rate);

        // 擺回去2D ARRAY
        i = 0 ;
        int sum = 0 ;
        for ( int h = 0 ; h < height ; h++ ) {
            for ( int w = 0 ; w < width ; w++ ) {
                img[h][w] = frame_1d[i++] ;
                //sum = sum + img_sb[h][w] ;
            }
        }

        return select_best_index ;
    }


    /**
     *
     * @param frame MUST be have
     * @param background can be null
     * @param ref_img MUST BE HAVE with `unsigned char` 0~255 only
     * @param width
     * @param height
     * @return
     */
    static public float cal_snr( int[][] frame, int [][] background, int [][] ref_img, int width, int height ) {

        int len = width * height;
        int[] frame_1d = new int[len];
        int[] background_1d = new int[len];
        int[] ref_img_1d = new int[len] ;

        int i = 0;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                frame_1d[i] = frame[h][w];
                ref_img_1d[i] = ref_img[h][w] ;
                i++;
            }
        }
        if (background != null) {
            i=0 ;
            for (int h = 0; h < height; h++) {
                for (int w = 0; w < width; w++) {
                    background_1d[i] = background[h][w];
                    i++;
                }
            }
        }

        float ret = cal_snr(frame_1d, background_1d, ref_img_1d, width, height) ;

        return ret ;
    }
}
